import pandas as pd
import numpy as np
import os.path
import copy

# Make main function
def find_file_step(l_all_pdb=None, l_miss_pdb=None):
    pbd_list = []

    for pdb in l_all_pdb:
        # Create filepahs
        fpath4 = os.path.join("pdbbind_v2017_refined",pdb,"04_out_%s.sd"%pdb)
        fpath3 = os.path.join("pdbbind_v2017_refined",pdb,"03_create_rdock_cavity_cav1.grd")
        fpath2 = os.path.join("pdbbind_v2017_refined",pdb,"02_%s.mol2"%pdb)
        fpath1 = os.path.join("pdbbind_v2017_refined",pdb,"01_%s.pdb"%pdb)

        # If pdb not in missing, step 4 mush have been completed
        if pdb not in l_miss_pdb:
            val = 4
        else:
            if os.path.exists(fpath4):
                val = 4
            elif os.path.exists(fpath3):
                val = 3
            elif os.path.exists(fpath2):
                val = 2
            elif os.path.exists(fpath1):
                val = 1
            else:
                val = 0

        # Append value
        pbd_list.append([pdb, val])

    columns = ["pdb_ids", "f_step"]
    df = pd.DataFrame(data=pbd_list, columns=columns)
    return df

def create_refined_data_2017():
    fname = os.path.join("pdbbind_v2017_refined","index","INDEX_refined_data.2017")    
    with open(fname) as f:
        f_lines = f.readlines()
    
    data = []
    for line in f_lines:
        if line[0] == "#":
            continue

        PDB = line[0:6].strip()
        resolution = float(line[6:12].strip())
        release_year = int(line[12:18].strip())
        logKd_Ki = float(line[18:25].strip())
        Kd_Ki = line[25:39].strip()
        reference = line[42:51].strip()
        ligand_name = line[51:].strip()
        #print(PDB, resolution, release_year, logKd_Ki, Kd_Ki, reference, ligand_name)
        
        if Kd_Ki[0:3] == "Kd=":
            Kd_Ki_type = "Kd"
            Kd_Ki = Kd_Ki.replace(Kd_Ki_type+"=", "")
        elif Kd_Ki[0:3] == "Ki=":
            Kd_Ki_type = "Ki"
            Kd_Ki = Kd_Ki.replace(Kd_Ki_type+"=", "")
        elif Kd_Ki[0:4] == "Ki<=":
            Kd_Ki_type = "Ki"
            Kd_Ki = Kd_Ki.replace(Kd_Ki_type+"<=", "")

        # Find the unit
        Kd_Ki_unit = Kd_Ki[-2:]
        # Now remove type from string
        Kd_Ki = float(Kd_Ki.replace(Kd_Ki_unit, ""))
        #print(Kd_Ki, Kd_Ki_unit)

        #ligand_name = ligand_name.replace("(","").replace(")","")

        if Kd_Ki_type == "Kd":
            data.append([PDB, resolution, release_year, logKd_Ki, True, False, Kd_Ki, Kd_Ki_unit, reference, ligand_name])
        elif Kd_Ki_type == "Ki":
            data.append([PDB, resolution, release_year, logKd_Ki, False, True, Kd_Ki, Kd_Ki_unit, reference, ligand_name])
        else:
            data.append([PDB, resolution, release_year, logKd_Ki, False, False, Kd_Ki, Kd_Ki_unit, reference, ligand_name])

    cols = ["pdb_ids", "resolution", "release_year", "-logKd/Ki", "Kd", "Ki", "Kd_Ki_val", "Kd_Ki_unit", "reference", "ligand_name"]
    df = pd.DataFrame(data=data, columns=cols)
    return df

def create_refined_set_2017():
    fname = os.path.join("pdbbind_v2017_refined","index","INDEX_refined_set.2017")    
    with open(fname, "r", errors='replace') as f:
        f_lines = f.readlines()
    
    data = []
    for line in f_lines:
        if line[0] == "#":
            continue

        PDB = line[0:6].strip()
        resolution = float(line[6:12].strip())
        release_year = int(line[12:18].strip())
        Kd_Ki = line[18:32].strip()
        reference = line[35:44].strip()
        ligand_info = line[44:].strip()
        #print(PDB, resolution, release_year, Kd_Ki, reference, ligand_info)

        if Kd_Ki[0:3] == "Kd=":
            Kd_Ki_type = "Kd"
            Kd_Ki = Kd_Ki.replace(Kd_Ki_type+"=", "")
        elif Kd_Ki[0:3] == "Ki=":
            Kd_Ki_type = "Ki"
            Kd_Ki = Kd_Ki.replace(Kd_Ki_type+"=", "")
        elif Kd_Ki[0:4] == "Ki<=":
            Kd_Ki_type = "Ki"
            Kd_Ki = Kd_Ki.replace(Kd_Ki_type+"<=", "")

        # Find the unit
        Kd_Ki_unit = Kd_Ki[-2:]
        # Now remove type from string
        Kd_Ki = float(Kd_Ki.replace(Kd_Ki_unit, ""))
        #print(Kd_Ki, Kd_Ki_unit)

        #ligand_name = ligand_name.replace("(","").replace(")","")

        if Kd_Ki_type == "Kd":
            data.append([PDB, resolution, release_year, True, False, Kd_Ki, Kd_Ki_unit, reference, ligand_info])
        elif Kd_Ki_type == "Ki":
            data.append([PDB, resolution, release_year, False, True, Kd_Ki, Kd_Ki_unit, reference, ligand_info])
        else:
            data.append([PDB, resolution, release_year, False, False, Kd_Ki, Kd_Ki_unit, reference, ligand_info])

    cols = ["pdb_ids", "resolution", "release_year", "Kd", "Ki", "Kd_Ki_val", "Kd_Ki_unit", "reference", "ligand_info"]
    df = pd.DataFrame(data=data, columns=cols)
    return df


def create_refined_name_2017():
    fname = os.path.join("pdbbind_v2017_refined","index","INDEX_refined_name.2017")    
    with open(fname, "r", errors='replace') as f:
        f_lines = f.readlines()
    
    data = []
    for line in f_lines:
        if line[0] == "#":
            continue

        PDB = line[0:6].strip()
        release_year = int(line[6:12].strip())
        Uniprot = line[12:20].strip()
        protein_name = line[20:].strip()
        #print(PDB, release_year, Uniprot, protein_name)
        
        data.append([PDB, release_year, protein_name, Uniprot])

    cols = ["pdb_ids", "release_year", "protein_name", "Uniprot"]
    df = pd.DataFrame(data=data, columns=cols)
    
    # Get the number of occurences
    #valc = df["Uniprot"].value_counts()
    #for Uniprot, Uniprot_count in valc.iteritems():

    # Collect in dictionary
    d = {}
    grouped = df.groupby("Uniprot")
    for name, group in grouped:
        d[name] = {}
        if name == "------":
            group_pdb_ids = []
        else:
            group_pdb_ids = list(group['pdb_ids'].values)
        d[name]["pdb_ids_list"] = group_pdb_ids

    # Create
    data = []
    for pdb, Uniprot in zip(df["pdb_ids"], df["Uniprot"]):
        Uniprot_pdb_list = copy.copy(d[Uniprot]["pdb_ids_list"])
        if pdb in Uniprot_pdb_list:
            Uniprot_pdb_list.remove(pdb)
        if Uniprot_pdb_list == None:
            Uniprot_pdb_list = []
        Uniprot_N = len(Uniprot_pdb_list)
        Uniprot_pdb_ids_s = ",".join(str(x) for x in Uniprot_pdb_list)

        #print(pdb, Uniprot, Uniprot_N, Uniprot_pdb_ids_s)
        data.append([pdb, Uniprot, Uniprot_N, Uniprot_pdb_ids_s])

    cols = ["pdb_ids", "Uniprot", "Uniprot_N", "Uniprot_pdb_ids_s"]
    df2 = pd.DataFrame(data=data, columns=cols)

    # Merge
    df_m = pd.merge(df, df2, how='left', on=["pdb_ids", "Uniprot"])
    
    return df_m

def create_index_df():

    # Create dataframe from each
    refined_data_2017 = create_refined_data_2017()
    refined_set_2017 = create_refined_set_2017()
    refined_name_2017 = create_refined_name_2017()

    # Merge first two
    df_m = pd.merge(refined_data_2017, refined_set_2017, how='outer', on=["pdb_ids", "resolution", "release_year","Kd", "Ki", "Kd_Ki_val", "Kd_Ki_unit", "reference"])
    # Merge next two
    df_m = pd.merge(df_m, refined_name_2017, how='outer', on=["pdb_ids", "release_year"])

    return df_m

def mymain(fname_pdb=None, fname_time=None):
    # Open file with pdb ids
    with open(fname_pdb) as f:
        f_lines = f.readlines()
    pdb_ids = []
    for l in f_lines:
        pdb_ids.append(l.strip())

    # Create initial dataframe
    columns = ["pdb_ids"]
    df = pd.DataFrame(data=pdb_ids, columns=columns)

    # Create dataframe of time
    df_time = pd.read_csv(fname_time, sep=',', header=0, names=["Nr", "pdb_ids", "time_N", "time_s"])
    df_time.drop(['Nr'], axis=1, inplace=True)

    # Merge on 
    df_m = pd.merge(df, df_time, how='left', on=['pdb_ids'])
    # Find dataframe with missing time
    df_miss = df_m[df_m['time_s'].isnull()]
    miss_pdb_ids = list(df_miss['pdb_ids'].values)
    
    # Create df with file info
    df_finfo = find_file_step(l_all_pdb=pdb_ids, l_miss_pdb=miss_pdb_ids)
    # Get the number of occurences
    #valc = df_finfo["f_step"].value_counts()
    #print(valc, "Total:", np.sum(valc))

    # Merge with file step
    df_m = pd.merge(df_m, df_finfo, how='left', on=['pdb_ids'])
    #print(df_m[df_m["f_step"] == 3])
    #print(df_m[df_m["f_step"] == 2])
    
    # Get the info df
    df_info = create_index_df()

    # Merge
    df_m = pd.merge(df_m, df_info, how='left', on=['pdb_ids'])

    # Save
    df_m.to_pickle("12_info_dataframe.pkl")

# Execute main
if __name__ == "__main__":
    mymain(fname_pdb="00_all_pdb.txt", fname_time="00_all_pdb.txt.sec")