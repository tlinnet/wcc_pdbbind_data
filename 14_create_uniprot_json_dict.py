from bioservices import UniProt
import json
import os.path

def mymain(fname_uniprot=None):
    # Open file with pdb ids
    with open(fname_uniprot) as f:
        f_lines = f.readlines()
    uniprots = []
    for l in f_lines:
        uniprots.append(l.strip())
    
    # Make instance
    UniC = UniProt(verbose=False)
    
    # Define data dict
    json_dic = '14_uniprot_json_dict.json'
    if os.path.exists(json_dic):
        with open(json_dic, 'r') as fp:
            d = json.load(fp)
    else:
        d = {}

    # Loop and perform
    for i, uniprot in enumerate(uniprots):
        # If the dictionary is already running
        if uniprot in d:
            print(i, "'%s'"%uniprot, d[uniprot])
            continue

        resp_dict = UniC.mapping(fr="ACC", to="PDB_ID", query=uniprot)

        if uniprot in resp_dict:
            pdblist = resp_dict[uniprot]
        else:
            pdblist = []

        print(i, "'%s'"%uniprot, pdblist)
        d[uniprot] = pdblist

        with open(json_dic, 'w') as fp:
            #json.dump(d, fp)
            json.dump(d, fp, indent=4)

# Execute main
if __name__ == "__main__":
    mymain(fname_uniprot="00_refined_uniprot_uniq.txt")
