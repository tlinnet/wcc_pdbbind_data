import pandas as pd
import numpy as np
import os.path
import copy
from rdkit import Chem
import re
import subprocess
import os

def get_sdf_info(l_all_pdb=None):
    d_list = []
    for pdb in l_all_pdb:
        d = {}
        d['pdb_ids'] = pdb

        # Create filepahs
        d['sanitizable'] = False
        fpath_sdf = os.path.join("pdbbind_v2017_refined",pdb,"%s_ligand.sdf"%pdb)
        suppl = Chem.SDMolSupplier(fpath_sdf)
        convert_sdf_sdf = False
        for mol in suppl:
            # If initial molecule worked
            if mol != None:
                d['sanitizable'] = "original"
            # If not initial molecule works, try sdf -> sdf conversion with obabel
            else:
                convert_sdf_sdf = True

        # Possible, convert ligand from sdf to sdf
        convert_mol2_sdf = False
        if convert_sdf_sdf:
            fpath_sdf_sdf = os.path.join("pdbbind_v2017_refined",pdb,"%s_ligand_sdf.sdf"%pdb)
            if not os.path.exists(fpath_sdf_sdf):
                #obabel -isdf  8cpa_ligand.sdf -osdf -O 8cpa_ligand_sdf.sdf
                subprocess.run(["obabel", "-isdf", fpath_sdf, "-osdf", "-O", fpath_sdf_sdf], check=True)
            suppl = Chem.SDMolSupplier(fpath_sdf_sdf)

            for mol in suppl:
                # If initial molecule worked
                if mol != None:
                    d['sanitizable'] = "sdf->sdf"
                # If not initial molecule works, try mol2 -> sdf conversion with obabel
                else:
                    convert_mol2_sdf = True
                    os.remove(fpath_sdf_sdf)

        # Possible, convert ligand from mol2 to sdf
        if convert_mol2_sdf:
            fpath_mol2 = os.path.join("pdbbind_v2017_refined",pdb,"%s_ligand.mol2"%pdb)
            fpath_mol2_sdf = os.path.join("pdbbind_v2017_refined",pdb,"%s_ligand_mol2.sdf"%pdb)
            if not os.path.exists(fpath_mol2_sdf):
                #obabel -imol2  8cpa_ligand.mol2 -osdf -O 8cpa_ligand_mol2.sdf
                subprocess.run(["obabel", "-imol2", fpath_mol2, "-osdf", "-O", fpath_mol2_sdf], check=True)
            suppl = Chem.SDMolSupplier(fpath_mol2_sdf)

            for mol in suppl:
                # If initial molecule worked
                if mol != None:
                    d['sanitizable'] = "mol2->sdf"
                else:
                    os.remove(fpath_mol2_sdf)

        if (d['sanitizable'] == False):
            print("---------- %s problems"%pdb)
            #suppl = Chem.SDMolSupplier(fpath_sdf, sanitize=False)

        # Search in original sdf file
        regexp = re.compile("> <(\S+)\>\s+(\S+)")
        with open(fpath_sdf, 'r') as myfile:
            data=myfile.read()
        props = regexp.findall(data)
        for match in props:
            try:
                value = float(match[1])
                if value.is_integer():
                    value = int(value)
            except:
                value = match[1]
            key = match[0]
            d[key] = value

        # Collect dict
        d_list.append(d)

    df = pd.DataFrame.from_dict(d_list)
    return df

def get_mol2_info(l_all_pdb=None):
    for pdb in l_all_pdb:
        # Create filepahs
        fpath_mol2 = os.path.join("pdbbind_v2017_refined",pdb,"%s_ligand.mol2"%pdb)
        #print(pdb, os.path.exists(fpath_sdf))

        mol = Chem.MolFromMol2File(fpath_mol2)
        if mol == None:
            print(pdb, mol)
            continue
        print(pdb, mol.GetNumAtoms(), mol.GetNumConformers())

def mymain(fname_pdb=None):
    # Open file with pdb ids
    with open(fname_pdb) as f:
        f_lines = f.readlines()
    pdb_ids = []
    for l in f_lines:
        pdb_ids.append(l.strip())

    # Create initial dataframe
    columns = ["pdb_ids"]
    df = pd.DataFrame(data=pdb_ids, columns=columns)

    df = get_sdf_info(l_all_pdb=pdb_ids)
    #print("---------")
    #get_mol2_info(l_all_pdb=pdb_ids)

    df.to_pickle("13_ligand_dataframe.pkl")

# Execute main
if __name__ == "__main__":
    #mymain(fname_pdb="00_5_pdb.txt")
    mymain(fname_pdb="00_all_pdb.txt")
