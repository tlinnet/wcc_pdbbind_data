load 02_5ewy.mol2
load 5ewy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ewy
show sticks, 5ewy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
