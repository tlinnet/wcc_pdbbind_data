load 02_2arm.mol2
load 2arm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2arm
show sticks, 2arm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
