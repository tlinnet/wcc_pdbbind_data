load 02_2gvj.mol2
load 2gvj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2gvj
show sticks, 2gvj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
