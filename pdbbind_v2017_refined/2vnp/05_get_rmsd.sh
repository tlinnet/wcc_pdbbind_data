#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2vnp_ligand.sdf 04_out_2vnp.sd -o 04_out_2vnp_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2vnp_rmsd.sd | tee 05_get_rmsd.score
