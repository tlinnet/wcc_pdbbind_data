load 02_2vnp.mol2
load 2vnp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vnp
show sticks, 2vnp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
