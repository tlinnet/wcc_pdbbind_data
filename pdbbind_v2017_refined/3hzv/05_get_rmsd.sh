#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3hzv_ligand.sdf 04_out_3hzv.sd -o 04_out_3hzv_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3hzv_rmsd.sd | tee 05_get_rmsd.score
