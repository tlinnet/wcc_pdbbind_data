load 02_5e8f.mol2
load 5e8f_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5e8f
show sticks, 5e8f_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
