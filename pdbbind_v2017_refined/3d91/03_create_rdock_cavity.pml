load 02_3d91.mol2
load 3d91_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3d91
show sticks, 3d91_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
