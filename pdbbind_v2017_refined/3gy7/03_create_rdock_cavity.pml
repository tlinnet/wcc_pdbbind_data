load 02_3gy7.mol2
load 3gy7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gy7
show sticks, 3gy7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
