load 02_2ydw.mol2
load 2ydw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ydw
show sticks, 2ydw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
