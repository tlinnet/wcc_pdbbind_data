load 02_3ies.mol2
load 3ies_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ies
show sticks, 3ies_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
