load 02_4qem.mol2
load 4qem_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qem
show sticks, 4qem_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
