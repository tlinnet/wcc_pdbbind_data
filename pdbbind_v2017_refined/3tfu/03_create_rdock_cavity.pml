load 02_3tfu.mol2
load 3tfu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3tfu
show sticks, 3tfu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
