load 02_4z93.mol2
load 4z93_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4z93
show sticks, 4z93_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
