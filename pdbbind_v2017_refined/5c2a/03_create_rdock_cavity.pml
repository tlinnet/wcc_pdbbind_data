load 02_5c2a.mol2
load 5c2a_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5c2a
show sticks, 5c2a_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
