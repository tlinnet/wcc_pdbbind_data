load 02_4eor.mol2
load 4eor_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4eor
show sticks, 4eor_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
