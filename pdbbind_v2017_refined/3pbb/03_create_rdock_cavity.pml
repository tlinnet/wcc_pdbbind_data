load 02_3pbb.mol2
load 3pbb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3pbb
show sticks, 3pbb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
