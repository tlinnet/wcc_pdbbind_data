load 02_2ovy.mol2
load 2ovy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ovy
show sticks, 2ovy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
