load 02_3k97.mol2
load 3k97_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3k97
show sticks, 3k97_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
