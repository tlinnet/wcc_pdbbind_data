load 02_2avq.mol2
load 2avq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2avq
show sticks, 2avq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
