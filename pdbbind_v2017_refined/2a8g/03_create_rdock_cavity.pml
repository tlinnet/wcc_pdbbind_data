load 02_2a8g.mol2
load 2a8g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2a8g
show sticks, 2a8g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
