load 02_4ezz.mol2
load 4ezz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ezz
show sticks, 4ezz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
