load 02_5ia5.mol2
load 5ia5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ia5
show sticks, 5ia5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
