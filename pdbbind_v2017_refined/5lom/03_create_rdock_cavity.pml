load 02_5lom.mol2
load 5lom_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5lom
show sticks, 5lom_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
