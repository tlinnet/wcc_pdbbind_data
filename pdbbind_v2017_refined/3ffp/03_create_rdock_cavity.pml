load 02_3ffp.mol2
load 3ffp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ffp
show sticks, 3ffp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
