load 02_4keq.mol2
load 4keq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4keq
show sticks, 4keq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
