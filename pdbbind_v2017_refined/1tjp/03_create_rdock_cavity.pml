load 02_1tjp.mol2
load 1tjp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1tjp
show sticks, 1tjp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
