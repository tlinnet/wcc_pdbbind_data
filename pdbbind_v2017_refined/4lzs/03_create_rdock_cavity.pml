load 02_4lzs.mol2
load 4lzs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4lzs
show sticks, 4lzs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
