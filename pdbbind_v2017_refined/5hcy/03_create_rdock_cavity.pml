load 02_5hcy.mol2
load 5hcy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5hcy
show sticks, 5hcy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
