load 02_4je7.mol2
load 4je7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4je7
show sticks, 4je7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
