load 02_1m5w.mol2
load 1m5w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1m5w
show sticks, 1m5w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
