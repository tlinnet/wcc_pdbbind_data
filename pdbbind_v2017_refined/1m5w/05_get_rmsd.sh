#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1m5w_ligand.sdf 04_out_1m5w.sd -o 04_out_1m5w_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1m5w_rmsd.sd | tee 05_get_rmsd.score
