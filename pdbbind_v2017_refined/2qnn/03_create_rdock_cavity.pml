load 02_2qnn.mol2
load 2qnn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qnn
show sticks, 2qnn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
