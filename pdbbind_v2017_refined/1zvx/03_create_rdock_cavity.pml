load 02_1zvx.mol2
load 1zvx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1zvx
show sticks, 1zvx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
