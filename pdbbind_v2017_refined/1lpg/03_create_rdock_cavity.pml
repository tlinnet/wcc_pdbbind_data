load 02_1lpg.mol2
load 1lpg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lpg
show sticks, 1lpg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
