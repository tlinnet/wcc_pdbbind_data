load 02_3s76.mol2
load 3s76_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3s76
show sticks, 3s76_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
