load 02_1nhz.mol2
load 1nhz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1nhz
show sticks, 1nhz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
