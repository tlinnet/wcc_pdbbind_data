load 02_2za0.mol2
load 2za0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2za0
show sticks, 2za0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
