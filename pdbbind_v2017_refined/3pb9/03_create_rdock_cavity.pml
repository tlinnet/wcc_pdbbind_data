load 02_3pb9.mol2
load 3pb9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3pb9
show sticks, 3pb9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
