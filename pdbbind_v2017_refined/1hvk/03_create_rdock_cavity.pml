load 02_1hvk.mol2
load 1hvk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hvk
show sticks, 1hvk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
