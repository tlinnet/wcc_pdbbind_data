load 02_3ckb.mol2
load 3ckb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ckb
show sticks, 3ckb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
