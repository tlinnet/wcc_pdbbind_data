load 02_3aaq.mol2
load 3aaq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3aaq
show sticks, 3aaq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
