load 02_4er1.mol2
load 4er1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4er1
show sticks, 4er1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
