load 02_4ynl.mol2
load 4ynl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ynl
show sticks, 4ynl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
