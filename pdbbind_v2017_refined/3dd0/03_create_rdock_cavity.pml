load 02_3dd0.mol2
load 3dd0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3dd0
show sticks, 3dd0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
