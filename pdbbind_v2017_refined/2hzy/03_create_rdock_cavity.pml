load 02_2hzy.mol2
load 2hzy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2hzy
show sticks, 2hzy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
