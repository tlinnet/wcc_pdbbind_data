load 02_2pog.mol2
load 2pog_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pog
show sticks, 2pog_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
