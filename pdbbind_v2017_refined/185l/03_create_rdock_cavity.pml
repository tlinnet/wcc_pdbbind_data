load 02_185l.mol2
load 185l_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_185l
show sticks, 185l_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
