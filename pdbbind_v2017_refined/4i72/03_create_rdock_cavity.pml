load 02_4i72.mol2
load 4i72_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4i72
show sticks, 4i72_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
