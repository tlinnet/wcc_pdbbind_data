load 02_1fzq.mol2
load 1fzq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fzq
show sticks, 1fzq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
