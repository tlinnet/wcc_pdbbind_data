load 02_3dgo.mol2
load 3dgo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3dgo
show sticks, 3dgo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
