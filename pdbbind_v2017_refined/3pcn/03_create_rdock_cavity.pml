load 02_3pcn.mol2
load 3pcn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3pcn
show sticks, 3pcn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
