#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4abh_ligand.sdf 04_out_4abh.sd -o 04_out_4abh_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4abh_rmsd.sd | tee 05_get_rmsd.score
