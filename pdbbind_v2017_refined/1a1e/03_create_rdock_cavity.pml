load 02_1a1e.mol2
load 1a1e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1a1e
show sticks, 1a1e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
