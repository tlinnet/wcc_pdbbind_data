load 02_1q8t.mol2
load 1q8t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1q8t
show sticks, 1q8t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
