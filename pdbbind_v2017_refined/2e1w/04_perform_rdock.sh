#!/usr/bin/env bash

rbdock -i 2e1w_ligand.sdf -o 04_out_2e1w -r 03_create_rdock_cavity.prm -p dock.prm -n 1 | tee 04_perform_rdock.log

echo $SECONDS > 04_perform_rdock.sec
times > 04_perform_rdock.time

