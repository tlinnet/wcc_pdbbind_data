load 02_2e1w.mol2
load 2e1w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2e1w
show sticks, 2e1w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
