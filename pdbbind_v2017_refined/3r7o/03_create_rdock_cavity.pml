load 02_3r7o.mol2
load 3r7o_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3r7o
show sticks, 3r7o_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
