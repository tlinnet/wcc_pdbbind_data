load 02_1i9n.mol2
load 1i9n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1i9n
show sticks, 1i9n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
