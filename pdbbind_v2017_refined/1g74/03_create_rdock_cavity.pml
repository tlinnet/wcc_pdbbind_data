load 02_1g74.mol2
load 1g74_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1g74
show sticks, 1g74_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
