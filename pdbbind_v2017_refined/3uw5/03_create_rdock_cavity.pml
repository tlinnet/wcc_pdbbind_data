load 02_3uw5.mol2
load 3uw5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3uw5
show sticks, 3uw5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
