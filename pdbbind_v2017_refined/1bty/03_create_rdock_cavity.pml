load 02_1bty.mol2
load 1bty_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bty
show sticks, 1bty_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
