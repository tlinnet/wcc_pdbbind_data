load 02_2pgz.mol2
load 2pgz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pgz
show sticks, 2pgz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
