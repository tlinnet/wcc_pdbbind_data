load 02_2jfz.mol2
load 2jfz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2jfz
show sticks, 2jfz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
