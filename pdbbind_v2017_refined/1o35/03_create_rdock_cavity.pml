load 02_1o35.mol2
load 1o35_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1o35
show sticks, 1o35_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
