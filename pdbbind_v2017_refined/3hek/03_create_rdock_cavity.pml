load 02_3hek.mol2
load 3hek_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hek
show sticks, 3hek_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
