load 02_2ewa.mol2
load 2ewa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ewa
show sticks, 2ewa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
