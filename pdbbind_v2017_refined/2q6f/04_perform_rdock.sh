#!/usr/bin/env bash

rbdock -i 2q6f_ligand.sdf -o 04_out_2q6f -r 03_create_rdock_cavity.prm -p dock.prm -n 1 | tee 04_perform_rdock.log

echo $SECONDS > 04_perform_rdock.sec
times > 04_perform_rdock.time

