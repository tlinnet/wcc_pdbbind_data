load 02_3rlb.mol2
load 3rlb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3rlb
show sticks, 3rlb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
