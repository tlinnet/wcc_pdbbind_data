load 02_1b38.mol2
load 1b38_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1b38
show sticks, 1b38_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
