load 02_2boh.mol2
load 2boh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2boh
show sticks, 2boh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
