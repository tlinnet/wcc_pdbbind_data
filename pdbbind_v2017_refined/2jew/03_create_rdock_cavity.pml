load 02_2jew.mol2
load 2jew_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2jew
show sticks, 2jew_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
