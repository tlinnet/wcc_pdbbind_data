load 02_5dlx.mol2
load 5dlx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5dlx
show sticks, 5dlx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
