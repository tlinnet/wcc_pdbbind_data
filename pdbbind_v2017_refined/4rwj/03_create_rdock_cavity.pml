load 02_4rwj.mol2
load 4rwj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rwj
show sticks, 4rwj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
