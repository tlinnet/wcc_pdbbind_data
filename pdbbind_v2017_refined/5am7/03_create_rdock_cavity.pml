load 02_5am7.mol2
load 5am7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5am7
show sticks, 5am7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
