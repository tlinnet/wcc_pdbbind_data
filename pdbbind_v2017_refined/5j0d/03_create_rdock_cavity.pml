load 02_5j0d.mol2
load 5j0d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5j0d
show sticks, 5j0d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
