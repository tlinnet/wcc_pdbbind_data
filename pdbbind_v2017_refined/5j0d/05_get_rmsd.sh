#!/usr/bin/env bash

# Get best rmsd
sdrmsd 5j0d_ligand.sdf 04_out_5j0d.sd -o 04_out_5j0d_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_5j0d_rmsd.sd | tee 05_get_rmsd.score
