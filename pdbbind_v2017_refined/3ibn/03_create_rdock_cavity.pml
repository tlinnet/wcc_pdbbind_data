load 02_3ibn.mol2
load 3ibn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ibn
show sticks, 3ibn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
