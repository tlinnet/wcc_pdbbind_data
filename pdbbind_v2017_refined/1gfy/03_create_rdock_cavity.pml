load 02_1gfy.mol2
load 1gfy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1gfy
show sticks, 1gfy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
