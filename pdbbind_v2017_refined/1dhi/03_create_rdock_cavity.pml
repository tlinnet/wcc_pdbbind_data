load 02_1dhi.mol2
load 1dhi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1dhi
show sticks, 1dhi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
