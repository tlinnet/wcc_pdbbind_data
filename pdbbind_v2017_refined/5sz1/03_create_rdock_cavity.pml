load 02_5sz1.mol2
load 5sz1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5sz1
show sticks, 5sz1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
