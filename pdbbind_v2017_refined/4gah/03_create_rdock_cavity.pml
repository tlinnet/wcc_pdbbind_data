load 02_4gah.mol2
load 4gah_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4gah
show sticks, 4gah_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
