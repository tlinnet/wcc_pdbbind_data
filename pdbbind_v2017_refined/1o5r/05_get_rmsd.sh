#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1o5r_ligand.sdf 04_out_1o5r.sd -o 04_out_1o5r_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1o5r_rmsd.sd | tee 05_get_rmsd.score
