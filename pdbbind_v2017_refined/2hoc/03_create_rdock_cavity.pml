load 02_2hoc.mol2
load 2hoc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2hoc
show sticks, 2hoc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
