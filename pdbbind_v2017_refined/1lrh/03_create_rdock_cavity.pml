load 02_1lrh.mol2
load 1lrh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lrh
show sticks, 1lrh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
