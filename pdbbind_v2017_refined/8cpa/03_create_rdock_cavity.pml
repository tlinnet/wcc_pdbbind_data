load 02_8cpa.mol2
load 8cpa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_8cpa
show sticks, 8cpa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
