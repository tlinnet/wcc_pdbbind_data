load 02_2yay.mol2
load 2yay_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2yay
show sticks, 2yay_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
