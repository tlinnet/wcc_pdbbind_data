load 02_4cd5.mol2
load 4cd5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cd5
show sticks, 4cd5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
