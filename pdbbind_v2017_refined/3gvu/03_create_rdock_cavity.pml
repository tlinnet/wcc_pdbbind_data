load 02_3gvu.mol2
load 3gvu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gvu
show sticks, 3gvu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
