load 02_4cga.mol2
load 4cga_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cga
show sticks, 4cga_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
