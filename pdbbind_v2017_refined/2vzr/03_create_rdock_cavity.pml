load 02_2vzr.mol2
load 2vzr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vzr
show sticks, 2vzr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
