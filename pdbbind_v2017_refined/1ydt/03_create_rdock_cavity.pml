load 02_1ydt.mol2
load 1ydt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ydt
show sticks, 1ydt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
