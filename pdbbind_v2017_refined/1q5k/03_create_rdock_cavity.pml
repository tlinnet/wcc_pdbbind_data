load 02_1q5k.mol2
load 1q5k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1q5k
show sticks, 1q5k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
