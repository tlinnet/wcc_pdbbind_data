load 02_4u5s.mol2
load 4u5s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4u5s
show sticks, 4u5s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
