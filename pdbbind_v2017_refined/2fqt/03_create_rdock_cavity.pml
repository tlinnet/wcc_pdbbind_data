load 02_2fqt.mol2
load 2fqt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2fqt
show sticks, 2fqt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
