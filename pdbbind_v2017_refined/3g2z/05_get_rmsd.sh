#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3g2z_ligand.sdf 04_out_3g2z.sd -o 04_out_3g2z_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3g2z_rmsd.sd | tee 05_get_rmsd.score
