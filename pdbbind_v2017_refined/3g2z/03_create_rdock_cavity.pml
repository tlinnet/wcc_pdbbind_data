load 02_3g2z.mol2
load 3g2z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3g2z
show sticks, 3g2z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
