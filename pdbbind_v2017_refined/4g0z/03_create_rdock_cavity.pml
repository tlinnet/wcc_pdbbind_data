load 02_4g0z.mol2
load 4g0z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4g0z
show sticks, 4g0z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
