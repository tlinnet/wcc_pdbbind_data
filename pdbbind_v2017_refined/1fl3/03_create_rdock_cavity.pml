load 02_1fl3.mol2
load 1fl3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fl3
show sticks, 1fl3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
