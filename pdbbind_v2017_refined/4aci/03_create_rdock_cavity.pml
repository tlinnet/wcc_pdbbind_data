load 02_4aci.mol2
load 4aci_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4aci
show sticks, 4aci_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
