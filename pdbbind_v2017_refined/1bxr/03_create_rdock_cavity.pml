load 02_1bxr.mol2
load 1bxr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bxr
show sticks, 1bxr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
