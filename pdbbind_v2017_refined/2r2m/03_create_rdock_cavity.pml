load 02_2r2m.mol2
load 2r2m_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2r2m
show sticks, 2r2m_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
