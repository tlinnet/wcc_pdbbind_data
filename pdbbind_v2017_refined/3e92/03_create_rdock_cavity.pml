load 02_3e92.mol2
load 3e92_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3e92
show sticks, 3e92_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
