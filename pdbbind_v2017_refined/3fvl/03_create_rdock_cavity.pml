load 02_3fvl.mol2
load 3fvl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3fvl
show sticks, 3fvl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
