load 02_3tfp.mol2
load 3tfp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3tfp
show sticks, 3tfp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
