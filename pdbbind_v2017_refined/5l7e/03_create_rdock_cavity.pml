load 02_5l7e.mol2
load 5l7e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5l7e
show sticks, 5l7e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
