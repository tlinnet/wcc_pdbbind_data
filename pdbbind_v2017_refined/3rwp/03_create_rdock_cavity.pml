load 02_3rwp.mol2
load 3rwp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3rwp
show sticks, 3rwp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
