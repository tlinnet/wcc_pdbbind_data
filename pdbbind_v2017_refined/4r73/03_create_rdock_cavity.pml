load 02_4r73.mol2
load 4r73_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4r73
show sticks, 4r73_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
