load 02_1kui.mol2
load 1kui_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1kui
show sticks, 1kui_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
