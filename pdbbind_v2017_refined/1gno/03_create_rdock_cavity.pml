load 02_1gno.mol2
load 1gno_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1gno
show sticks, 1gno_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
