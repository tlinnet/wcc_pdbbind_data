load 02_3zps.mol2
load 3zps_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3zps
show sticks, 3zps_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
