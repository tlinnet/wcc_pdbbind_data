load 02_5iwg.mol2
load 5iwg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5iwg
show sticks, 5iwg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
