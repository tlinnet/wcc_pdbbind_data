load 02_4jss.mol2
load 4jss_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4jss
show sticks, 4jss_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
