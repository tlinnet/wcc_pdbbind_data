load 02_2qbw.mol2
load 2qbw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qbw
show sticks, 2qbw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
