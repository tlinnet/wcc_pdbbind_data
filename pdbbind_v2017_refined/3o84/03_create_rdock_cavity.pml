load 02_3o84.mol2
load 3o84_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3o84
show sticks, 3o84_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
