load 02_4f39.mol2
load 4f39_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4f39
show sticks, 4f39_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
