#!/usr/bin/env bash

rbdock -i 5k0h_ligand.sdf -o 04_out_5k0h -r 03_create_rdock_cavity.prm -p dock.prm -n 1 | tee 04_perform_rdock.log

echo $SECONDS > 04_perform_rdock.sec
times > 04_perform_rdock.time

