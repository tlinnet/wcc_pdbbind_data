load 02_5cjf.mol2
load 5cjf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5cjf
show sticks, 5cjf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
