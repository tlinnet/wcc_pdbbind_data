load 02_3dcc.mol2
load 3dcc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3dcc
show sticks, 3dcc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
