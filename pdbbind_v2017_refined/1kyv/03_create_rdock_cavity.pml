load 02_1kyv.mol2
load 1kyv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1kyv
show sticks, 1kyv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
