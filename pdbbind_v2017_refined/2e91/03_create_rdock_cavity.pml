load 02_2e91.mol2
load 2e91_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2e91
show sticks, 2e91_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
