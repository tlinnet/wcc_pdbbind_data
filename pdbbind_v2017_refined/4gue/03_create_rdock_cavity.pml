load 02_4gue.mol2
load 4gue_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4gue
show sticks, 4gue_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
