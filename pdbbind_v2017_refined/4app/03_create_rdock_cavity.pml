load 02_4app.mol2
load 4app_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4app
show sticks, 4app_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
