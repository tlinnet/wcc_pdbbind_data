load 02_3r1v.mol2
load 3r1v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3r1v
show sticks, 3r1v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
