load 02_4lbu.mol2
load 4lbu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4lbu
show sticks, 4lbu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
