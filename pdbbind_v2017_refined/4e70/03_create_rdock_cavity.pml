load 02_4e70.mol2
load 4e70_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4e70
show sticks, 4e70_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
