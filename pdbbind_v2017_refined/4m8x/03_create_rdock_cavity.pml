load 02_4m8x.mol2
load 4m8x_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4m8x
show sticks, 4m8x_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
