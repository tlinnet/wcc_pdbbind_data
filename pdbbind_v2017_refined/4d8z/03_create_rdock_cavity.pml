load 02_4d8z.mol2
load 4d8z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4d8z
show sticks, 4d8z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
