load 02_1zhy.mol2
load 1zhy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1zhy
show sticks, 1zhy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
