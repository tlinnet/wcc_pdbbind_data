load 02_3buh.mol2
load 3buh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3buh
show sticks, 3buh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
