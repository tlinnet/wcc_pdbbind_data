load 02_1rp7.mol2
load 1rp7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1rp7
show sticks, 1rp7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
