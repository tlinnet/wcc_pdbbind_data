load 02_4k18.mol2
load 4k18_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4k18
show sticks, 4k18_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
