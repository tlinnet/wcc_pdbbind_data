load 02_3rdq.mol2
load 3rdq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3rdq
show sticks, 3rdq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
