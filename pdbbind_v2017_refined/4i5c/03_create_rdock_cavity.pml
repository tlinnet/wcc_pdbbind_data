load 02_4i5c.mol2
load 4i5c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4i5c
show sticks, 4i5c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
