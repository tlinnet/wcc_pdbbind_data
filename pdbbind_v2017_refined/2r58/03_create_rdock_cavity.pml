load 02_2r58.mol2
load 2r58_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2r58
show sticks, 2r58_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
