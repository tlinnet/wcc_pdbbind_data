load 02_1j4r.mol2
load 1j4r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1j4r
show sticks, 1j4r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
