load 02_2qbu.mol2
load 2qbu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qbu
show sticks, 2qbu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
