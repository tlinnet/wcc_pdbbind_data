#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2wf5_ligand.sdf 04_out_2wf5.sd -o 04_out_2wf5_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2wf5_rmsd.sd | tee 05_get_rmsd.score
