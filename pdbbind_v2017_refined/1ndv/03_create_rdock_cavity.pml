load 02_1ndv.mol2
load 1ndv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ndv
show sticks, 1ndv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
