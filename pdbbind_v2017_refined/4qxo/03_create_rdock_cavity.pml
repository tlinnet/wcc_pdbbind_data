load 02_4qxo.mol2
load 4qxo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qxo
show sticks, 4qxo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
