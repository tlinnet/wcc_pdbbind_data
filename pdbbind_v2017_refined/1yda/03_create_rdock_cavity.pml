load 02_1yda.mol2
load 1yda_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1yda
show sticks, 1yda_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
