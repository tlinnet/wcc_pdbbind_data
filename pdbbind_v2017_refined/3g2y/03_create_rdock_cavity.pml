load 02_3g2y.mol2
load 3g2y_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3g2y
show sticks, 3g2y_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
