load 02_1sdu.mol2
load 1sdu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1sdu
show sticks, 1sdu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
