load 02_2vmc.mol2
load 2vmc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vmc
show sticks, 2vmc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
