load 02_3s2v.mol2
load 3s2v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3s2v
show sticks, 3s2v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
