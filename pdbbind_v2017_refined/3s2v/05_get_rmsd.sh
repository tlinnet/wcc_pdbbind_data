#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3s2v_ligand.sdf 04_out_3s2v.sd -o 04_out_3s2v_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3s2v_rmsd.sd | tee 05_get_rmsd.score
