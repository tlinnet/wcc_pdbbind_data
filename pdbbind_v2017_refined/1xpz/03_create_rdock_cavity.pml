load 02_1xpz.mol2
load 1xpz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1xpz
show sticks, 1xpz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
