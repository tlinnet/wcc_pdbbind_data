load 02_1utj.mol2
load 1utj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1utj
show sticks, 1utj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
