load 02_5l9l.mol2
load 5l9l_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5l9l
show sticks, 5l9l_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
