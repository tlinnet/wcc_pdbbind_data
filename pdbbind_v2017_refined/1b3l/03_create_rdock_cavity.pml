load 02_1b3l.mol2
load 1b3l_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1b3l
show sticks, 1b3l_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
