load 02_1j37.mol2
load 1j37_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1j37
show sticks, 1j37_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
