load 02_4cjr.mol2
load 4cjr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cjr
show sticks, 4cjr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
