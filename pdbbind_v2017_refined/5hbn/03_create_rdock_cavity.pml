load 02_5hbn.mol2
load 5hbn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5hbn
show sticks, 5hbn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
