load 02_3djp.mol2
load 3djp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3djp
show sticks, 3djp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
