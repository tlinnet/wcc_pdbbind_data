load 02_5ev8.mol2
load 5ev8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ev8
show sticks, 5ev8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
