load 02_2fdp.mol2
load 2fdp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2fdp
show sticks, 2fdp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
