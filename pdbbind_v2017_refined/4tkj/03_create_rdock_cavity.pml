load 02_4tkj.mol2
load 4tkj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4tkj
show sticks, 4tkj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
