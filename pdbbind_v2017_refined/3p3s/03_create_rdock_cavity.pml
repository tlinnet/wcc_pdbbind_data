load 02_3p3s.mol2
load 3p3s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3p3s
show sticks, 3p3s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
