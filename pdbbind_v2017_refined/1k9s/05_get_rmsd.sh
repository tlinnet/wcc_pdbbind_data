#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1k9s_ligand.sdf 04_out_1k9s.sd -o 04_out_1k9s_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1k9s_rmsd.sd | tee 05_get_rmsd.score
