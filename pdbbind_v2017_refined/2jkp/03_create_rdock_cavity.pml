load 02_2jkp.mol2
load 2jkp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2jkp
show sticks, 2jkp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
