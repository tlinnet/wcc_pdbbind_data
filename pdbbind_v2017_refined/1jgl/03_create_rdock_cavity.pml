load 02_1jgl.mol2
load 1jgl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1jgl
show sticks, 1jgl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
