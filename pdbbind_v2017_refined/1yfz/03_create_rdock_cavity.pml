load 02_1yfz.mol2
load 1yfz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1yfz
show sticks, 1yfz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
