load 02_1nfw.mol2
load 1nfw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1nfw
show sticks, 1nfw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
