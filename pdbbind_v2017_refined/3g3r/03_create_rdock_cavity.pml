load 02_3g3r.mol2
load 3g3r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3g3r
show sticks, 3g3r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
