load 02_4isu.mol2
load 4isu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4isu
show sticks, 4isu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
