load 02_3pww.mol2
load 3pww_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3pww
show sticks, 3pww_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
