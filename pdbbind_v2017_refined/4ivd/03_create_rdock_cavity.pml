load 02_4ivd.mol2
load 4ivd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ivd
show sticks, 4ivd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
