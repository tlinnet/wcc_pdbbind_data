load 02_5ftg.mol2
load 5ftg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ftg
show sticks, 5ftg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
