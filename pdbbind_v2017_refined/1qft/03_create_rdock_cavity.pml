load 02_1qft.mol2
load 1qft_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qft
show sticks, 1qft_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
