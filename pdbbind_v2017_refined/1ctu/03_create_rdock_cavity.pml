load 02_1ctu.mol2
load 1ctu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ctu
show sticks, 1ctu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
