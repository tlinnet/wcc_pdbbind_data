load 02_4tkb.mol2
load 4tkb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4tkb
show sticks, 4tkb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
