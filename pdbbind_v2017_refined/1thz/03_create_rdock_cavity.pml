load 02_1thz.mol2
load 1thz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1thz
show sticks, 1thz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
