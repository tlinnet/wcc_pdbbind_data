load 02_1mfa.mol2
load 1mfa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1mfa
show sticks, 1mfa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
