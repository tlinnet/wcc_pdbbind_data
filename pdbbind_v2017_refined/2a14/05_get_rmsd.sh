#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2a14_ligand.sdf 04_out_2a14.sd -o 04_out_2a14_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2a14_rmsd.sd | tee 05_get_rmsd.score
