load 02_1erb.mol2
load 1erb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1erb
show sticks, 1erb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
