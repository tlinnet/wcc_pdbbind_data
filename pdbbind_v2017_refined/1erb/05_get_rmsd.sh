#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1erb_ligand.sdf 04_out_1erb.sd -o 04_out_1erb_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1erb_rmsd.sd | tee 05_get_rmsd.score
