load 02_3nyx.mol2
load 3nyx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nyx
show sticks, 3nyx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
