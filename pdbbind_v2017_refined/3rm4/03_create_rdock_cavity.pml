load 02_3rm4.mol2
load 3rm4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3rm4
show sticks, 3rm4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
