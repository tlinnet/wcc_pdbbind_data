load 02_1gjc.mol2
load 1gjc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1gjc
show sticks, 1gjc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
