#!/usr/bin/env bash

rbdock -i 1hfs_ligand.sdf -o 04_out_1hfs -r 03_create_rdock_cavity.prm -p dock.prm -n 1 | tee 04_perform_rdock.log

echo $SECONDS > 04_perform_rdock.sec
times > 04_perform_rdock.time

