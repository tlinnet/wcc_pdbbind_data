load 02_3dp9.mol2
load 3dp9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3dp9
show sticks, 3dp9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
