load 02_3ppq.mol2
load 3ppq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ppq
show sticks, 3ppq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
