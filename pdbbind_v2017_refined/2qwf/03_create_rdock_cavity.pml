load 02_2qwf.mol2
load 2qwf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qwf
show sticks, 2qwf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
