load 02_2ygf.mol2
load 2ygf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ygf
show sticks, 2ygf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
