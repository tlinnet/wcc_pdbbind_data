load 02_5kcb.mol2
load 5kcb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5kcb
show sticks, 5kcb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
