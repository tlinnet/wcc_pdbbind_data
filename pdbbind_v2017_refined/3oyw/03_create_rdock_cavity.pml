load 02_3oyw.mol2
load 3oyw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3oyw
show sticks, 3oyw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
