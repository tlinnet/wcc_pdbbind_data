load 02_4k55.mol2
load 4k55_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4k55
show sticks, 4k55_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
