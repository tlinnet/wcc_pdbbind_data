load 02_3ffg.mol2
load 3ffg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ffg
show sticks, 3ffg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
