load 02_1hk4.mol2
load 1hk4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hk4
show sticks, 1hk4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
