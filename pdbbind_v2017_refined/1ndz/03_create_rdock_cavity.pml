load 02_1ndz.mol2
load 1ndz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ndz
show sticks, 1ndz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
