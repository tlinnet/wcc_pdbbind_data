load 02_1fkh.mol2
load 1fkh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fkh
show sticks, 1fkh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
