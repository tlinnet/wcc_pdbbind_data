load 02_2aqu.mol2
load 2aqu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2aqu
show sticks, 2aqu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
