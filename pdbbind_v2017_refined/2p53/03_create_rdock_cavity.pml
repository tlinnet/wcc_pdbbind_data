load 02_2p53.mol2
load 2p53_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2p53
show sticks, 2p53_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
