load 02_2evl.mol2
load 2evl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2evl
show sticks, 2evl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
