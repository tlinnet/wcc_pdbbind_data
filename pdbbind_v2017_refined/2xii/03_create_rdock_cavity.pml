load 02_2xii.mol2
load 2xii_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xii
show sticks, 2xii_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
