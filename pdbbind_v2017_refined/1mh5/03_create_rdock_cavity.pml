load 02_1mh5.mol2
load 1mh5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1mh5
show sticks, 1mh5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
