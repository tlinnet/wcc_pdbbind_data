load 02_3gbe.mol2
load 3gbe_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gbe
show sticks, 3gbe_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
