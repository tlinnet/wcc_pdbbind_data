load 02_4o2b.mol2
load 4o2b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4o2b
show sticks, 4o2b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
