load 02_4nue.mol2
load 4nue_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nue
show sticks, 4nue_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
