load 02_5dkn.mol2
load 5dkn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5dkn
show sticks, 5dkn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
