load 02_4io4.mol2
load 4io4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4io4
show sticks, 4io4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
