load 02_1zdp.mol2
load 1zdp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1zdp
show sticks, 1zdp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
