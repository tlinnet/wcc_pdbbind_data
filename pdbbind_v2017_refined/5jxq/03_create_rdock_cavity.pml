load 02_5jxq.mol2
load 5jxq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5jxq
show sticks, 5jxq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
