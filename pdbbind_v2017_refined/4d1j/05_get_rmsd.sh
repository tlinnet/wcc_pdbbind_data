#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4d1j_ligand.sdf 04_out_4d1j.sd -o 04_out_4d1j_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4d1j_rmsd.sd | tee 05_get_rmsd.score
