load 02_1hpv.mol2
load 1hpv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hpv
show sticks, 1hpv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
