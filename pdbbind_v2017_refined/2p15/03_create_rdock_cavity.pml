load 02_2p15.mol2
load 2p15_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2p15
show sticks, 2p15_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
