load 02_4msc.mol2
load 4msc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4msc
show sticks, 4msc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
