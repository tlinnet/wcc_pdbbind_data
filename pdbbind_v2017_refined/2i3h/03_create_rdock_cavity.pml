load 02_2i3h.mol2
load 2i3h_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i3h
show sticks, 2i3h_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
