load 02_4k7o.mol2
load 4k7o_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4k7o
show sticks, 4k7o_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
