load 02_3ozs.mol2
load 3ozs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ozs
show sticks, 3ozs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
