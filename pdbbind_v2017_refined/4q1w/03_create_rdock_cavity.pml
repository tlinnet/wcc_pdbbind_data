load 02_4q1w.mol2
load 4q1w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4q1w
show sticks, 4q1w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
