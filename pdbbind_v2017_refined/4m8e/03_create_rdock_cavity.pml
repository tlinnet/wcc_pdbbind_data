load 02_4m8e.mol2
load 4m8e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4m8e
show sticks, 4m8e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
