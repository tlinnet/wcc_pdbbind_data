load 02_1n4h.mol2
load 1n4h_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1n4h
show sticks, 1n4h_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
