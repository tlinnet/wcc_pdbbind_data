#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4uja_ligand.sdf 04_out_4uja.sd -o 04_out_4uja_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4uja_rmsd.sd | tee 05_get_rmsd.score
