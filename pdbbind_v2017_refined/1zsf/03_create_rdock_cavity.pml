load 02_1zsf.mol2
load 1zsf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1zsf
show sticks, 1zsf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
