load 02_4a4w.mol2
load 4a4w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4a4w
show sticks, 4a4w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
