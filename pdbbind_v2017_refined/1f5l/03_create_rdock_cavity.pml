load 02_1f5l.mol2
load 1f5l_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1f5l
show sticks, 1f5l_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
