load 02_5ahw.mol2
load 5ahw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ahw
show sticks, 5ahw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
