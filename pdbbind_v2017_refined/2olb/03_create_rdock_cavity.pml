load 02_2olb.mol2
load 2olb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2olb
show sticks, 2olb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
