load 02_2w5g.mol2
load 2w5g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2w5g
show sticks, 2w5g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
