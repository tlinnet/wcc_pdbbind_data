load 02_2oxd.mol2
load 2oxd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2oxd
show sticks, 2oxd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
