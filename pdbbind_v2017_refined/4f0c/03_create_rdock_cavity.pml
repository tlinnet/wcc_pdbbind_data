load 02_4f0c.mol2
load 4f0c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4f0c
show sticks, 4f0c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
