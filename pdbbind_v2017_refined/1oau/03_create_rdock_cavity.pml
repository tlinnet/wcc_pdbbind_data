load 02_1oau.mol2
load 1oau_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1oau
show sticks, 1oau_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
