load 02_4nze.mol2
load 4nze_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nze
show sticks, 4nze_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
