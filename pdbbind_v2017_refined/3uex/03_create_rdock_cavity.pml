load 02_3uex.mol2
load 3uex_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3uex
show sticks, 3uex_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
