load 02_1jlr.mol2
load 1jlr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1jlr
show sticks, 1jlr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
