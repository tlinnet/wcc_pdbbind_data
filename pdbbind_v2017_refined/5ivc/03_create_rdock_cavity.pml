load 02_5ivc.mol2
load 5ivc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ivc
show sticks, 5ivc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
