load 02_4g95.mol2
load 4g95_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4g95
show sticks, 4g95_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
