load 02_4agl.mol2
load 4agl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4agl
show sticks, 4agl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
