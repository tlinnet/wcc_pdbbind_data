load 02_1o33.mol2
load 1o33_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1o33
show sticks, 1o33_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
