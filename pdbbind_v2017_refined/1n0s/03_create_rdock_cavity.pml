load 02_1n0s.mol2
load 1n0s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1n0s
show sticks, 1n0s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
