load 02_4fht.mol2
load 4fht_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4fht
show sticks, 4fht_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
