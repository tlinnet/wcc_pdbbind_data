load 02_4ew3.mol2
load 4ew3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ew3
show sticks, 4ew3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
