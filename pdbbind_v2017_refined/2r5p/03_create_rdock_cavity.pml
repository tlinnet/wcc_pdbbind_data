load 02_2r5p.mol2
load 2r5p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2r5p
show sticks, 2r5p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
