load 02_4r59.mol2
load 4r59_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4r59
show sticks, 4r59_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
