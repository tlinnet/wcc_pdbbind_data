load 02_1usi.mol2
load 1usi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1usi
show sticks, 1usi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
