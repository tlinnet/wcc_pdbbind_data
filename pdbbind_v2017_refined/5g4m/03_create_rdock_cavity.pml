load 02_5g4m.mol2
load 5g4m_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5g4m
show sticks, 5g4m_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
