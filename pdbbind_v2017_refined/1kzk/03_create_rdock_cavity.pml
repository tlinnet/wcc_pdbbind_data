load 02_1kzk.mol2
load 1kzk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1kzk
show sticks, 1kzk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
