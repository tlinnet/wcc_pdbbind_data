load 02_4pox.mol2
load 4pox_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pox
show sticks, 4pox_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
