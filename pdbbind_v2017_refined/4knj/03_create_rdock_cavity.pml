load 02_4knj.mol2
load 4knj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4knj
show sticks, 4knj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
