load 02_4uoh.mol2
load 4uoh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4uoh
show sticks, 4uoh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
