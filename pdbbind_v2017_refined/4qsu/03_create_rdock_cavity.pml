load 02_4qsu.mol2
load 4qsu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qsu
show sticks, 4qsu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
