load 02_2wej.mol2
load 2wej_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wej
show sticks, 2wej_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
