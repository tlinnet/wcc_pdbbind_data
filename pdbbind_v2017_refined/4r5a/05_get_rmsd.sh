#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4r5a_ligand.sdf 04_out_4r5a.sd -o 04_out_4r5a_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4r5a_rmsd.sd | tee 05_get_rmsd.score
