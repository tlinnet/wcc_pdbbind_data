load 02_4oct.mol2
load 4oct_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4oct
show sticks, 4oct_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
