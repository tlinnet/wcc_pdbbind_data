load 02_4mnp.mol2
load 4mnp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4mnp
show sticks, 4mnp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
