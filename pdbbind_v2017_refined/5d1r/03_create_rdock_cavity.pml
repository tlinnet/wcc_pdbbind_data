load 02_5d1r.mol2
load 5d1r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5d1r
show sticks, 5d1r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
