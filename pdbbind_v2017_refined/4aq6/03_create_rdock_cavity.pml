load 02_4aq6.mol2
load 4aq6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4aq6
show sticks, 4aq6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
