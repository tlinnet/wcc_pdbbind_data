load 02_3o7u.mol2
load 3o7u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3o7u
show sticks, 3o7u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
