load 02_4ido.mol2
load 4ido_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ido
show sticks, 4ido_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
