load 02_2vsl.mol2
load 2vsl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vsl
show sticks, 2vsl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
