load 02_2whp.mol2
load 2whp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2whp
show sticks, 2whp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
