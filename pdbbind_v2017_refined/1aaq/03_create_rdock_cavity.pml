load 02_1aaq.mol2
load 1aaq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1aaq
show sticks, 1aaq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
