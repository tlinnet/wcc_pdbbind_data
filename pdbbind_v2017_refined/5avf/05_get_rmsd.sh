#!/usr/bin/env bash

# Get best rmsd
sdrmsd 5avf_ligand.sdf 04_out_5avf.sd -o 04_out_5avf_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_5avf_rmsd.sd | tee 05_get_rmsd.score
