#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1b5j_ligand.sdf 04_out_1b5j.sd -o 04_out_1b5j_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1b5j_rmsd.sd | tee 05_get_rmsd.score
