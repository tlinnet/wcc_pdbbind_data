load 02_4ymb.mol2
load 4ymb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ymb
show sticks, 4ymb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
