#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4ymb_ligand.sdf 04_out_4ymb.sd -o 04_out_4ymb_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4ymb_rmsd.sd | tee 05_get_rmsd.score
