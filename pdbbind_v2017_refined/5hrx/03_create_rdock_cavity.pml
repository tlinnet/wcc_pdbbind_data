load 02_5hrx.mol2
load 5hrx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5hrx
show sticks, 5hrx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
