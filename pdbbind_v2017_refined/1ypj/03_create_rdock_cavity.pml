load 02_1ypj.mol2
load 1ypj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ypj
show sticks, 1ypj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
