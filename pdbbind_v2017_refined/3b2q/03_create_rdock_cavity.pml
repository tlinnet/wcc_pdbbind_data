load 02_3b2q.mol2
load 3b2q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3b2q
show sticks, 3b2q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
