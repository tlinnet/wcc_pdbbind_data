load 02_2nmx.mol2
load 2nmx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2nmx
show sticks, 2nmx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
