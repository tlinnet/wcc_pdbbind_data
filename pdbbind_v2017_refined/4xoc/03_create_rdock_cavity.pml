load 02_4xoc.mol2
load 4xoc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4xoc
show sticks, 4xoc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
