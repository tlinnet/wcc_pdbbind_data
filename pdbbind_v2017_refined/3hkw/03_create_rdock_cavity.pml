load 02_3hkw.mol2
load 3hkw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hkw
show sticks, 3hkw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
