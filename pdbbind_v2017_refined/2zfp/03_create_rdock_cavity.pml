load 02_2zfp.mol2
load 2zfp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zfp
show sticks, 2zfp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
