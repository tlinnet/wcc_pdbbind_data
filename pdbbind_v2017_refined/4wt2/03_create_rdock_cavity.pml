load 02_4wt2.mol2
load 4wt2_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4wt2
show sticks, 4wt2_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
