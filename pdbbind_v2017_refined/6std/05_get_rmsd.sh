#!/usr/bin/env bash

# Get best rmsd
sdrmsd 6std_ligand.sdf 04_out_6std.sd -o 04_out_6std_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_6std_rmsd.sd | tee 05_get_rmsd.score
