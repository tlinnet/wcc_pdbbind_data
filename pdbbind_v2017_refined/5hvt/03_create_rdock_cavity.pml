load 02_5hvt.mol2
load 5hvt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5hvt
show sticks, 5hvt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
