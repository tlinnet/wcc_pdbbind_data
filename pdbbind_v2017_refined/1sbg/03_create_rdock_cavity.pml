load 02_1sbg.mol2
load 1sbg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1sbg
show sticks, 1sbg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
