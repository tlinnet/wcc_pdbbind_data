load 02_5h9r.mol2
load 5h9r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5h9r
show sticks, 5h9r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
