load 02_1f0u.mol2
load 1f0u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1f0u
show sticks, 1f0u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
