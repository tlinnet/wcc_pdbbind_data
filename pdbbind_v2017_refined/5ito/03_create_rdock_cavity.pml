load 02_5ito.mol2
load 5ito_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ito
show sticks, 5ito_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
