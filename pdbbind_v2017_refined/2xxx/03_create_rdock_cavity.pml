load 02_2xxx.mol2
load 2xxx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xxx
show sticks, 2xxx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
