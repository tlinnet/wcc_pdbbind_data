load 02_1jmg.mol2
load 1jmg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1jmg
show sticks, 1jmg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
