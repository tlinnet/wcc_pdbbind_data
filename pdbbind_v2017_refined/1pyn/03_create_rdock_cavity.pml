load 02_1pyn.mol2
load 1pyn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pyn
show sticks, 1pyn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
