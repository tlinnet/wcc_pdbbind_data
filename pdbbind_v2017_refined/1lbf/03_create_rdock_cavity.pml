load 02_1lbf.mol2
load 1lbf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lbf
show sticks, 1lbf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
