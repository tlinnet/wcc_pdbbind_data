load 02_3t09.mol2
load 3t09_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3t09
show sticks, 3t09_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
