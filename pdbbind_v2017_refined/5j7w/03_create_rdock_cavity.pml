load 02_5j7w.mol2
load 5j7w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5j7w
show sticks, 5j7w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
