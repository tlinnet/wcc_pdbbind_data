load 02_4fz3.mol2
load 4fz3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4fz3
show sticks, 4fz3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
