load 02_4bah.mol2
load 4bah_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4bah
show sticks, 4bah_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
