load 02_1m1b.mol2
load 1m1b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1m1b
show sticks, 1m1b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
