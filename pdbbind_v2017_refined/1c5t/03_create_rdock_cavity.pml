load 02_1c5t.mol2
load 1c5t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1c5t
show sticks, 1c5t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
