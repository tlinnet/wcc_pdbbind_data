#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3bl1_ligand.sdf 04_out_3bl1.sd -o 04_out_3bl1_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3bl1_rmsd.sd | tee 05_get_rmsd.score
