load 02_1ype.mol2
load 1ype_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ype
show sticks, 1ype_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
