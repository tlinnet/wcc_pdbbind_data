load 02_3pwd.mol2
load 3pwd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3pwd
show sticks, 3pwd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
