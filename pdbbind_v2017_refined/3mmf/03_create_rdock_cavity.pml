load 02_3mmf.mol2
load 3mmf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3mmf
show sticks, 3mmf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
