load 02_3r4p.mol2
load 3r4p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3r4p
show sticks, 3r4p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
