load 02_4lvt.mol2
load 4lvt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4lvt
show sticks, 4lvt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
