load 02_2q2a.mol2
load 2q2a_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2q2a
show sticks, 2q2a_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
