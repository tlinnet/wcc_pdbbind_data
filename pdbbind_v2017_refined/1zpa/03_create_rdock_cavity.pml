load 02_1zpa.mol2
load 1zpa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1zpa
show sticks, 1zpa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
