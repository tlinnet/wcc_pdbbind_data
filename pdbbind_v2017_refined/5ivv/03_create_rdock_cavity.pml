load 02_5ivv.mol2
load 5ivv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ivv
show sticks, 5ivv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
