load 02_1t32.mol2
load 1t32_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1t32
show sticks, 1t32_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
