load 02_3gx0.mol2
load 3gx0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gx0
show sticks, 3gx0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
