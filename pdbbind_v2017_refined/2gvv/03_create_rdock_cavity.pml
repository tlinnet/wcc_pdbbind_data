load 02_2gvv.mol2
load 2gvv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2gvv
show sticks, 2gvv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
