load 02_3ap4.mol2
load 3ap4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ap4
show sticks, 3ap4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
