load 02_1hi5.mol2
load 1hi5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hi5
show sticks, 1hi5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
