load 02_1sv3.mol2
load 1sv3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1sv3
show sticks, 1sv3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
