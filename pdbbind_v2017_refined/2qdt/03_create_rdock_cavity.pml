load 02_2qdt.mol2
load 2qdt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qdt
show sticks, 2qdt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
