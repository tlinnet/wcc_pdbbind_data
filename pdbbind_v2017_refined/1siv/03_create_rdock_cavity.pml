load 02_1siv.mol2
load 1siv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1siv
show sticks, 1siv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
