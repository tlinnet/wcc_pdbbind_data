load 02_2pbw.mol2
load 2pbw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pbw
show sticks, 2pbw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
