load 02_2fqw.mol2
load 2fqw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2fqw
show sticks, 2fqw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
