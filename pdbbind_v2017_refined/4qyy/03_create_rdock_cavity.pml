load 02_4qyy.mol2
load 4qyy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qyy
show sticks, 4qyy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
