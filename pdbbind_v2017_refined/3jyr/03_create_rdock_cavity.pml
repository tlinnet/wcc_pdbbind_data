load 02_3jyr.mol2
load 3jyr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3jyr
show sticks, 3jyr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
