load 02_3nuo.mol2
load 3nuo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nuo
show sticks, 3nuo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
