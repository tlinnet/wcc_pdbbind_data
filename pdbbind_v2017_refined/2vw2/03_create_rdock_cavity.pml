load 02_2vw2.mol2
load 2vw2_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vw2
show sticks, 2vw2_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
