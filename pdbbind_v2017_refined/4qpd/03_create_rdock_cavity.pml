load 02_4qpd.mol2
load 4qpd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qpd
show sticks, 4qpd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
