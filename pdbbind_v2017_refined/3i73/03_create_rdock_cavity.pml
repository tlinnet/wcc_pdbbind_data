load 02_3i73.mol2
load 3i73_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3i73
show sticks, 3i73_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
