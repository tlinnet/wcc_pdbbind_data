#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4kiu_ligand.sdf 04_out_4kiu.sd -o 04_out_4kiu_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4kiu_rmsd.sd | tee 05_get_rmsd.score
