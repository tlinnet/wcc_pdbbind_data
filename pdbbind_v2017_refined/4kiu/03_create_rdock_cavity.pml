load 02_4kiu.mol2
load 4kiu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4kiu
show sticks, 4kiu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
