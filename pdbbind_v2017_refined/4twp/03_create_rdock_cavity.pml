load 02_4twp.mol2
load 4twp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4twp
show sticks, 4twp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
