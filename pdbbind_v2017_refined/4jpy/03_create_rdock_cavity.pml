load 02_4jpy.mol2
load 4jpy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4jpy
show sticks, 4jpy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
