load 02_3f78.mol2
load 3f78_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3f78
show sticks, 3f78_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
