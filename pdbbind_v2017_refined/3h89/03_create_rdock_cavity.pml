load 02_3h89.mol2
load 3h89_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3h89
show sticks, 3h89_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
