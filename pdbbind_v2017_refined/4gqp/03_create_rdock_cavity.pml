load 02_4gqp.mol2
load 4gqp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4gqp
show sticks, 4gqp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
