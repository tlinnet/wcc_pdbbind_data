load 02_3td4.mol2
load 3td4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3td4
show sticks, 3td4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
