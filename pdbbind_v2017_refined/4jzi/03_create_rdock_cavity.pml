load 02_4jzi.mol2
load 4jzi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4jzi
show sticks, 4jzi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
