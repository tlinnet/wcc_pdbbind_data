load 02_3vjc.mol2
load 3vjc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3vjc
show sticks, 3vjc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
