load 02_3f8f.mol2
load 3f8f_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3f8f
show sticks, 3f8f_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
