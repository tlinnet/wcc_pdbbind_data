load 02_1amw.mol2
load 1amw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1amw
show sticks, 1amw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
