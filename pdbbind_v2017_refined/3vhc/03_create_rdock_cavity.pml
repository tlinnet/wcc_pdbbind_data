load 02_3vhc.mol2
load 3vhc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3vhc
show sticks, 3vhc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
