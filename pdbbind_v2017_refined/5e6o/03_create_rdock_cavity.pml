load 02_5e6o.mol2
load 5e6o_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5e6o
show sticks, 5e6o_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
