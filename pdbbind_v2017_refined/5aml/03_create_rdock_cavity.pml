load 02_5aml.mol2
load 5aml_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5aml
show sticks, 5aml_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
