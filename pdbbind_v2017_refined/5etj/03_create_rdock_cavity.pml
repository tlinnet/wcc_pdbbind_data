load 02_5etj.mol2
load 5etj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5etj
show sticks, 5etj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
