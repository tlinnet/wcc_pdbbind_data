load 02_3nex.mol2
load 3nex_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nex
show sticks, 3nex_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
