load 02_2j95.mol2
load 2j95_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2j95
show sticks, 2j95_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
