load 02_2zq0.mol2
load 2zq0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zq0
show sticks, 2zq0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
