load 02_3lxe.mol2
load 3lxe_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3lxe
show sticks, 3lxe_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
