load 02_3tb6.mol2
load 3tb6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3tb6
show sticks, 3tb6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
