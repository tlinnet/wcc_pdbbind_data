load 02_5flq.mol2
load 5flq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5flq
show sticks, 5flq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
