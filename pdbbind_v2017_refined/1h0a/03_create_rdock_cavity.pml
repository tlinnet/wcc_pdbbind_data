load 02_1h0a.mol2
load 1h0a_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1h0a
show sticks, 1h0a_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
