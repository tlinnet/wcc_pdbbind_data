load 02_4q3t.mol2
load 4q3t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4q3t
show sticks, 4q3t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
