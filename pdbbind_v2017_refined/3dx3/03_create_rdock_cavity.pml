load 02_3dx3.mol2
load 3dx3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3dx3
show sticks, 3dx3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
