load 02_1q8w.mol2
load 1q8w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1q8w
show sticks, 1q8w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
