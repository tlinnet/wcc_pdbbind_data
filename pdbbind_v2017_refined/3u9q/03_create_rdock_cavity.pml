load 02_3u9q.mol2
load 3u9q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3u9q
show sticks, 3u9q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
