load 02_3fv2.mol2
load 3fv2_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3fv2
show sticks, 3fv2_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
