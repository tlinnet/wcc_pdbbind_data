load 02_4xya.mol2
load 4xya_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4xya
show sticks, 4xya_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
