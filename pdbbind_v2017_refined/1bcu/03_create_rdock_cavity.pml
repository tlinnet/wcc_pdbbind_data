load 02_1bcu.mol2
load 1bcu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bcu
show sticks, 1bcu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
