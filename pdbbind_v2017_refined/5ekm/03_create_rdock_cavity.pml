load 02_5ekm.mol2
load 5ekm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ekm
show sticks, 5ekm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
