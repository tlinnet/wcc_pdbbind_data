load 02_3n1c.mol2
load 3n1c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3n1c
show sticks, 3n1c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
