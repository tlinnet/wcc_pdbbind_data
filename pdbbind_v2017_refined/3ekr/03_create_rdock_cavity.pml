load 02_3ekr.mol2
load 3ekr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ekr
show sticks, 3ekr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
