#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4h3j_ligand.sdf 04_out_4h3j.sd -o 04_out_4h3j_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4h3j_rmsd.sd | tee 05_get_rmsd.score
