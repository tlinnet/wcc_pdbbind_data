load 02_1mmq.mol2
load 1mmq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1mmq
show sticks, 1mmq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
