load 02_4o0x.mol2
load 4o0x_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4o0x
show sticks, 4o0x_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
