load 02_3u6h.mol2
load 3u6h_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3u6h
show sticks, 3u6h_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
