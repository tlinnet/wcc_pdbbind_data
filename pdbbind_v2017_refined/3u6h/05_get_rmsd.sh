#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3u6h_ligand.sdf 04_out_3u6h.sd -o 04_out_3u6h_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3u6h_rmsd.sd | tee 05_get_rmsd.score
