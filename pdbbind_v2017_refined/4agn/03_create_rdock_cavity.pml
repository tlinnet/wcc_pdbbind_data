load 02_4agn.mol2
load 4agn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4agn
show sticks, 4agn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
