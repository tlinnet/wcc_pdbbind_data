load 02_1sld.mol2
load 1sld_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1sld
show sticks, 1sld_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
