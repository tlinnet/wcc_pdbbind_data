load 02_4qev.mol2
load 4qev_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qev
show sticks, 4qev_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
