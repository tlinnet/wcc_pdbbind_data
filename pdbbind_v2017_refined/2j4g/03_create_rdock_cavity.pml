load 02_2j4g.mol2
load 2j4g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2j4g
show sticks, 2j4g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
