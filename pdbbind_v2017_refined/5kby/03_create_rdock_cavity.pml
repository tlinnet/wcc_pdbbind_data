load 02_5kby.mol2
load 5kby_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5kby
show sticks, 5kby_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
