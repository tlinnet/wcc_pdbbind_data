load 02_1oif.mol2
load 1oif_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1oif
show sticks, 1oif_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
