#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4mrz_ligand.sdf 04_out_4mrz.sd -o 04_out_4mrz_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4mrz_rmsd.sd | tee 05_get_rmsd.score
