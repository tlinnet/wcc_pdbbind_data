load 02_3ppp.mol2
load 3ppp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ppp
show sticks, 3ppp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
