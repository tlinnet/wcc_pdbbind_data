load 02_4rlu.mol2
load 4rlu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rlu
show sticks, 4rlu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
