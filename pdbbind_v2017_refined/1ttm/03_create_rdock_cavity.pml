load 02_1ttm.mol2
load 1ttm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ttm
show sticks, 1ttm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
