load 02_1w13.mol2
load 1w13_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1w13
show sticks, 1w13_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
