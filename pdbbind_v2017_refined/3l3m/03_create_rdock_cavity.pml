load 02_3l3m.mol2
load 3l3m_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3l3m
show sticks, 3l3m_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
