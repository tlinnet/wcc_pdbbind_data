load 02_2jxr.mol2
load 2jxr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2jxr
show sticks, 2jxr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
