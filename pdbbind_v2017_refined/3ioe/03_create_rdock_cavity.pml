load 02_3ioe.mol2
load 3ioe_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ioe
show sticks, 3ioe_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
