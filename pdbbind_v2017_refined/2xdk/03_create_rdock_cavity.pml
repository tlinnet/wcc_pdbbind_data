load 02_2xdk.mol2
load 2xdk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xdk
show sticks, 2xdk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
