load 02_3kgt.mol2
load 3kgt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kgt
show sticks, 3kgt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
