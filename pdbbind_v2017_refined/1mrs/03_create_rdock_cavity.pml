load 02_1mrs.mol2
load 1mrs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1mrs
show sticks, 1mrs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
