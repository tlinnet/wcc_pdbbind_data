load 02_3aid.mol2
load 3aid_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3aid
show sticks, 3aid_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
