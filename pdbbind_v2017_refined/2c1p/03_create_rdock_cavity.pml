load 02_2c1p.mol2
load 2c1p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2c1p
show sticks, 2c1p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
