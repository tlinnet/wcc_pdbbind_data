load 02_4b34.mol2
load 4b34_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4b34
show sticks, 4b34_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
