load 02_2za5.mol2
load 2za5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2za5
show sticks, 2za5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
