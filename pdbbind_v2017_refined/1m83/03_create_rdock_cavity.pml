load 02_1m83.mol2
load 1m83_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1m83
show sticks, 1m83_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
