load 02_1sh9.mol2
load 1sh9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1sh9
show sticks, 1sh9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
