load 02_3jya.mol2
load 3jya_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3jya
show sticks, 3jya_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
