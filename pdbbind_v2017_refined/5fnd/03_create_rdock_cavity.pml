load 02_5fnd.mol2
load 5fnd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fnd
show sticks, 5fnd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
