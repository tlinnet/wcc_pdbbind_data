load 02_4l4z.mol2
load 4l4z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4l4z
show sticks, 4l4z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
