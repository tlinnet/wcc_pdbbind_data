load 02_4djy.mol2
load 4djy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4djy
show sticks, 4djy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
