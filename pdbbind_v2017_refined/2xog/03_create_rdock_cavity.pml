load 02_2xog.mol2
load 2xog_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xog
show sticks, 2xog_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
