load 02_3ldq.mol2
load 3ldq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ldq
show sticks, 3ldq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
