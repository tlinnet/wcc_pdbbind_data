load 02_1pzp.mol2
load 1pzp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pzp
show sticks, 1pzp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
