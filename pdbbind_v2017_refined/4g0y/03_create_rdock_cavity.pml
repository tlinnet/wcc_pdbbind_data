load 02_4g0y.mol2
load 4g0y_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4g0y
show sticks, 4g0y_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
