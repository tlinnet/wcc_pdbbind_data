load 02_1g53.mol2
load 1g53_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1g53
show sticks, 1g53_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
