load 02_5eq1.mol2
load 5eq1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5eq1
show sticks, 5eq1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
