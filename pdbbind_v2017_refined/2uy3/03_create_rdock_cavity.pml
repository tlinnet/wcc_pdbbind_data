load 02_2uy3.mol2
load 2uy3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2uy3
show sticks, 2uy3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
