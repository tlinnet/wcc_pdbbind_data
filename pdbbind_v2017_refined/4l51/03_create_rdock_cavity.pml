load 02_4l51.mol2
load 4l51_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4l51
show sticks, 4l51_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
