#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2v58_ligand.sdf 04_out_2v58.sd -o 04_out_2v58_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2v58_rmsd.sd | tee 05_get_rmsd.score
