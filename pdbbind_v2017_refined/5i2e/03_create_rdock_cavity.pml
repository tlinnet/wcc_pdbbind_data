load 02_5i2e.mol2
load 5i2e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5i2e
show sticks, 5i2e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
