load 02_4lkk.mol2
load 4lkk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4lkk
show sticks, 4lkk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
