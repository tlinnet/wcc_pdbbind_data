load 02_5d0c.mol2
load 5d0c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5d0c
show sticks, 5d0c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
