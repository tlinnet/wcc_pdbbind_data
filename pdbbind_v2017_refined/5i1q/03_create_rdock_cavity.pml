load 02_5i1q.mol2
load 5i1q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5i1q
show sticks, 5i1q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
