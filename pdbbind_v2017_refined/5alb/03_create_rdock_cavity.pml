load 02_5alb.mol2
load 5alb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5alb
show sticks, 5alb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
