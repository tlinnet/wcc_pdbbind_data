load 02_4ucc.mol2
load 4ucc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ucc
show sticks, 4ucc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
