load 02_1hps.mol2
load 1hps_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hps
show sticks, 1hps_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
