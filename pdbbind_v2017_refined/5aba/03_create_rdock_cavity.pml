load 02_5aba.mol2
load 5aba_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5aba
show sticks, 5aba_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
