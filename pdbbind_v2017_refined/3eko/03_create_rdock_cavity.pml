load 02_3eko.mol2
load 3eko_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3eko
show sticks, 3eko_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
