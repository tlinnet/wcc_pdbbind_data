load 02_1uto.mol2
load 1uto_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1uto
show sticks, 1uto_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
