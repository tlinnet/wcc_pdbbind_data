load 02_1b46.mol2
load 1b46_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1b46
show sticks, 1b46_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
