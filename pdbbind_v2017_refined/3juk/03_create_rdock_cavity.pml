load 02_3juk.mol2
load 3juk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3juk
show sticks, 3juk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
