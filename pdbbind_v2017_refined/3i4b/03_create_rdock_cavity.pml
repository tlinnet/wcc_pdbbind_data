load 02_3i4b.mol2
load 3i4b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3i4b
show sticks, 3i4b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
