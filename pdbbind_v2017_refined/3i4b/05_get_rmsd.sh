#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3i4b_ligand.sdf 04_out_3i4b.sd -o 04_out_3i4b_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3i4b_rmsd.sd | tee 05_get_rmsd.score
