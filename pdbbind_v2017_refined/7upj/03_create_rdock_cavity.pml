load 02_7upj.mol2
load 7upj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_7upj
show sticks, 7upj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
