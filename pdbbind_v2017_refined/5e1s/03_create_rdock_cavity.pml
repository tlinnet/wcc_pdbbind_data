load 02_5e1s.mol2
load 5e1s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5e1s
show sticks, 5e1s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
