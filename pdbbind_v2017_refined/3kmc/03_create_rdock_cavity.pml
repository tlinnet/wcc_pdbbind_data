load 02_3kmc.mol2
load 3kmc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kmc
show sticks, 3kmc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
