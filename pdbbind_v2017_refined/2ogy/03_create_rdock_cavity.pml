load 02_2ogy.mol2
load 2ogy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ogy
show sticks, 2ogy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
