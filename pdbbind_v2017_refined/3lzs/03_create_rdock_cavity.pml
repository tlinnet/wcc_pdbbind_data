load 02_3lzs.mol2
load 3lzs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3lzs
show sticks, 3lzs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
