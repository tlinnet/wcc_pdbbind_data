load 02_3qfd.mol2
load 3qfd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qfd
show sticks, 3qfd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
