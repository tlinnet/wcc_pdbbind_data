load 02_1dl7.mol2
load 1dl7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1dl7
show sticks, 1dl7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
