load 02_2o4r.mol2
load 2o4r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2o4r
show sticks, 2o4r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
