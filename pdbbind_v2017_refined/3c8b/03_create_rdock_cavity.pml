load 02_3c8b.mol2
load 3c8b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3c8b
show sticks, 3c8b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
