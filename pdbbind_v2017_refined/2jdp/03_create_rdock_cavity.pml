load 02_2jdp.mol2
load 2jdp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2jdp
show sticks, 2jdp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
