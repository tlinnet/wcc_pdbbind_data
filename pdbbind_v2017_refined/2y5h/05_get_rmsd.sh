#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2y5h_ligand.sdf 04_out_2y5h.sd -o 04_out_2y5h_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2y5h_rmsd.sd | tee 05_get_rmsd.score
