load 02_4y3y.mol2
load 4y3y_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4y3y
show sticks, 4y3y_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
