load 02_2wtv.mol2
load 2wtv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wtv
show sticks, 2wtv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
