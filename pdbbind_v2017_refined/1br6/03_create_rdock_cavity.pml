load 02_1br6.mol2
load 1br6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1br6
show sticks, 1br6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
