load 02_1g7v.mol2
load 1g7v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1g7v
show sticks, 1g7v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
