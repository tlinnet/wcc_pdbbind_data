load 02_3roc.mol2
load 3roc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3roc
show sticks, 3roc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
