load 02_2jke.mol2
load 2jke_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2jke
show sticks, 2jke_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
