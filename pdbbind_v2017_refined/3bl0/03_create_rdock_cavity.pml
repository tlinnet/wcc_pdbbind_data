load 02_3bl0.mol2
load 3bl0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3bl0
show sticks, 3bl0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
