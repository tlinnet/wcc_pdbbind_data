load 02_2yki.mol2
load 2yki_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2yki
show sticks, 2yki_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
