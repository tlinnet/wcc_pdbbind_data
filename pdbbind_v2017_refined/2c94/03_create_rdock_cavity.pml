load 02_2c94.mol2
load 2c94_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2c94
show sticks, 2c94_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
