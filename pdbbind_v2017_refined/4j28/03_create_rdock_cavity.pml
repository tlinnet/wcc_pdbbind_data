load 02_4j28.mol2
load 4j28_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4j28
show sticks, 4j28_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
