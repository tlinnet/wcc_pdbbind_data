load 02_2i2c.mol2
load 2i2c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i2c
show sticks, 2i2c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
