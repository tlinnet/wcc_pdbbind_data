load 02_4iic.mol2
load 4iic_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4iic
show sticks, 4iic_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
