load 02_2qe4.mol2
load 2qe4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qe4
show sticks, 2qe4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
