load 02_4zzz.mol2
load 4zzz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zzz
show sticks, 4zzz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
