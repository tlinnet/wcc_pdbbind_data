load 02_2ovv.mol2
load 2ovv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ovv
show sticks, 2ovv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
