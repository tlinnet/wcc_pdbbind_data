load 02_4nnr.mol2
load 4nnr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nnr
show sticks, 4nnr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
