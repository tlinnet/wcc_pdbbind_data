load 02_3ddg.mol2
load 3ddg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ddg
show sticks, 3ddg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
