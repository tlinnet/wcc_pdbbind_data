load 02_3zdv.mol2
load 3zdv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3zdv
show sticks, 3zdv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
