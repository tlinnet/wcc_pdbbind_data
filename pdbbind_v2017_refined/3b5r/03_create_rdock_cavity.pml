load 02_3b5r.mol2
load 3b5r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3b5r
show sticks, 3b5r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
