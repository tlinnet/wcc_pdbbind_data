load 02_5khm.mol2
load 5khm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5khm
show sticks, 5khm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
