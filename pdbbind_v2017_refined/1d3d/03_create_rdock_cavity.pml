load 02_1d3d.mol2
load 1d3d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1d3d
show sticks, 1d3d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
