load 02_4rww.mol2
load 4rww_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rww
show sticks, 4rww_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
