#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3kgq_ligand.sdf 04_out_3kgq.sd -o 04_out_3kgq_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3kgq_rmsd.sd | tee 05_get_rmsd.score
