load 02_3kgq.mol2
load 3kgq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kgq
show sticks, 3kgq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
