load 02_1olu.mol2
load 1olu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1olu
show sticks, 1olu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
