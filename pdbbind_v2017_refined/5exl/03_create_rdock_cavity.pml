load 02_5exl.mol2
load 5exl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5exl
show sticks, 5exl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
