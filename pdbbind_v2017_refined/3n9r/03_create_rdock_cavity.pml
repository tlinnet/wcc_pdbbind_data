load 02_3n9r.mol2
load 3n9r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3n9r
show sticks, 3n9r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
