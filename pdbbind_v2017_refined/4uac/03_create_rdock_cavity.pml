load 02_4uac.mol2
load 4uac_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4uac
show sticks, 4uac_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
