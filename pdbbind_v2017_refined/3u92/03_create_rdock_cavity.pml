load 02_3u92.mol2
load 3u92_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3u92
show sticks, 3u92_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
