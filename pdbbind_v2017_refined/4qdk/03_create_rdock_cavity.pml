load 02_4qdk.mol2
load 4qdk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qdk
show sticks, 4qdk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
