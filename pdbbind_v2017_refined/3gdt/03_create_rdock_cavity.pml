load 02_3gdt.mol2
load 3gdt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gdt
show sticks, 3gdt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
