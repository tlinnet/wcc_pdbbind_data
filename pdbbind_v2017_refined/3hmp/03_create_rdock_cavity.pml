load 02_3hmp.mol2
load 3hmp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hmp
show sticks, 3hmp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
