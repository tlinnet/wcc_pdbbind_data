load 02_1pvn.mol2
load 1pvn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pvn
show sticks, 1pvn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
