load 02_4hj2.mol2
load 4hj2_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4hj2
show sticks, 4hj2_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
