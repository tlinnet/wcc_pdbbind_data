load 02_3owj.mol2
load 3owj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3owj
show sticks, 3owj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
