load 02_4m0y.mol2
load 4m0y_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4m0y
show sticks, 4m0y_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
