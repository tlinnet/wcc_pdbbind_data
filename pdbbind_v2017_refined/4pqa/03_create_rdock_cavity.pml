load 02_4pqa.mol2
load 4pqa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pqa
show sticks, 4pqa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
