load 02_1e3g.mol2
load 1e3g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1e3g
show sticks, 1e3g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
