load 02_3t0d.mol2
load 3t0d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3t0d
show sticks, 3t0d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
