load 02_3ime.mol2
load 3ime_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ime
show sticks, 3ime_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
