load 02_1uml.mol2
load 1uml_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1uml
show sticks, 1uml_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
