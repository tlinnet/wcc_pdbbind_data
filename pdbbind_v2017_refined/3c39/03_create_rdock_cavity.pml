load 02_3c39.mol2
load 3c39_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3c39
show sticks, 3c39_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
