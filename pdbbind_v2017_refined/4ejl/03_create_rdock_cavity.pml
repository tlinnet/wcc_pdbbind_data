load 02_4ejl.mol2
load 4ejl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ejl
show sticks, 4ejl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
