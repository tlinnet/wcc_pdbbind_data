#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3v4t_ligand.sdf 04_out_3v4t.sd -o 04_out_3v4t_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3v4t_rmsd.sd | tee 05_get_rmsd.score
