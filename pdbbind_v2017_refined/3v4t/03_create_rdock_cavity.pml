load 02_3v4t.mol2
load 3v4t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3v4t
show sticks, 3v4t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
