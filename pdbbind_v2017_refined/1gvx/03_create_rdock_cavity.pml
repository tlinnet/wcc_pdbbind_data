load 02_1gvx.mol2
load 1gvx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1gvx
show sticks, 1gvx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
