load 02_2cen.mol2
load 2cen_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2cen
show sticks, 2cen_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
