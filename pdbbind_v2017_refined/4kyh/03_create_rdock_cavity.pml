load 02_4kyh.mol2
load 4kyh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4kyh
show sticks, 4kyh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
