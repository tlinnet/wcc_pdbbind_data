load 02_1o38.mol2
load 1o38_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1o38
show sticks, 1o38_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
