load 02_3usx.mol2
load 3usx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3usx
show sticks, 3usx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
