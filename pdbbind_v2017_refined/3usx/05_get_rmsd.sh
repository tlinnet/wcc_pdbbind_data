#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3usx_ligand.sdf 04_out_3usx.sd -o 04_out_3usx_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3usx_rmsd.sd | tee 05_get_rmsd.score
