load 02_3ljz.mol2
load 3ljz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ljz
show sticks, 3ljz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
