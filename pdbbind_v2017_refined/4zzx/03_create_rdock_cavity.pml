load 02_4zzx.mol2
load 4zzx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zzx
show sticks, 4zzx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
