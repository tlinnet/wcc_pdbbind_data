load 02_4n6g.mol2
load 4n6g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4n6g
show sticks, 4n6g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
