load 02_5dgw.mol2
load 5dgw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5dgw
show sticks, 5dgw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
