load 02_10gs.mol2
load 10gs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_10gs
show sticks, 10gs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
