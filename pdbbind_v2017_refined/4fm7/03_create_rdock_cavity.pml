load 02_4fm7.mol2
load 4fm7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4fm7
show sticks, 4fm7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
