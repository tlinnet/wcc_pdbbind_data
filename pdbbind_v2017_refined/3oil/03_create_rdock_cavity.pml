load 02_3oil.mol2
load 3oil_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3oil
show sticks, 3oil_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
