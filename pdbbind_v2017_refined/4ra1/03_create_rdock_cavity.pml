load 02_4ra1.mol2
load 4ra1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ra1
show sticks, 4ra1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
