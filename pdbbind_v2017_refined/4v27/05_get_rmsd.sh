#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4v27_ligand.sdf 04_out_4v27.sd -o 04_out_4v27_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4v27_rmsd.sd | tee 05_get_rmsd.score
