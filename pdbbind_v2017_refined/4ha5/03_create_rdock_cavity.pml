load 02_4ha5.mol2
load 4ha5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ha5
show sticks, 4ha5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
