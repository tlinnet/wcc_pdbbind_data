load 02_4ezx.mol2
load 4ezx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ezx
show sticks, 4ezx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
