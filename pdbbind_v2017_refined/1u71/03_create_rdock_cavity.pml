load 02_1u71.mol2
load 1u71_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1u71
show sticks, 1u71_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
