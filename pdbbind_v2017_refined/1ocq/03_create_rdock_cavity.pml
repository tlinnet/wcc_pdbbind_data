load 02_1ocq.mol2
load 1ocq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ocq
show sticks, 1ocq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
