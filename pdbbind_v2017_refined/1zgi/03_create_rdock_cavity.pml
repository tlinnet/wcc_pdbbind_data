load 02_1zgi.mol2
load 1zgi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1zgi
show sticks, 1zgi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
