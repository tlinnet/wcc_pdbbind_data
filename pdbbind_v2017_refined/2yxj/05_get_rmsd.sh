#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2yxj_ligand.sdf 04_out_2yxj.sd -o 04_out_2yxj_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2yxj_rmsd.sd | tee 05_get_rmsd.score
