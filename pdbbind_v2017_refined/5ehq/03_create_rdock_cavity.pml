load 02_5ehq.mol2
load 5ehq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ehq
show sticks, 5ehq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
