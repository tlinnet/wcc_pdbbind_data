load 02_4erf.mol2
load 4erf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4erf
show sticks, 4erf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
