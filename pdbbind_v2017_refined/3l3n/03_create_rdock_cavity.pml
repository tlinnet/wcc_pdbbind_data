load 02_3l3n.mol2
load 3l3n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3l3n
show sticks, 3l3n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
