load 02_3suw.mol2
load 3suw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3suw
show sticks, 3suw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
