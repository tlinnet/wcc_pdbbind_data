load 02_2hmu.mol2
load 2hmu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2hmu
show sticks, 2hmu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
