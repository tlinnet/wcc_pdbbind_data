#!/usr/bin/env bash

rbdock -i 3n3j_ligand.sdf -o 04_out_3n3j -r 03_create_rdock_cavity.prm -p dock.prm -n 1 | tee 04_perform_rdock.log

echo $SECONDS > 04_perform_rdock.sec
times > 04_perform_rdock.time

