load 02_2jdm.mol2
load 2jdm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2jdm
show sticks, 2jdm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
