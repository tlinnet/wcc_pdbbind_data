load 02_1yej.mol2
load 1yej_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1yej
show sticks, 1yej_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
