load 02_5e7n.mol2
load 5e7n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5e7n
show sticks, 5e7n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
