load 02_5fog.mol2
load 5fog_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fog
show sticks, 5fog_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
