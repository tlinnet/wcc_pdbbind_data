load 02_2xdl.mol2
load 2xdl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xdl
show sticks, 2xdl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
