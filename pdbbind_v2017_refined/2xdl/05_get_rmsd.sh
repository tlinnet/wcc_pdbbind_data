#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2xdl_ligand.sdf 04_out_2xdl.sd -o 04_out_2xdl_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2xdl_rmsd.sd | tee 05_get_rmsd.score
