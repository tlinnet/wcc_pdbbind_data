load 02_1h6h.mol2
load 1h6h_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1h6h
show sticks, 1h6h_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
