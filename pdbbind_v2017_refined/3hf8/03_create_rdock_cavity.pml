load 02_3hf8.mol2
load 3hf8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hf8
show sticks, 3hf8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
