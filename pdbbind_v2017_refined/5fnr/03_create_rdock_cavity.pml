load 02_5fnr.mol2
load 5fnr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fnr
show sticks, 5fnr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
