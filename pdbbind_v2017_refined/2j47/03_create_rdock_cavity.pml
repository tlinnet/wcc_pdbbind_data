load 02_2j47.mol2
load 2j47_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2j47
show sticks, 2j47_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
