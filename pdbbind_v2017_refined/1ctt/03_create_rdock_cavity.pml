load 02_1ctt.mol2
load 1ctt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ctt
show sticks, 1ctt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
