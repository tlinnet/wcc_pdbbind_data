load 02_1njs.mol2
load 1njs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1njs
show sticks, 1njs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
