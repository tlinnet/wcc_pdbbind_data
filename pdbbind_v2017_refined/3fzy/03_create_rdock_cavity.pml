load 02_3fzy.mol2
load 3fzy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3fzy
show sticks, 3fzy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
