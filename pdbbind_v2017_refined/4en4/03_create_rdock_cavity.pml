load 02_4en4.mol2
load 4en4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4en4
show sticks, 4en4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
