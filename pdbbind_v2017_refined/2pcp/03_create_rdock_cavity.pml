load 02_2pcp.mol2
load 2pcp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pcp
show sticks, 2pcp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
