load 02_2xj7.mol2
load 2xj7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xj7
show sticks, 2xj7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
