load 02_2r3t.mol2
load 2r3t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2r3t
show sticks, 2r3t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
