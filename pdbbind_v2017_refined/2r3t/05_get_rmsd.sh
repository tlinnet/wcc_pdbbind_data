#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2r3t_ligand.sdf 04_out_2r3t.sd -o 04_out_2r3t_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2r3t_rmsd.sd | tee 05_get_rmsd.score
