load 02_4zzy.mol2
load 4zzy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zzy
show sticks, 4zzy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
