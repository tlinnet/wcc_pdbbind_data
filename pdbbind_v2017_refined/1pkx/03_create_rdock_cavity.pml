load 02_1pkx.mol2
load 1pkx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pkx
show sticks, 1pkx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
