load 02_5dey.mol2
load 5dey_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5dey
show sticks, 5dey_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
