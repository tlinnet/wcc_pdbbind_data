#!/usr/bin/env bash

# Get best rmsd
sdrmsd 5l4i_ligand.sdf 04_out_5l4i.sd -o 04_out_5l4i_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_5l4i_rmsd.sd | tee 05_get_rmsd.score
