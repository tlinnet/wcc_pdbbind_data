load 02_1ela.mol2
load 1ela_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ela
show sticks, 1ela_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
