load 02_1v2k.mol2
load 1v2k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1v2k
show sticks, 1v2k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
