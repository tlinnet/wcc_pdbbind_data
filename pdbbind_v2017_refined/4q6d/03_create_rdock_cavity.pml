load 02_4q6d.mol2
load 4q6d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4q6d
show sticks, 4q6d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
