load 02_4j7e.mol2
load 4j7e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4j7e
show sticks, 4j7e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
