load 02_5evz.mol2
load 5evz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5evz
show sticks, 5evz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
