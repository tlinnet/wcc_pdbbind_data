load 02_2i4w.mol2
load 2i4w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i4w
show sticks, 2i4w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
