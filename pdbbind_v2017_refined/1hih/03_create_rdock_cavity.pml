load 02_1hih.mol2
load 1hih_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hih
show sticks, 1hih_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
