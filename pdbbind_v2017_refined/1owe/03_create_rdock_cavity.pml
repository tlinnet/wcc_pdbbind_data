load 02_1owe.mol2
load 1owe_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1owe
show sticks, 1owe_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
