load 02_4lov.mol2
load 4lov_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4lov
show sticks, 4lov_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
