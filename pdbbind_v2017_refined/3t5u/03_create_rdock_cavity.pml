load 02_3t5u.mol2
load 3t5u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3t5u
show sticks, 3t5u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
