load 02_2xeg.mol2
load 2xeg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xeg
show sticks, 2xeg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
