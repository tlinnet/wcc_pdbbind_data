load 02_5i9z.mol2
load 5i9z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5i9z
show sticks, 5i9z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
