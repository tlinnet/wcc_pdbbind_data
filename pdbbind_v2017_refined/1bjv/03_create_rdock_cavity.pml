load 02_1bjv.mol2
load 1bjv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bjv
show sticks, 1bjv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
