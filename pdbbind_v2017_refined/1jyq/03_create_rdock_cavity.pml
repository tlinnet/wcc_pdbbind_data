load 02_1jyq.mol2
load 1jyq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1jyq
show sticks, 1jyq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
