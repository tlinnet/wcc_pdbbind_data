load 02_1qka.mol2
load 1qka_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qka
show sticks, 1qka_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
