load 02_2c97.mol2
load 2c97_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2c97
show sticks, 2c97_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
