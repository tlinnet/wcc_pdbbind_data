load 02_5i29.mol2
load 5i29_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5i29
show sticks, 5i29_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
