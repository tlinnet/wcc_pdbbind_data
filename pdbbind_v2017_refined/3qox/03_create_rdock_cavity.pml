load 02_3qox.mol2
load 3qox_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qox
show sticks, 3qox_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
