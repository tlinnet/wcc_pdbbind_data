load 02_2ojg.mol2
load 2ojg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ojg
show sticks, 2ojg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
