load 02_2i3i.mol2
load 2i3i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i3i
show sticks, 2i3i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
