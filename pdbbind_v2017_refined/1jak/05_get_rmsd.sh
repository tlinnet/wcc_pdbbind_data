#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1jak_ligand.sdf 04_out_1jak.sd -o 04_out_1jak_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1jak_rmsd.sd | tee 05_get_rmsd.score
