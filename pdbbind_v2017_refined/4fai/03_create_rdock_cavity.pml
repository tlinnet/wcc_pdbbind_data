load 02_4fai.mol2
load 4fai_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4fai
show sticks, 4fai_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
