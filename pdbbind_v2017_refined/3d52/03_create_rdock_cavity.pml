load 02_3d52.mol2
load 3d52_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3d52
show sticks, 3d52_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
