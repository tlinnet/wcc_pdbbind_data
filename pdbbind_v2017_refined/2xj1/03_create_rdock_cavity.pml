load 02_2xj1.mol2
load 2xj1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xj1
show sticks, 2xj1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
