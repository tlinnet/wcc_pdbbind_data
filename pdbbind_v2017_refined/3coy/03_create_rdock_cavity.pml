load 02_3coy.mol2
load 3coy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3coy
show sticks, 3coy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
