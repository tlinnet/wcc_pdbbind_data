load 02_2i19.mol2
load 2i19_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i19
show sticks, 2i19_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
