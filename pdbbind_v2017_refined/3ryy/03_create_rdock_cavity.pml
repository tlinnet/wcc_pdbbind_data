load 02_3ryy.mol2
load 3ryy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ryy
show sticks, 3ryy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
