load 02_5hct.mol2
load 5hct_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5hct
show sticks, 5hct_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
