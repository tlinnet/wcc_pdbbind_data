load 02_4pee.mol2
load 4pee_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pee
show sticks, 4pee_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
