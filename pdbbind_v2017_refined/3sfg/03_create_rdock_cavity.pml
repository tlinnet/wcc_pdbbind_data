load 02_3sfg.mol2
load 3sfg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3sfg
show sticks, 3sfg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
