load 4h85_protein.pdb

#Remove alternates
cmd.remove("not alt +A")
cmd.valence("guess", selection1='4h85_protein')

#Prepare molecules with polar hydrogens:
cmd.h_add("elem O or elem N or elem S")

#Delete Waters and Heteroatoms
cmd.remove("solvent")
cmd.remove("resn HOH")
cmd.remove("hetatm")

save 01_4h85.pdb
