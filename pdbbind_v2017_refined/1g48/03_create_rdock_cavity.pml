load 02_1g48.mol2
load 1g48_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1g48
show sticks, 1g48_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
