load 02_4der.mol2
load 4der_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4der
show sticks, 4der_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
