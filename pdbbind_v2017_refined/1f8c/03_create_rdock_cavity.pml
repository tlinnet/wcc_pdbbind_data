load 02_1f8c.mol2
load 1f8c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1f8c
show sticks, 1f8c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
