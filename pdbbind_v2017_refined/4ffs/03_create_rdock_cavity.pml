load 02_4ffs.mol2
load 4ffs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ffs
show sticks, 4ffs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
