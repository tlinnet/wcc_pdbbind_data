load 02_4gr8.mol2
load 4gr8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4gr8
show sticks, 4gr8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
