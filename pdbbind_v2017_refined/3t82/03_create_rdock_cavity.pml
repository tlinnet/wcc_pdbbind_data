load 02_3t82.mol2
load 3t82_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3t82
show sticks, 3t82_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
