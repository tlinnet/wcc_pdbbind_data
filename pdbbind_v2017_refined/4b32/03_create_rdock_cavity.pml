load 02_4b32.mol2
load 4b32_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4b32
show sticks, 4b32_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
