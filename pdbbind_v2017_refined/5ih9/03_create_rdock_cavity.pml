load 02_5ih9.mol2
load 5ih9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ih9
show sticks, 5ih9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
