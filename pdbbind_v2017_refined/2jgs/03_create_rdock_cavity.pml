load 02_2jgs.mol2
load 2jgs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2jgs
show sticks, 2jgs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
