load 02_4eo8.mol2
load 4eo8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4eo8
show sticks, 4eo8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
