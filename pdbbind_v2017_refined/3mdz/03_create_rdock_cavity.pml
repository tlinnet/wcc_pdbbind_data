load 02_3mdz.mol2
load 3mdz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3mdz
show sticks, 3mdz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
