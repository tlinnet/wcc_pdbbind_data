load 02_4zx4.mol2
load 4zx4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zx4
show sticks, 4zx4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
