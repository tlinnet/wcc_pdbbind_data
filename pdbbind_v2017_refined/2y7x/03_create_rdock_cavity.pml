load 02_2y7x.mol2
load 2y7x_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2y7x
show sticks, 2y7x_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
