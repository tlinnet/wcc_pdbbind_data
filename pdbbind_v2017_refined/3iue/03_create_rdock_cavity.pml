load 02_3iue.mol2
load 3iue_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3iue
show sticks, 3iue_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
