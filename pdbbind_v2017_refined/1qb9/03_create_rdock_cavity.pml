load 02_1qb9.mol2
load 1qb9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qb9
show sticks, 1qb9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
