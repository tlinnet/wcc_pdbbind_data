load 02_1d6v.mol2
load 1d6v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1d6v
show sticks, 1d6v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
