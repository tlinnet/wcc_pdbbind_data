load 02_5lvl.mol2
load 5lvl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5lvl
show sticks, 5lvl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
