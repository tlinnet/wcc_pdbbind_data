load 02_4q7v.mol2
load 4q7v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4q7v
show sticks, 4q7v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
