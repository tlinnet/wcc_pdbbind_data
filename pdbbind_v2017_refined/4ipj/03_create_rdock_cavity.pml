load 02_4ipj.mol2
load 4ipj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ipj
show sticks, 4ipj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
