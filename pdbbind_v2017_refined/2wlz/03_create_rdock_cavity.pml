load 02_2wlz.mol2
load 2wlz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wlz
show sticks, 2wlz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
