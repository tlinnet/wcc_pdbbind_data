load 02_3eax.mol2
load 3eax_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3eax
show sticks, 3eax_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
