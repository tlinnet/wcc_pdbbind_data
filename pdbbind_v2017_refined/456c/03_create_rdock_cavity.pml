load 02_456c.mol2
load 456c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_456c
show sticks, 456c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
