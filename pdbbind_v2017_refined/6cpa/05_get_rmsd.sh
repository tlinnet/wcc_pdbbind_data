#!/usr/bin/env bash

# Get best rmsd
sdrmsd 6cpa_ligand.sdf 04_out_6cpa.sd -o 04_out_6cpa_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_6cpa_rmsd.sd | tee 05_get_rmsd.score
