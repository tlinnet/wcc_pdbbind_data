load 02_4non.mol2
load 4non_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4non
show sticks, 4non_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
