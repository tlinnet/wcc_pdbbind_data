load 02_4pzv.mol2
load 4pzv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pzv
show sticks, 4pzv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
