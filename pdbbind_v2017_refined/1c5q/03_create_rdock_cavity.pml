load 02_1c5q.mol2
load 1c5q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1c5q
show sticks, 1c5q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
