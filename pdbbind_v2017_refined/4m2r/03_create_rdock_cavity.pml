load 02_4m2r.mol2
load 4m2r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4m2r
show sticks, 4m2r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
