load 02_3g0e.mol2
load 3g0e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3g0e
show sticks, 3g0e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
