load 02_5kat.mol2
load 5kat_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5kat
show sticks, 5kat_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
