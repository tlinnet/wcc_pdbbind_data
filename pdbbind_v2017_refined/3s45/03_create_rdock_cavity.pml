load 02_3s45.mol2
load 3s45_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3s45
show sticks, 3s45_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
