load 02_4riv.mol2
load 4riv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4riv
show sticks, 4riv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
