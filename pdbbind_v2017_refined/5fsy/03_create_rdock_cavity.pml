load 02_5fsy.mol2
load 5fsy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fsy
show sticks, 5fsy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
