load 02_2oc2.mol2
load 2oc2_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2oc2
show sticks, 2oc2_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
