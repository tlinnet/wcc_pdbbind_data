load 02_1e6q.mol2
load 1e6q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1e6q
show sticks, 1e6q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
