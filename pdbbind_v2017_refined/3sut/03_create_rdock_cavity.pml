load 02_3sut.mol2
load 3sut_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3sut
show sticks, 3sut_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
