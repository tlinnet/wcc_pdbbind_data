load 02_4gii.mol2
load 4gii_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4gii
show sticks, 4gii_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
