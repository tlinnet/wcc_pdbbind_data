load 02_4fys.mol2
load 4fys_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4fys
show sticks, 4fys_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
