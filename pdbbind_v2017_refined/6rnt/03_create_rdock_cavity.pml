load 02_6rnt.mol2
load 6rnt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_6rnt
show sticks, 6rnt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
