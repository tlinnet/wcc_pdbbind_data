load 02_1dgm.mol2
load 1dgm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1dgm
show sticks, 1dgm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
