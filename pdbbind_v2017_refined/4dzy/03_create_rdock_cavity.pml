load 02_4dzy.mol2
load 4dzy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4dzy
show sticks, 4dzy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
