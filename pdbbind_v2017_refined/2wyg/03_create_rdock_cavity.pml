load 02_2wyg.mol2
load 2wyg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wyg
show sticks, 2wyg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
