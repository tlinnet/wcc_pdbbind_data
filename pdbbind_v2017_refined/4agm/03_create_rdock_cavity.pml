load 02_4agm.mol2
load 4agm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4agm
show sticks, 4agm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
