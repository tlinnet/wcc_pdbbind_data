load 02_1mai.mol2
load 1mai_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1mai
show sticks, 1mai_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
