load 02_1hxb.mol2
load 1hxb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hxb
show sticks, 1hxb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
