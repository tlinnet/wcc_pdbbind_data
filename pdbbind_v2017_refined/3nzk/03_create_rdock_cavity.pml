load 02_3nzk.mol2
load 3nzk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nzk
show sticks, 3nzk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
