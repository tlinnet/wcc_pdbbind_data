load 02_1fkw.mol2
load 1fkw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fkw
show sticks, 1fkw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
