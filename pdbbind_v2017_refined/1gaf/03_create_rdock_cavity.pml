load 02_1gaf.mol2
load 1gaf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1gaf
show sticks, 1gaf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
