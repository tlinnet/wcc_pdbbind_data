load 02_2xyf.mol2
load 2xyf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xyf
show sticks, 2xyf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
