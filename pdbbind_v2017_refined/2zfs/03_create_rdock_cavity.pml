load 02_2zfs.mol2
load 2zfs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zfs
show sticks, 2zfs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
