load 02_4ara.mol2
load 4ara_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ara
show sticks, 4ara_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
