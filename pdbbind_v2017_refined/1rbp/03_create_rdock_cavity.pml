load 02_1rbp.mol2
load 1rbp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1rbp
show sticks, 1rbp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
