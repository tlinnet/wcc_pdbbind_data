load 02_2am4.mol2
load 2am4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2am4
show sticks, 2am4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
