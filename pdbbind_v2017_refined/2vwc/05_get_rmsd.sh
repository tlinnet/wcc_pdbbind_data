#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2vwc_ligand.sdf 04_out_2vwc.sd -o 04_out_2vwc_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2vwc_rmsd.sd | tee 05_get_rmsd.score
