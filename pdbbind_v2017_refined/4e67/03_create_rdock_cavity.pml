load 02_4e67.mol2
load 4e67_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4e67
show sticks, 4e67_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
