load 02_1ogz.mol2
load 1ogz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ogz
show sticks, 1ogz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
