load 02_4luz.mol2
load 4luz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4luz
show sticks, 4luz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
