load 02_1c70.mol2
load 1c70_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1c70
show sticks, 1c70_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
