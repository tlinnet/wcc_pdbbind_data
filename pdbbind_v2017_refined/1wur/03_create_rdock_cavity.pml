load 02_1wur.mol2
load 1wur_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1wur
show sticks, 1wur_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
