load 02_2rkg.mol2
load 2rkg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2rkg
show sticks, 2rkg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
