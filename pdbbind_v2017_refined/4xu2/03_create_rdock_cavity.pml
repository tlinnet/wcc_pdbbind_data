load 02_4xu2.mol2
load 4xu2_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4xu2
show sticks, 4xu2_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
