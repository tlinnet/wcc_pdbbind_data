load 02_3f17.mol2
load 3f17_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3f17
show sticks, 3f17_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
