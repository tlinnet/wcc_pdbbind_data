load 02_1apv.mol2
load 1apv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1apv
show sticks, 1apv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
