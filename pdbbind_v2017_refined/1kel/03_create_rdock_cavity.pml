load 02_1kel.mol2
load 1kel_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1kel
show sticks, 1kel_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
