load 02_2rio.mol2
load 2rio_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2rio
show sticks, 2rio_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
