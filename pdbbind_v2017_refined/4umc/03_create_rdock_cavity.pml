load 02_4umc.mol2
load 4umc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4umc
show sticks, 4umc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
