load 02_4m0e.mol2
load 4m0e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4m0e
show sticks, 4m0e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
