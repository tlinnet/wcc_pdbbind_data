load 02_2bal.mol2
load 2bal_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2bal
show sticks, 2bal_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
