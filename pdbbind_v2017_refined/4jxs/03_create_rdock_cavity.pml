load 02_4jxs.mol2
load 4jxs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4jxs
show sticks, 4jxs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
