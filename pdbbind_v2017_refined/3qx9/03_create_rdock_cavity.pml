load 02_3qx9.mol2
load 3qx9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qx9
show sticks, 3qx9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
