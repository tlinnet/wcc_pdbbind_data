load 02_1o2n.mol2
load 1o2n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1o2n
show sticks, 1o2n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
