load 02_1nli.mol2
load 1nli_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1nli
show sticks, 1nli_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
