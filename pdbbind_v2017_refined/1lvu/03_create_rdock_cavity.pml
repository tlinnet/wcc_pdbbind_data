load 02_1lvu.mol2
load 1lvu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lvu
show sticks, 1lvu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
