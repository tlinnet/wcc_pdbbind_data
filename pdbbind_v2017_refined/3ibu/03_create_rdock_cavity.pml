load 02_3ibu.mol2
load 3ibu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ibu
show sticks, 3ibu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
