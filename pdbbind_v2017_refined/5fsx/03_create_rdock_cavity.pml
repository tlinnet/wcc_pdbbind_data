load 02_5fsx.mol2
load 5fsx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fsx
show sticks, 5fsx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
