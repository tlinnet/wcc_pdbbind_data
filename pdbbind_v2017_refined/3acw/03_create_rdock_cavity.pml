load 02_3acw.mol2
load 3acw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3acw
show sticks, 3acw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
