load 02_2h15.mol2
load 2h15_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2h15
show sticks, 2h15_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
