load 02_4cj4.mol2
load 4cj4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cj4
show sticks, 4cj4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
