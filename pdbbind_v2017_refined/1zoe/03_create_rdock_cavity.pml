load 02_1zoe.mol2
load 1zoe_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1zoe
show sticks, 1zoe_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
