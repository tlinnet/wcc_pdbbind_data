load 02_4zip.mol2
load 4zip_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zip
show sticks, 4zip_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
