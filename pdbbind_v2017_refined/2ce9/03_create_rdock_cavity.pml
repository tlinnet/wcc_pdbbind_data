load 02_2ce9.mol2
load 2ce9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ce9
show sticks, 2ce9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
