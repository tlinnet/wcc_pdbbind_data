load 02_3zbx.mol2
load 3zbx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3zbx
show sticks, 3zbx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
