load 02_2xib.mol2
load 2xib_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xib
show sticks, 2xib_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
