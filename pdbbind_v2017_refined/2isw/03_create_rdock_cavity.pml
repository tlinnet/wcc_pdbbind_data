load 02_2isw.mol2
load 2isw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2isw
show sticks, 2isw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
