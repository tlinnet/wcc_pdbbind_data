load 02_5kbe.mol2
load 5kbe_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5kbe
show sticks, 5kbe_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
