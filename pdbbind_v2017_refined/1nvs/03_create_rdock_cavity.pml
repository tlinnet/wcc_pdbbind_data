load 02_1nvs.mol2
load 1nvs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1nvs
show sticks, 1nvs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
