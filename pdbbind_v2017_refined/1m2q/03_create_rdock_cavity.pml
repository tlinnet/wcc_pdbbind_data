load 02_1m2q.mol2
load 1m2q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1m2q
show sticks, 1m2q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
