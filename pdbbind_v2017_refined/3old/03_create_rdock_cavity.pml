load 02_3old.mol2
load 3old_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3old
show sticks, 3old_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
