load 02_1alw.mol2
load 1alw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1alw
show sticks, 1alw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
