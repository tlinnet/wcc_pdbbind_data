load 02_3pe2.mol2
load 3pe2_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3pe2
show sticks, 3pe2_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
