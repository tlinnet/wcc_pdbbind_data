load 02_4w9d.mol2
load 4w9d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4w9d
show sticks, 4w9d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
