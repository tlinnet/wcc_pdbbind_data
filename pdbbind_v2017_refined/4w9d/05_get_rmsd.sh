#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4w9d_ligand.sdf 04_out_4w9d.sd -o 04_out_4w9d_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4w9d_rmsd.sd | tee 05_get_rmsd.score
