load 02_3iww.mol2
load 3iww_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3iww
show sticks, 3iww_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
