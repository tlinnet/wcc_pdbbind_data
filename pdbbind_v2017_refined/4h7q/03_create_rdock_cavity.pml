load 02_4h7q.mol2
load 4h7q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4h7q
show sticks, 4h7q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
