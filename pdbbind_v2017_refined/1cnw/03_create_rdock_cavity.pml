load 02_1cnw.mol2
load 1cnw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1cnw
show sticks, 1cnw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
