#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2qrl_ligand.sdf 04_out_2qrl.sd -o 04_out_2qrl_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2qrl_rmsd.sd | tee 05_get_rmsd.score
