load 02_4djv.mol2
load 4djv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4djv
show sticks, 4djv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
