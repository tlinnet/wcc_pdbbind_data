load 02_2cbv.mol2
load 2cbv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2cbv
show sticks, 2cbv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
