load 02_3gs6.mol2
load 3gs6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gs6
show sticks, 3gs6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
