load 02_4ca7.mol2
load 4ca7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ca7
show sticks, 4ca7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
