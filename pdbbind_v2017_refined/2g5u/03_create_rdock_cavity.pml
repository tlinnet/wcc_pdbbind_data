load 02_2g5u.mol2
load 2g5u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2g5u
show sticks, 2g5u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
