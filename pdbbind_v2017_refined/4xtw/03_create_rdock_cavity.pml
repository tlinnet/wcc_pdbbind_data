load 02_4xtw.mol2
load 4xtw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4xtw
show sticks, 4xtw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
