load 02_2bet.mol2
load 2bet_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2bet
show sticks, 2bet_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
