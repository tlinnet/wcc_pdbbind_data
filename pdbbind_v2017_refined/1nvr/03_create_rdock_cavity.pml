load 02_1nvr.mol2
load 1nvr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1nvr
show sticks, 1nvr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
