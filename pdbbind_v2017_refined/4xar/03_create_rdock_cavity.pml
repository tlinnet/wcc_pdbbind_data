load 02_4xar.mol2
load 4xar_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4xar
show sticks, 4xar_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
