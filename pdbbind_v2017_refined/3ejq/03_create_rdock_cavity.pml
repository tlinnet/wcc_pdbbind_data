load 02_3ejq.mol2
load 3ejq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ejq
show sticks, 3ejq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
