load 02_2xys.mol2
load 2xys_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xys
show sticks, 2xys_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
