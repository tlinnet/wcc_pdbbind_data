load 02_3hww.mol2
load 3hww_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hww
show sticks, 3hww_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
