load 02_1ex8.mol2
load 1ex8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ex8
show sticks, 1ex8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
