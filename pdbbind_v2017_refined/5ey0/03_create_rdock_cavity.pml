load 02_5ey0.mol2
load 5ey0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ey0
show sticks, 5ey0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
