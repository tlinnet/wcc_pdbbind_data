load 02_1fkb.mol2
load 1fkb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fkb
show sticks, 1fkb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
