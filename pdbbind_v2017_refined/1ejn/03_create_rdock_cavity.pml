load 02_1ejn.mol2
load 1ejn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ejn
show sticks, 1ejn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
