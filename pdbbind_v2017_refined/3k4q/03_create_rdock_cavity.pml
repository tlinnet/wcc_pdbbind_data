load 02_3k4q.mol2
load 3k4q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3k4q
show sticks, 3k4q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
