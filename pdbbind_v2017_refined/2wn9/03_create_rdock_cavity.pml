load 02_2wn9.mol2
load 2wn9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wn9
show sticks, 2wn9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
