load 02_1jet.mol2
load 1jet_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1jet
show sticks, 1jet_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
