load 02_2on6.mol2
load 2on6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2on6
show sticks, 2on6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
