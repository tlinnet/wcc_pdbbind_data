load 02_1xff.mol2
load 1xff_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1xff
show sticks, 1xff_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
