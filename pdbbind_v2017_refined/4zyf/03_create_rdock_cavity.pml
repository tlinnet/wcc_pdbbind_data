load 02_4zyf.mol2
load 4zyf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zyf
show sticks, 4zyf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
