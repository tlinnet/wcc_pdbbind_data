load 02_2izl.mol2
load 2izl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2izl
show sticks, 2izl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
