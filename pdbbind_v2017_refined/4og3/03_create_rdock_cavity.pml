load 02_4og3.mol2
load 4og3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4og3
show sticks, 4og3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
