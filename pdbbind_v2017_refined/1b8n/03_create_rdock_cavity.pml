load 02_1b8n.mol2
load 1b8n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1b8n
show sticks, 1b8n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
