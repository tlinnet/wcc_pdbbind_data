load 02_1q91.mol2
load 1q91_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1q91
show sticks, 1q91_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
