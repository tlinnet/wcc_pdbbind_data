load 02_2vhj.mol2
load 2vhj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vhj
show sticks, 2vhj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
