load 02_4exs.mol2
load 4exs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4exs
show sticks, 4exs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
