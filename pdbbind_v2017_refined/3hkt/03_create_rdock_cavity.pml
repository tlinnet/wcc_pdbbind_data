load 02_3hkt.mol2
load 3hkt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hkt
show sticks, 3hkt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
