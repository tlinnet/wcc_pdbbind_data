load 02_1fjs.mol2
load 1fjs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fjs
show sticks, 1fjs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
