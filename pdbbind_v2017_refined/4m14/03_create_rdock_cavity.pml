load 02_4m14.mol2
load 4m14_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4m14
show sticks, 4m14_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
