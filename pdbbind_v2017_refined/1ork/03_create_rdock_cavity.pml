load 02_1ork.mol2
load 1ork_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ork
show sticks, 1ork_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
