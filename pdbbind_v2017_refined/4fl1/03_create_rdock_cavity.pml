load 02_4fl1.mol2
load 4fl1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4fl1
show sticks, 4fl1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
