load 02_1d7i.mol2
load 1d7i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1d7i
show sticks, 1d7i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
