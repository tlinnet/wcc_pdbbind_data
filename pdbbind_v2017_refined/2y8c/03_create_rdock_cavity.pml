load 02_2y8c.mol2
load 2y8c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2y8c
show sticks, 2y8c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
