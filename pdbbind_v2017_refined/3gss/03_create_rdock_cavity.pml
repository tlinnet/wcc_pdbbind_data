load 02_3gss.mol2
load 3gss_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gss
show sticks, 3gss_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
