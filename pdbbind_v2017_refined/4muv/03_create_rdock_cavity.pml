load 02_4muv.mol2
load 4muv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4muv
show sticks, 4muv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
