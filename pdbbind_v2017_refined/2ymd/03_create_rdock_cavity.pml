load 02_2ymd.mol2
load 2ymd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ymd
show sticks, 2ymd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
