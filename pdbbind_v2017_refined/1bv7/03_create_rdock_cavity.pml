load 02_1bv7.mol2
load 1bv7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bv7
show sticks, 1bv7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
