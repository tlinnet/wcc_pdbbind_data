load 02_4alx.mol2
load 4alx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4alx
show sticks, 4alx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
