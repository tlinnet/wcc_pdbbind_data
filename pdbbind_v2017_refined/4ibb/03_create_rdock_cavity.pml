load 02_4ibb.mol2
load 4ibb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ibb
show sticks, 4ibb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
