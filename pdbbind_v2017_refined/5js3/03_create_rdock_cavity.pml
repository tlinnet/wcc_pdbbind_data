load 02_5js3.mol2
load 5js3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5js3
show sticks, 5js3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
