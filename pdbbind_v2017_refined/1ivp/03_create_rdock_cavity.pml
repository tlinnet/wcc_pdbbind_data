load 02_1ivp.mol2
load 1ivp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ivp
show sticks, 1ivp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
