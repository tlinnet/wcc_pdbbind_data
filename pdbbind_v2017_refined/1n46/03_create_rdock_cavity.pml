load 02_1n46.mol2
load 1n46_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1n46
show sticks, 1n46_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
