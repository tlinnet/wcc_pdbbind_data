load 02_4det.mol2
load 4det_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4det
show sticks, 4det_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
