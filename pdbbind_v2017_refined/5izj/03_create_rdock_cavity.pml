load 02_5izj.mol2
load 5izj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5izj
show sticks, 5izj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
