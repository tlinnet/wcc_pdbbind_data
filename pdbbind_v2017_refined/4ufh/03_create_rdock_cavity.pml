load 02_4ufh.mol2
load 4ufh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ufh
show sticks, 4ufh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
