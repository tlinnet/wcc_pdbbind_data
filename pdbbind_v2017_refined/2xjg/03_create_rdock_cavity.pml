load 02_2xjg.mol2
load 2xjg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xjg
show sticks, 2xjg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
