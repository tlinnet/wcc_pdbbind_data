load 02_2xbp.mol2
load 2xbp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xbp
show sticks, 2xbp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
