load 02_5g2g.mol2
load 5g2g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5g2g
show sticks, 5g2g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
