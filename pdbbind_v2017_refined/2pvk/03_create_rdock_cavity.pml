load 02_2pvk.mol2
load 2pvk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pvk
show sticks, 2pvk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
