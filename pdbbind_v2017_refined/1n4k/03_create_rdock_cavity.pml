load 02_1n4k.mol2
load 1n4k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1n4k
show sticks, 1n4k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
