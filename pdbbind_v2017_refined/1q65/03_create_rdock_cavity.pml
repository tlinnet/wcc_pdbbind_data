load 02_1q65.mol2
load 1q65_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1q65
show sticks, 1q65_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
