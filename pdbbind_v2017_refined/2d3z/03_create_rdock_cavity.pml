load 02_2d3z.mol2
load 2d3z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2d3z
show sticks, 2d3z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
