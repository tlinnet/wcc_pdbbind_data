load 02_4kni.mol2
load 4kni_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4kni
show sticks, 4kni_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
