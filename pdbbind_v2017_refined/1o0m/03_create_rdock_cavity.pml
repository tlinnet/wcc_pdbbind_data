load 02_1o0m.mol2
load 1o0m_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1o0m
show sticks, 1o0m_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
