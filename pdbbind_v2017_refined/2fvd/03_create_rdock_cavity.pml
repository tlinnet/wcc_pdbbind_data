load 02_2fvd.mol2
load 2fvd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2fvd
show sticks, 2fvd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
