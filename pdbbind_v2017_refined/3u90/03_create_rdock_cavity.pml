load 02_3u90.mol2
load 3u90_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3u90
show sticks, 3u90_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
