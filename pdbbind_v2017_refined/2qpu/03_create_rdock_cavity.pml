load 02_2qpu.mol2
load 2qpu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qpu
show sticks, 2qpu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
