load 02_1onz.mol2
load 1onz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1onz
show sticks, 1onz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
