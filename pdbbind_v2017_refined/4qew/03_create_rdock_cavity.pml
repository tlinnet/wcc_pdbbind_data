load 02_4qew.mol2
load 4qew_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qew
show sticks, 4qew_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
