load 02_4oiv.mol2
load 4oiv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4oiv
show sticks, 4oiv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
