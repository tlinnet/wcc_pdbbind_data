load 02_3s6t.mol2
load 3s6t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3s6t
show sticks, 3s6t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
