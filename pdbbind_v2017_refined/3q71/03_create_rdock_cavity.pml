load 02_3q71.mol2
load 3q71_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3q71
show sticks, 3q71_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
