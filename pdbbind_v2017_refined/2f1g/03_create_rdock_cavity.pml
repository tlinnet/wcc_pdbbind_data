load 02_2f1g.mol2
load 2f1g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2f1g
show sticks, 2f1g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
