load 02_4u0w.mol2
load 4u0w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4u0w
show sticks, 4u0w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
