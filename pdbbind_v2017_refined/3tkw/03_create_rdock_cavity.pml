load 02_3tkw.mol2
load 3tkw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3tkw
show sticks, 3tkw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
