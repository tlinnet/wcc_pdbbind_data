load 02_2zkj.mol2
load 2zkj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zkj
show sticks, 2zkj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
