load 02_2cer.mol2
load 2cer_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2cer
show sticks, 2cer_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
