load 02_5er4.mol2
load 5er4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5er4
show sticks, 5er4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
