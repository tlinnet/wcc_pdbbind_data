#!/usr/bin/env bash

# Get best rmsd
sdrmsd 5er4_ligand.sdf 04_out_5er4.sd -o 04_out_5er4_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_5er4_rmsd.sd | tee 05_get_rmsd.score
