load 02_3rt8.mol2
load 3rt8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3rt8
show sticks, 3rt8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
