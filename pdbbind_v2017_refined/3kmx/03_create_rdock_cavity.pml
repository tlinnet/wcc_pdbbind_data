load 02_3kmx.mol2
load 3kmx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kmx
show sticks, 3kmx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
