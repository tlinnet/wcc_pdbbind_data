load 02_2b1i.mol2
load 2b1i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2b1i
show sticks, 2b1i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
