load 02_4zwz.mol2
load 4zwz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zwz
show sticks, 4zwz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
