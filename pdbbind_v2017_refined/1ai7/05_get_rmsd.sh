#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1ai7_ligand.sdf 04_out_1ai7.sd -o 04_out_1ai7_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1ai7_rmsd.sd | tee 05_get_rmsd.score
