load 02_1ax0.mol2
load 1ax0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ax0
show sticks, 1ax0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
