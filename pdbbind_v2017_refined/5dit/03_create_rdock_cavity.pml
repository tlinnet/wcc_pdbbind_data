load 02_5dit.mol2
load 5dit_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5dit
show sticks, 5dit_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
