load 02_1rtf.mol2
load 1rtf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1rtf
show sticks, 1rtf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
