load 02_2yfx.mol2
load 2yfx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2yfx
show sticks, 2yfx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
