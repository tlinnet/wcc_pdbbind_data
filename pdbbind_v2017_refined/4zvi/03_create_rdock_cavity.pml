load 02_4zvi.mol2
load 4zvi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zvi
show sticks, 4zvi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
