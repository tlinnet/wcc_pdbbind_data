load 02_2j2u.mol2
load 2j2u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2j2u
show sticks, 2j2u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
