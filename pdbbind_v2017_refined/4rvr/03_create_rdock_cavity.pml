load 02_4rvr.mol2
load 4rvr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rvr
show sticks, 4rvr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
