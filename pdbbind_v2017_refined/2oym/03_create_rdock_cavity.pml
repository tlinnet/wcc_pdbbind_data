load 02_2oym.mol2
load 2oym_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2oym
show sticks, 2oym_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
