load 02_4djx.mol2
load 4djx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4djx
show sticks, 4djx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
