load 02_1fkn.mol2
load 1fkn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fkn
show sticks, 1fkn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
