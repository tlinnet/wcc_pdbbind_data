load 02_4loh.mol2
load 4loh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4loh
show sticks, 4loh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
