load 02_2qtt.mol2
load 2qtt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qtt
show sticks, 2qtt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
