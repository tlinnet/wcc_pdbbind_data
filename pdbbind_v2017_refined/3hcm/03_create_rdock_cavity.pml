load 02_3hcm.mol2
load 3hcm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hcm
show sticks, 3hcm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
