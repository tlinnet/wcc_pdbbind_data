load 02_5a6k.mol2
load 5a6k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5a6k
show sticks, 5a6k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
