load 02_4mre.mol2
load 4mre_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4mre
show sticks, 4mre_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
