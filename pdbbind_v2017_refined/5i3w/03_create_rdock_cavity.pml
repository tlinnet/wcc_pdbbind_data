load 02_5i3w.mol2
load 5i3w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5i3w
show sticks, 5i3w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
