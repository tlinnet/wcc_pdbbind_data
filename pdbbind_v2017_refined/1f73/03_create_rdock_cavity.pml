load 02_1f73.mol2
load 1f73_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1f73
show sticks, 1f73_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
