load 02_3wz8.mol2
load 3wz8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3wz8
show sticks, 3wz8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
