load 02_2zcr.mol2
load 2zcr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zcr
show sticks, 2zcr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
