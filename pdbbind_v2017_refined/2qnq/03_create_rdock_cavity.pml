load 02_2qnq.mol2
load 2qnq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qnq
show sticks, 2qnq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
