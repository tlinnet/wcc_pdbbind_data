load 02_4kwf.mol2
load 4kwf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4kwf
show sticks, 4kwf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
