load 02_1ghv.mol2
load 1ghv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ghv
show sticks, 1ghv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
