load 02_3vha.mol2
load 3vha_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3vha
show sticks, 3vha_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
