load 02_2qtg.mol2
load 2qtg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qtg
show sticks, 2qtg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
