load 02_4leq.mol2
load 4leq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4leq
show sticks, 4leq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
