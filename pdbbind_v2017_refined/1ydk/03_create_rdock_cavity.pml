load 02_1ydk.mol2
load 1ydk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ydk
show sticks, 1ydk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
