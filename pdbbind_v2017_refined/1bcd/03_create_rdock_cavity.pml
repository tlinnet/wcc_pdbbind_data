load 02_1bcd.mol2
load 1bcd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bcd
show sticks, 1bcd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
