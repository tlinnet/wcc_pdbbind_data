load 02_4oma.mol2
load 4oma_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4oma
show sticks, 4oma_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
