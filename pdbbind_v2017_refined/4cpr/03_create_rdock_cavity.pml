load 02_4cpr.mol2
load 4cpr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cpr
show sticks, 4cpr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
