load 02_1bnw.mol2
load 1bnw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bnw
show sticks, 1bnw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
