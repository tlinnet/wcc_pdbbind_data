load 02_4rfr.mol2
load 4rfr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rfr
show sticks, 4rfr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
