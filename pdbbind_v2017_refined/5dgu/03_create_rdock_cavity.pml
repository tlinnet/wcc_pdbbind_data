load 02_5dgu.mol2
load 5dgu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5dgu
show sticks, 5dgu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
