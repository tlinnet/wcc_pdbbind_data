load 02_4std.mol2
load 4std_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4std
show sticks, 4std_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
