load 02_4a95.mol2
load 4a95_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4a95
show sticks, 4a95_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
