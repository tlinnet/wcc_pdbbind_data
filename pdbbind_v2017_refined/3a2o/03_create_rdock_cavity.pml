load 02_3a2o.mol2
load 3a2o_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3a2o
show sticks, 3a2o_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
