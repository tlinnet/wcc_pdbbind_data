load 02_2cbz.mol2
load 2cbz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2cbz
show sticks, 2cbz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
