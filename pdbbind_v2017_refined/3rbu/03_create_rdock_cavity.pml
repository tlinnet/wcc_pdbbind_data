load 02_3rbu.mol2
load 3rbu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3rbu
show sticks, 3rbu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
