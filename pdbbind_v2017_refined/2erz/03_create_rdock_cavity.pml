load 02_2erz.mol2
load 2erz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2erz
show sticks, 2erz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
