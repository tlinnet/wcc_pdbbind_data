load 02_4c9x.mol2
load 4c9x_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4c9x
show sticks, 4c9x_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
