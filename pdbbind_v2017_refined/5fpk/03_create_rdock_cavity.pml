load 02_5fpk.mol2
load 5fpk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fpk
show sticks, 5fpk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
