load 02_5lz7.mol2
load 5lz7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5lz7
show sticks, 5lz7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
