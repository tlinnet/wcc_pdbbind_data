load 02_3sue.mol2
load 3sue_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3sue
show sticks, 3sue_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
