load 02_3slz.mol2
load 3slz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3slz
show sticks, 3slz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
