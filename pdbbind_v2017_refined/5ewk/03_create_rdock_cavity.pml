load 02_5ewk.mol2
load 5ewk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ewk
show sticks, 5ewk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
