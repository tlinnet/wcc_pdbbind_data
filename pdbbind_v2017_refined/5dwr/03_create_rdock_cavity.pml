load 02_5dwr.mol2
load 5dwr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5dwr
show sticks, 5dwr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
