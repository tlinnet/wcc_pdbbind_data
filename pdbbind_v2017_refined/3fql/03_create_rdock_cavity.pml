load 02_3fql.mol2
load 3fql_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3fql
show sticks, 3fql_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
