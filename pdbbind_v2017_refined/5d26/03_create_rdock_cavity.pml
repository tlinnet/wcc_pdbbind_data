load 02_5d26.mol2
load 5d26_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5d26
show sticks, 5d26_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
