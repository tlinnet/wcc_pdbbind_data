load 02_1b51.mol2
load 1b51_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1b51
show sticks, 1b51_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
