load 02_2oxn.mol2
load 2oxn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2oxn
show sticks, 2oxn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
