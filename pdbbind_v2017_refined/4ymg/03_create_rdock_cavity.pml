load 02_4ymg.mol2
load 4ymg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ymg
show sticks, 4ymg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
