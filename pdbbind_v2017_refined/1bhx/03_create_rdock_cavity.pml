load 02_1bhx.mol2
load 1bhx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bhx
show sticks, 1bhx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
