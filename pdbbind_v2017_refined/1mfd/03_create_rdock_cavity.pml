load 02_1mfd.mol2
load 1mfd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1mfd
show sticks, 1mfd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
