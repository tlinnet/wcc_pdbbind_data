load 02_3nhi.mol2
load 3nhi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nhi
show sticks, 3nhi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
