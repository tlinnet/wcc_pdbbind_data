load 02_1a4w.mol2
load 1a4w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1a4w
show sticks, 1a4w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
