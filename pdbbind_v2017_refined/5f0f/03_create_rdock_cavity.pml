load 02_5f0f.mol2
load 5f0f_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5f0f
show sticks, 5f0f_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
