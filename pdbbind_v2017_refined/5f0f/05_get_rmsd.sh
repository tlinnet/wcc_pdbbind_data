#!/usr/bin/env bash

# Get best rmsd
sdrmsd 5f0f_ligand.sdf 04_out_5f0f.sd -o 04_out_5f0f_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_5f0f_rmsd.sd | tee 05_get_rmsd.score
