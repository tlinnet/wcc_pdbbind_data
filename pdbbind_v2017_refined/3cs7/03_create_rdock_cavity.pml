load 02_3cs7.mol2
load 3cs7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3cs7
show sticks, 3cs7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
