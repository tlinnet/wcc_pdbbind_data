load 02_3x00.mol2
load 3x00_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3x00
show sticks, 3x00_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
