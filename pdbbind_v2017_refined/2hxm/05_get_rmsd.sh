#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2hxm_ligand.sdf 04_out_2hxm.sd -o 04_out_2hxm_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2hxm_rmsd.sd | tee 05_get_rmsd.score
