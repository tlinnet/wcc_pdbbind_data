load 02_2hxm.mol2
load 2hxm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2hxm
show sticks, 2hxm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
