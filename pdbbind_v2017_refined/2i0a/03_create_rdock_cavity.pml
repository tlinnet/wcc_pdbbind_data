load 02_2i0a.mol2
load 2i0a_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i0a
show sticks, 2i0a_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
