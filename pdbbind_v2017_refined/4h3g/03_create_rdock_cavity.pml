load 02_4h3g.mol2
load 4h3g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4h3g
show sticks, 4h3g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
