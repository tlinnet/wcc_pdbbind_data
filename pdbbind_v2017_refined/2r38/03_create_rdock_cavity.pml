load 02_2r38.mol2
load 2r38_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2r38
show sticks, 2r38_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
