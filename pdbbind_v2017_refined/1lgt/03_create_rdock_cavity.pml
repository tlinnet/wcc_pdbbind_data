load 02_1lgt.mol2
load 1lgt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lgt
show sticks, 1lgt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
