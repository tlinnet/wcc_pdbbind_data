load 02_3r6u.mol2
load 3r6u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3r6u
show sticks, 3r6u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
