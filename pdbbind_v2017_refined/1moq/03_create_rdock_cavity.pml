load 02_1moq.mol2
load 1moq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1moq
show sticks, 1moq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
