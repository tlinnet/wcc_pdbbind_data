load 02_3gkz.mol2
load 3gkz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gkz
show sticks, 3gkz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
