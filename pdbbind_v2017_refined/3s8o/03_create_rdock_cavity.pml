load 02_3s8o.mol2
load 3s8o_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3s8o
show sticks, 3s8o_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
