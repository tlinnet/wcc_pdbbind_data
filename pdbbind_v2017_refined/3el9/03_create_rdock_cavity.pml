load 02_3el9.mol2
load 3el9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3el9
show sticks, 3el9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
