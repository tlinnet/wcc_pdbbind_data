load 02_1qkt.mol2
load 1qkt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qkt
show sticks, 1qkt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
