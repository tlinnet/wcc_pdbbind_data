load 02_5egm.mol2
load 5egm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5egm
show sticks, 5egm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
