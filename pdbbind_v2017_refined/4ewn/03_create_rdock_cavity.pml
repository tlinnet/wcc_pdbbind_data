load 02_4ewn.mol2
load 4ewn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ewn
show sticks, 4ewn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
