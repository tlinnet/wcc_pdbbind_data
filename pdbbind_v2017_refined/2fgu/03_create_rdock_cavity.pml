load 02_2fgu.mol2
load 2fgu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2fgu
show sticks, 2fgu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
