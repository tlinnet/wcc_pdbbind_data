load 02_4f3k.mol2
load 4f3k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4f3k
show sticks, 4f3k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
