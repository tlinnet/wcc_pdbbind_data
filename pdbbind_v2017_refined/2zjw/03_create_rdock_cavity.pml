load 02_2zjw.mol2
load 2zjw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zjw
show sticks, 2zjw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
