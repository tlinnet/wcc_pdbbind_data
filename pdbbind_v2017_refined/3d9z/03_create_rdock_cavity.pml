load 02_3d9z.mol2
load 3d9z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3d9z
show sticks, 3d9z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
