load 02_5f74.mol2
load 5f74_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5f74
show sticks, 5f74_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
