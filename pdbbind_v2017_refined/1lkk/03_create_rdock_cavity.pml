load 02_1lkk.mol2
load 1lkk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lkk
show sticks, 1lkk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
