load 02_2y82.mol2
load 2y82_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2y82
show sticks, 2y82_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
