#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2y82_ligand.sdf 04_out_2y82.sd -o 04_out_2y82_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2y82_rmsd.sd | tee 05_get_rmsd.score
