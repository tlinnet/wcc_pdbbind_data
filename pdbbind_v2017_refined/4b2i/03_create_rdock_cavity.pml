load 02_4b2i.mol2
load 4b2i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4b2i
show sticks, 4b2i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
