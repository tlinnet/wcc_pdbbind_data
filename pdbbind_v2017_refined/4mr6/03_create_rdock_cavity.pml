load 02_4mr6.mol2
load 4mr6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4mr6
show sticks, 4mr6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
