load 02_5dqf.mol2
load 5dqf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5dqf
show sticks, 5dqf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
