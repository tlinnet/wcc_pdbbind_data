#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3su0_ligand.sdf 04_out_3su0.sd -o 04_out_3su0_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3su0_rmsd.sd | tee 05_get_rmsd.score
