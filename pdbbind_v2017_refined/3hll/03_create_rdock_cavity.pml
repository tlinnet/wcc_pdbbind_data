load 02_3hll.mol2
load 3hll_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hll
show sticks, 3hll_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
