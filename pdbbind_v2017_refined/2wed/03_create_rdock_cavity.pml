load 02_2wed.mol2
load 2wed_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wed
show sticks, 2wed_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
