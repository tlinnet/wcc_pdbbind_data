load 02_4r4i.mol2
load 4r4i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4r4i
show sticks, 4r4i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
