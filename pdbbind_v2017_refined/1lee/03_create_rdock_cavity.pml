load 02_1lee.mol2
load 1lee_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lee
show sticks, 1lee_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
