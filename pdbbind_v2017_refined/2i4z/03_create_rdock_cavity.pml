load 02_2i4z.mol2
load 2i4z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i4z
show sticks, 2i4z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
