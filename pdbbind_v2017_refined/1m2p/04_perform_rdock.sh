#!/usr/bin/env bash

rbdock -i 1m2p_ligand.sdf -o 04_out_1m2p -r 03_create_rdock_cavity.prm -p dock.prm -n 1 | tee 04_perform_rdock.log

echo $SECONDS > 04_perform_rdock.sec
times > 04_perform_rdock.time

