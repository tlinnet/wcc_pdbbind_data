load 02_3ttm.mol2
load 3ttm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ttm
show sticks, 3ttm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
