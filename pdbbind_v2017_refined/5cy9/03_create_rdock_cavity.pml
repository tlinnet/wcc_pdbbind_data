load 02_5cy9.mol2
load 5cy9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5cy9
show sticks, 5cy9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
