load 02_2x7t.mol2
load 2x7t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2x7t
show sticks, 2x7t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
