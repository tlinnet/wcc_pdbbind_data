load 02_1ajv.mol2
load 1ajv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ajv
show sticks, 1ajv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
