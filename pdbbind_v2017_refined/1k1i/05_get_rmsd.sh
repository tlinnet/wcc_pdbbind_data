#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1k1i_ligand.sdf 04_out_1k1i.sd -o 04_out_1k1i_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1k1i_rmsd.sd | tee 05_get_rmsd.score
