load 02_1k1i.mol2
load 1k1i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1k1i
show sticks, 1k1i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
