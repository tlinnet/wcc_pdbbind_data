load 02_4k3h.mol2
load 4k3h_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4k3h
show sticks, 4k3h_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
