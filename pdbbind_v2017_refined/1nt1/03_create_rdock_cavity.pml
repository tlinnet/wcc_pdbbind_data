load 02_1nt1.mol2
load 1nt1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1nt1
show sticks, 1nt1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
