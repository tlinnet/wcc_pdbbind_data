load 02_2zwz.mol2
load 2zwz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zwz
show sticks, 2zwz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
