load 02_4ibi.mol2
load 4ibi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ibi
show sticks, 4ibi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
