load 02_3h8b.mol2
load 3h8b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3h8b
show sticks, 3h8b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
