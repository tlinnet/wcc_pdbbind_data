load 02_1koj.mol2
load 1koj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1koj
show sticks, 1koj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
