load 02_1pme.mol2
load 1pme_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pme
show sticks, 1pme_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
