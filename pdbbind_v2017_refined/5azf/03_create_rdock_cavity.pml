load 02_5azf.mol2
load 5azf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5azf
show sticks, 5azf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
