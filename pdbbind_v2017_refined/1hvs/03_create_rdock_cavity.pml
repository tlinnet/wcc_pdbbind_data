load 02_1hvs.mol2
load 1hvs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hvs
show sticks, 1hvs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
