#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3pgl_ligand.sdf 04_out_3pgl.sd -o 04_out_3pgl_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3pgl_rmsd.sd | tee 05_get_rmsd.score
