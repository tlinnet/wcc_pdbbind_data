load 02_3pgl.mol2
load 3pgl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3pgl
show sticks, 3pgl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
