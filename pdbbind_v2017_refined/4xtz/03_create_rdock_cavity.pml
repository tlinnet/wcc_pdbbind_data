load 02_4xtz.mol2
load 4xtz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4xtz
show sticks, 4xtz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
