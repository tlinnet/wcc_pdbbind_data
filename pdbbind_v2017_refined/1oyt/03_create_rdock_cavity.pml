load 02_1oyt.mol2
load 1oyt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1oyt
show sticks, 1oyt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
