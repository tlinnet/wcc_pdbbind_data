load 02_1kug.mol2
load 1kug_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1kug
show sticks, 1kug_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
