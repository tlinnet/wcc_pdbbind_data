load 02_3t2w.mol2
load 3t2w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3t2w
show sticks, 3t2w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
