#!/usr/bin/env bash

rbdock -i 5a2i_ligand.sdf -o 04_out_5a2i -r 03_create_rdock_cavity.prm -p dock.prm -n 1 | tee 04_perform_rdock.log

echo $SECONDS > 04_perform_rdock.sec
times > 04_perform_rdock.time

