load 02_5a2i.mol2
load 5a2i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5a2i
show sticks, 5a2i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
