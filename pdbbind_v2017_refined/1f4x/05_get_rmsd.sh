#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1f4x_ligand.sdf 04_out_1f4x.sd -o 04_out_1f4x_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1f4x_rmsd.sd | tee 05_get_rmsd.score
