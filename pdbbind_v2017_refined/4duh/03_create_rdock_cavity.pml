load 02_4duh.mol2
load 4duh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4duh
show sticks, 4duh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
