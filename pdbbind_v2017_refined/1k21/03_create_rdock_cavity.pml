load 02_1k21.mol2
load 1k21_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1k21
show sticks, 1k21_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
