load 02_4cwr.mol2
load 4cwr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cwr
show sticks, 4cwr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
