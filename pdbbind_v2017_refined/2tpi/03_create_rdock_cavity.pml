load 02_2tpi.mol2
load 2tpi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2tpi
show sticks, 2tpi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
