load 02_1syi.mol2
load 1syi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1syi
show sticks, 1syi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
