load 02_4cpy.mol2
load 4cpy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cpy
show sticks, 4cpy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
