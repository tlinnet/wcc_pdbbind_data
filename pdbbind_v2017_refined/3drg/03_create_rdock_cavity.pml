load 02_3drg.mol2
load 3drg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3drg
show sticks, 3drg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
