load 02_3uug.mol2
load 3uug_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3uug
show sticks, 3uug_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
