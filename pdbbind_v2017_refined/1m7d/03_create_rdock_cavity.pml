load 02_1m7d.mol2
load 1m7d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1m7d
show sticks, 1m7d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
