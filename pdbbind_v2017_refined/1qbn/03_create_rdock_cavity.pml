load 02_1qbn.mol2
load 1qbn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qbn
show sticks, 1qbn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
