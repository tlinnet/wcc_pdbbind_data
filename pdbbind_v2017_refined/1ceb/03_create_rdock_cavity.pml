load 02_1ceb.mol2
load 1ceb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ceb
show sticks, 1ceb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
