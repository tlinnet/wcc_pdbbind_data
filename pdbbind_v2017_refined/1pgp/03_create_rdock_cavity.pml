load 02_1pgp.mol2
load 1pgp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pgp
show sticks, 1pgp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
