load 02_3ivg.mol2
load 3ivg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ivg
show sticks, 3ivg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
