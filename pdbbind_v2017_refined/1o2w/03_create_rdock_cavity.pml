load 02_1o2w.mol2
load 1o2w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1o2w
show sticks, 1o2w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
