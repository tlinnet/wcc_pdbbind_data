load 02_3u5j.mol2
load 3u5j_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3u5j
show sticks, 3u5j_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
