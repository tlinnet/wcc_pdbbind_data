load 02_1bnt.mol2
load 1bnt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bnt
show sticks, 1bnt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
