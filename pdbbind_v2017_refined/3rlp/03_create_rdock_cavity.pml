load 02_3rlp.mol2
load 3rlp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3rlp
show sticks, 3rlp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
