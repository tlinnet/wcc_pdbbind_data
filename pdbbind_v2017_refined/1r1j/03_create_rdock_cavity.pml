load 02_1r1j.mol2
load 1r1j_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1r1j
show sticks, 1r1j_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
