load 02_2vb8.mol2
load 2vb8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vb8
show sticks, 2vb8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
