load 02_1my4.mol2
load 1my4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1my4
show sticks, 1my4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
