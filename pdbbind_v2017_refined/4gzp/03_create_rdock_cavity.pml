load 02_4gzp.mol2
load 4gzp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4gzp
show sticks, 4gzp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
