load 02_2q5k.mol2
load 2q5k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2q5k
show sticks, 2q5k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
