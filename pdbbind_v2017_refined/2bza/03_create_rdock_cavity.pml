load 02_2bza.mol2
load 2bza_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2bza
show sticks, 2bza_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
