load 02_3nuj.mol2
load 3nuj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nuj
show sticks, 3nuj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
