load 02_4b6p.mol2
load 4b6p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4b6p
show sticks, 4b6p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
