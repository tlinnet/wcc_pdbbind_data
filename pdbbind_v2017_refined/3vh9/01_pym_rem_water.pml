load 3vh9_protein.pdb

#Remove alternates
cmd.remove("not alt +A")
cmd.valence("guess", selection1='3vh9_protein')

#Prepare molecules with polar hydrogens:
cmd.h_add("elem O or elem N or elem S")

#Delete Waters and Heteroatoms
cmd.remove("solvent")
cmd.remove("resn HOH")
cmd.remove("hetatm")

save 01_3vh9.pdb
