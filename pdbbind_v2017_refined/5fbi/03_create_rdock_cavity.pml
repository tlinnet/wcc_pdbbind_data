load 02_5fbi.mol2
load 5fbi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fbi
show sticks, 5fbi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
