#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4x8o_ligand.sdf 04_out_4x8o.sd -o 04_out_4x8o_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4x8o_rmsd.sd | tee 05_get_rmsd.score
