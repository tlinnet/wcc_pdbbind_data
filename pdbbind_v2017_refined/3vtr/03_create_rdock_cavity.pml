load 02_3vtr.mol2
load 3vtr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3vtr
show sticks, 3vtr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
