load 02_2vvp.mol2
load 2vvp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vvp
show sticks, 2vvp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
