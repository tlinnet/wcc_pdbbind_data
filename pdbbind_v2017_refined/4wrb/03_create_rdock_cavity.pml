load 02_4wrb.mol2
load 4wrb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4wrb
show sticks, 4wrb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
