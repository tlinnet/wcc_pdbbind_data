load 02_2zc9.mol2
load 2zc9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zc9
show sticks, 2zc9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
