#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4ih5_ligand.sdf 04_out_4ih5.sd -o 04_out_4ih5_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4ih5_rmsd.sd | tee 05_get_rmsd.score
