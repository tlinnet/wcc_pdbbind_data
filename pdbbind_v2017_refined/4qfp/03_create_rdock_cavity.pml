load 02_4qfp.mol2
load 4qfp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qfp
show sticks, 4qfp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
