load 02_1drk.mol2
load 1drk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1drk
show sticks, 1drk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
