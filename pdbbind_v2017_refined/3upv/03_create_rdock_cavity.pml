load 02_3upv.mol2
load 3upv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3upv
show sticks, 3upv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
