load 02_5f2p.mol2
load 5f2p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5f2p
show sticks, 5f2p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
