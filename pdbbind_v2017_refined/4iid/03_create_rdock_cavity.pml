load 02_4iid.mol2
load 4iid_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4iid
show sticks, 4iid_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
