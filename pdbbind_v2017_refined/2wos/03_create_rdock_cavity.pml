load 02_2wos.mol2
load 2wos_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wos
show sticks, 2wos_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
