load 02_1hlk.mol2
load 1hlk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hlk
show sticks, 1hlk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
