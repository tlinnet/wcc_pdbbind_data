load 02_4a4q.mol2
load 4a4q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4a4q
show sticks, 4a4q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
