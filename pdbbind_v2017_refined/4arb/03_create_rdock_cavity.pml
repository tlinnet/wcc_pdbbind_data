load 02_4arb.mol2
load 4arb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4arb
show sticks, 4arb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
