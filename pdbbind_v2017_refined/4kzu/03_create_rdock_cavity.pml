load 02_4kzu.mol2
load 4kzu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4kzu
show sticks, 4kzu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
