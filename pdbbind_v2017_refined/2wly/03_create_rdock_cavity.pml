load 02_2wly.mol2
load 2wly_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wly
show sticks, 2wly_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
