load 02_3eft.mol2
load 3eft_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3eft
show sticks, 3eft_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
