load 02_3h78.mol2
load 3h78_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3h78
show sticks, 3h78_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
