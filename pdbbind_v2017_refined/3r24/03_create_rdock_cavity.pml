load 02_3r24.mol2
load 3r24_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3r24
show sticks, 3r24_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
