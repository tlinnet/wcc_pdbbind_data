load 02_2c3l.mol2
load 2c3l_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2c3l
show sticks, 2c3l_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
