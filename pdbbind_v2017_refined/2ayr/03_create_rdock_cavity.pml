load 02_2ayr.mol2
load 2ayr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ayr
show sticks, 2ayr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
