load 02_4pp0.mol2
load 4pp0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pp0
show sticks, 4pp0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
