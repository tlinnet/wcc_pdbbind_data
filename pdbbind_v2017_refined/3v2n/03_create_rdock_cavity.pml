load 02_3v2n.mol2
load 3v2n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3v2n
show sticks, 3v2n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
