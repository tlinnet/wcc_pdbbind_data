load 02_3mjl.mol2
load 3mjl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3mjl
show sticks, 3mjl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
