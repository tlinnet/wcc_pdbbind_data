#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4g8y_ligand.sdf 04_out_4g8y.sd -o 04_out_4g8y_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4g8y_rmsd.sd | tee 05_get_rmsd.score
