#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4kmz_ligand.sdf 04_out_4kmz.sd -o 04_out_4kmz_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4kmz_rmsd.sd | tee 05_get_rmsd.score
