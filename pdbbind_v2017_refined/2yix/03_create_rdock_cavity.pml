load 02_2yix.mol2
load 2yix_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2yix
show sticks, 2yix_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
