load 02_3w9r.mol2
load 3w9r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3w9r
show sticks, 3w9r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
