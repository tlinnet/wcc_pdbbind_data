load 02_2ca8.mol2
load 2ca8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ca8
show sticks, 2ca8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
