load 02_4ruz.mol2
load 4ruz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ruz
show sticks, 4ruz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
