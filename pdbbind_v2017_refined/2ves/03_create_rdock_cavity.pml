load 02_2ves.mol2
load 2ves_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ves
show sticks, 2ves_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
