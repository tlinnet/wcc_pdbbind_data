load 02_2aac.mol2
load 2aac_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2aac
show sticks, 2aac_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
