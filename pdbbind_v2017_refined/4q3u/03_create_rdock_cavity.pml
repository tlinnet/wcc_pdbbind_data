load 02_4q3u.mol2
load 4q3u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4q3u
show sticks, 4q3u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
