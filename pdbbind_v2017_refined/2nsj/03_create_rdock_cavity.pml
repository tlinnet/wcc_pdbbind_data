load 02_2nsj.mol2
load 2nsj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2nsj
show sticks, 2nsj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
