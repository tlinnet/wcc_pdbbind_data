load 02_5fou.mol2
load 5fou_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fou
show sticks, 5fou_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
