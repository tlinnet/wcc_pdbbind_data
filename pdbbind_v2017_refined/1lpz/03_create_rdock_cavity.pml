load 02_1lpz.mol2
load 1lpz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lpz
show sticks, 1lpz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
