load 02_4omj.mol2
load 4omj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4omj
show sticks, 4omj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
