load 02_3qdd.mol2
load 3qdd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qdd
show sticks, 3qdd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
