load 02_1gx8.mol2
load 1gx8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1gx8
show sticks, 1gx8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
