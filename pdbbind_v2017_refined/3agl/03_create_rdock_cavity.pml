load 02_3agl.mol2
load 3agl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3agl
show sticks, 3agl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
