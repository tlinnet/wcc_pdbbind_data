load 02_1jmf.mol2
load 1jmf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1jmf
show sticks, 1jmf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
