load 02_4agc.mol2
load 4agc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4agc
show sticks, 4agc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
