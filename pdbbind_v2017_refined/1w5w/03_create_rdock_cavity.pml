load 02_1w5w.mol2
load 1w5w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1w5w
show sticks, 1w5w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
