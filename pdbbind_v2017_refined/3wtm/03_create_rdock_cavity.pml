load 02_3wtm.mol2
load 3wtm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3wtm
show sticks, 3wtm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
