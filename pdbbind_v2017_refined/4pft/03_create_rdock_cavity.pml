load 02_4pft.mol2
load 4pft_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pft
show sticks, 4pft_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
