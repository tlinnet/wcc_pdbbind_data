load 02_3ebh.mol2
load 3ebh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ebh
show sticks, 3ebh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
