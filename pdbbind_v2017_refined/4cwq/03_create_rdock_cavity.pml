load 02_4cwq.mol2
load 4cwq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cwq
show sticks, 4cwq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
