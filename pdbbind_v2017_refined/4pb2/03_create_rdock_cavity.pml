load 02_4pb2.mol2
load 4pb2_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pb2
show sticks, 4pb2_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
