load 02_2iwx.mol2
load 2iwx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2iwx
show sticks, 2iwx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
