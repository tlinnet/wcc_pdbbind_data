load 02_4i7j.mol2
load 4i7j_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4i7j
show sticks, 4i7j_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
