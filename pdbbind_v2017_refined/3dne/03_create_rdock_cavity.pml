load 02_3dne.mol2
load 3dne_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3dne
show sticks, 3dne_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
