load 02_2bmk.mol2
load 2bmk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2bmk
show sticks, 2bmk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
