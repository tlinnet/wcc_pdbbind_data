load 02_4aia.mol2
load 4aia_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4aia
show sticks, 4aia_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
