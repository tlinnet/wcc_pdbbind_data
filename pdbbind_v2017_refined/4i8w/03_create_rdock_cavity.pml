load 02_4i8w.mol2
load 4i8w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4i8w
show sticks, 4i8w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
