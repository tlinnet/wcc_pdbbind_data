load 02_4tmk.mol2
load 4tmk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4tmk
show sticks, 4tmk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
