load 02_1bzy.mol2
load 1bzy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bzy
show sticks, 1bzy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
