load 02_1pxp.mol2
load 1pxp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pxp
show sticks, 1pxp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
