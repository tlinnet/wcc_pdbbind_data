load 02_5k03.mol2
load 5k03_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5k03
show sticks, 5k03_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
