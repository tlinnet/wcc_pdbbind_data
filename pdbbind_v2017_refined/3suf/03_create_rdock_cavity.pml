load 02_3suf.mol2
load 3suf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3suf
show sticks, 3suf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
