load 02_4tim.mol2
load 4tim_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4tim
show sticks, 4tim_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
