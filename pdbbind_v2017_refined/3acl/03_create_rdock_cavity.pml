load 02_3acl.mol2
load 3acl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3acl
show sticks, 3acl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
