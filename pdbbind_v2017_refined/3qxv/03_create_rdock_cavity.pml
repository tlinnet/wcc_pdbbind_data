load 02_3qxv.mol2
load 3qxv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qxv
show sticks, 3qxv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
