load 02_3kdd.mol2
load 3kdd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kdd
show sticks, 3kdd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
