load 02_3iae.mol2
load 3iae_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3iae
show sticks, 3iae_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
