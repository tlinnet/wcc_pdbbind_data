load 02_4hwp.mol2
load 4hwp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4hwp
show sticks, 4hwp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
