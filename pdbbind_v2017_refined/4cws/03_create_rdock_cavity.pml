load 02_4cws.mol2
load 4cws_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cws
show sticks, 4cws_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
