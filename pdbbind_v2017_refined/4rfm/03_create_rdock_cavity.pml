load 02_4rfm.mol2
load 4rfm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rfm
show sticks, 4rfm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
