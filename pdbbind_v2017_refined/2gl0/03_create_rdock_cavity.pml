load 02_2gl0.mol2
load 2gl0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2gl0
show sticks, 2gl0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
