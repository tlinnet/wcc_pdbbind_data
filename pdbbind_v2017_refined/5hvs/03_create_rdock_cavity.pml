load 02_5hvs.mol2
load 5hvs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5hvs
show sticks, 5hvs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
