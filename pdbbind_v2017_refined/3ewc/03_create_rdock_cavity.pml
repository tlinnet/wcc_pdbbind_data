load 02_3ewc.mol2
load 3ewc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ewc
show sticks, 3ewc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
