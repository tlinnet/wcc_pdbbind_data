load 02_1drv.mol2
load 1drv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1drv
show sticks, 1drv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
