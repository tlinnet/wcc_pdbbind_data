load 02_1w0z.mol2
load 1w0z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1w0z
show sticks, 1w0z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
