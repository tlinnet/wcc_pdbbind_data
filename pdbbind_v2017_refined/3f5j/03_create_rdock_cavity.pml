load 02_3f5j.mol2
load 3f5j_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3f5j
show sticks, 3f5j_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
