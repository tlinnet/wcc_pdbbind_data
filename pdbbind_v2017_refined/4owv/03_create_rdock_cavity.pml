load 02_4owv.mol2
load 4owv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4owv
show sticks, 4owv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
