load 02_3aho.mol2
load 3aho_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3aho
show sticks, 3aho_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
