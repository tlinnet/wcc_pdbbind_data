load 02_1gar.mol2
load 1gar_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1gar
show sticks, 1gar_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
