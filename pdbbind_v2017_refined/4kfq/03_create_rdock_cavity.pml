load 02_4kfq.mol2
load 4kfq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4kfq
show sticks, 4kfq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
