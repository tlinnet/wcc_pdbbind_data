load 02_1stc.mol2
load 1stc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1stc
show sticks, 1stc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
