load 02_3nb5.mol2
load 3nb5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nb5
show sticks, 3nb5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
