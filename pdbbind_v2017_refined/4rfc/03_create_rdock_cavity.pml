load 02_4rfc.mol2
load 4rfc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rfc
show sticks, 4rfc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
