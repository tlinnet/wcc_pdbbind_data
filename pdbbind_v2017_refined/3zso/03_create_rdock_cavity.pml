load 02_3zso.mol2
load 3zso_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3zso
show sticks, 3zso_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
