load 02_5bs4.mol2
load 5bs4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5bs4
show sticks, 5bs4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
