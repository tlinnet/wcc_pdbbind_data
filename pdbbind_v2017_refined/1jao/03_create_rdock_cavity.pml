load 02_1jao.mol2
load 1jao_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1jao
show sticks, 1jao_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
