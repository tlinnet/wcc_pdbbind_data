load 02_2fxs.mol2
load 2fxs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2fxs
show sticks, 2fxs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
