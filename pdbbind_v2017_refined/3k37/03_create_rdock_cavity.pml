load 02_3k37.mol2
load 3k37_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3k37
show sticks, 3k37_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
