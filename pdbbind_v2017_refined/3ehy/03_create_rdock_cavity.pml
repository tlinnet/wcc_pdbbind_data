load 02_3ehy.mol2
load 3ehy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ehy
show sticks, 3ehy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
