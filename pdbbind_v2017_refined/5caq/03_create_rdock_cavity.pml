load 02_5caq.mol2
load 5caq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5caq
show sticks, 5caq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
