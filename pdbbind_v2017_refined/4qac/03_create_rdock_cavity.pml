load 02_4qac.mol2
load 4qac_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qac
show sticks, 4qac_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
