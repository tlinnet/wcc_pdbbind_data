load 02_5czm.mol2
load 5czm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5czm
show sticks, 5czm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
