load 02_3l0v.mol2
load 3l0v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3l0v
show sticks, 3l0v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
