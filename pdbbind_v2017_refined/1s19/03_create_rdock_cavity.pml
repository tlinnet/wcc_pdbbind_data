load 02_1s19.mol2
load 1s19_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1s19
show sticks, 1s19_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
