load 02_3p2e.mol2
load 3p2e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3p2e
show sticks, 3p2e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
