load 02_2haw.mol2
load 2haw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2haw
show sticks, 2haw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
