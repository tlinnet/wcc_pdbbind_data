load 02_3o9i.mol2
load 3o9i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3o9i
show sticks, 3o9i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
