load 02_2yhw.mol2
load 2yhw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2yhw
show sticks, 2yhw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
