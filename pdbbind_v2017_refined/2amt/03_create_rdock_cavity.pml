load 02_2amt.mol2
load 2amt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2amt
show sticks, 2amt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
