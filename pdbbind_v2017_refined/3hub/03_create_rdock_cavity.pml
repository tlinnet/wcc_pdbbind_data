load 02_3hub.mol2
load 3hub_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hub
show sticks, 3hub_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
