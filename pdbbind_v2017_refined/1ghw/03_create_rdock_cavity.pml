load 02_1ghw.mol2
load 1ghw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ghw
show sticks, 1ghw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
