load 02_2pql.mol2
load 2pql_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pql
show sticks, 2pql_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
