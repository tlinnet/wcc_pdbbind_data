load 02_1flr.mol2
load 1flr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1flr
show sticks, 1flr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
