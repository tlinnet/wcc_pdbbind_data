load 02_4baq.mol2
load 4baq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4baq
show sticks, 4baq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
