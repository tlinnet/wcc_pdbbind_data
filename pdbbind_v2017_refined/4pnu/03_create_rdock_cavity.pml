load 02_4pnu.mol2
load 4pnu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pnu
show sticks, 4pnu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
