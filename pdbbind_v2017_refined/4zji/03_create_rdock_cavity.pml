load 02_4zji.mol2
load 4zji_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zji
show sticks, 4zji_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
