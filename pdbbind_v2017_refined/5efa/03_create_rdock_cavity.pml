load 02_5efa.mol2
load 5efa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5efa
show sticks, 5efa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
