load 02_3ldp.mol2
load 3ldp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ldp
show sticks, 3ldp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
