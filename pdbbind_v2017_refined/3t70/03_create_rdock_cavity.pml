load 02_3t70.mol2
load 3t70_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3t70
show sticks, 3t70_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
