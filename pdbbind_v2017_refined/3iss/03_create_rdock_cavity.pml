load 02_3iss.mol2
load 3iss_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3iss
show sticks, 3iss_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
