load 02_3n0n.mol2
load 3n0n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3n0n
show sticks, 3n0n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
