load 02_3ipq.mol2
load 3ipq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ipq
show sticks, 3ipq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
