load 02_4sga.mol2
load 4sga_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4sga
show sticks, 4sga_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
