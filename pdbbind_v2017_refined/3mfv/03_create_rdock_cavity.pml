load 02_3mfv.mol2
load 3mfv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3mfv
show sticks, 3mfv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
