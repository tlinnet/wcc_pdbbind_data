load 02_1cps.mol2
load 1cps_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1cps
show sticks, 1cps_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
