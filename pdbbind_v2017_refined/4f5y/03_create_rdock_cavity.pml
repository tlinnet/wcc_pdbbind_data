load 02_4f5y.mol2
load 4f5y_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4f5y
show sticks, 4f5y_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
