load 02_4jne.mol2
load 4jne_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4jne
show sticks, 4jne_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
