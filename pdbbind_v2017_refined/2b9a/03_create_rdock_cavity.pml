load 02_2b9a.mol2
load 2b9a_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2b9a
show sticks, 2b9a_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
