load 02_1d4k.mol2
load 1d4k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1d4k
show sticks, 1d4k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
