load 02_3kyq.mol2
load 3kyq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kyq
show sticks, 3kyq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
