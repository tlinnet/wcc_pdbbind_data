load 02_4j22.mol2
load 4j22_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4j22
show sticks, 4j22_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
