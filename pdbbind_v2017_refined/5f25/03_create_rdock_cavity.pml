load 02_5f25.mol2
load 5f25_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5f25
show sticks, 5f25_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
