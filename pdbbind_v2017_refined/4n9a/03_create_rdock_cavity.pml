load 02_4n9a.mol2
load 4n9a_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4n9a
show sticks, 4n9a_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
