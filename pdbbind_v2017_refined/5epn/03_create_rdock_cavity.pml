load 02_5epn.mol2
load 5epn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5epn
show sticks, 5epn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
