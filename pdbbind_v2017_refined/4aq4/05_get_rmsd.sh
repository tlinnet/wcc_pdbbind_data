#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4aq4_ligand.sdf 04_out_4aq4.sd -o 04_out_4aq4_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4aq4_rmsd.sd | tee 05_get_rmsd.score
