load 02_4nbn.mol2
load 4nbn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nbn
show sticks, 4nbn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
