load 02_3ioc.mol2
load 3ioc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ioc
show sticks, 3ioc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
