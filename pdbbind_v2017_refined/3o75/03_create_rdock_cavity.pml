load 02_3o75.mol2
load 3o75_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3o75
show sticks, 3o75_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
