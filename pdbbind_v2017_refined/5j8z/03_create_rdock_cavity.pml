load 02_5j8z.mol2
load 5j8z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5j8z
show sticks, 5j8z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
