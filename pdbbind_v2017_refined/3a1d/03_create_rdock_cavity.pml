load 02_3a1d.mol2
load 3a1d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3a1d
show sticks, 3a1d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
