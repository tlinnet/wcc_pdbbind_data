load 02_3cke.mol2
load 3cke_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3cke
show sticks, 3cke_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
