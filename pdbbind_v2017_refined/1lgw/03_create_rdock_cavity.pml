load 02_1lgw.mol2
load 1lgw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lgw
show sticks, 1lgw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
