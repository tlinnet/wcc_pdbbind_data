load 02_4rpo.mol2
load 4rpo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rpo
show sticks, 4rpo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
