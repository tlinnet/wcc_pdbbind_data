load 02_4abe.mol2
load 4abe_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4abe
show sticks, 4abe_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
