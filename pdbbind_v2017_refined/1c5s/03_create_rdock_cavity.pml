load 02_1c5s.mol2
load 1c5s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1c5s
show sticks, 1c5s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
