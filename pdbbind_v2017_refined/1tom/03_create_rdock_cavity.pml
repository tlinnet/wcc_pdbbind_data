load 02_1tom.mol2
load 1tom_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1tom
show sticks, 1tom_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
