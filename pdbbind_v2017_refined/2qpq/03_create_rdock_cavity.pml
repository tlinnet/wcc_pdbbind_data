load 02_2qpq.mol2
load 2qpq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qpq
show sticks, 2qpq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
