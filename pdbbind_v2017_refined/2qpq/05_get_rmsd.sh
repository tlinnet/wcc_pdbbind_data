#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2qpq_ligand.sdf 04_out_2qpq.sd -o 04_out_2qpq_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2qpq_rmsd.sd | tee 05_get_rmsd.score
