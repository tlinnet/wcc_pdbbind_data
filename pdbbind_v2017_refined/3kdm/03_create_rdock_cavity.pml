load 02_3kdm.mol2
load 3kdm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kdm
show sticks, 3kdm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
