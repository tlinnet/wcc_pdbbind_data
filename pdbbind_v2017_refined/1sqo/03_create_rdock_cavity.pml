load 02_1sqo.mol2
load 1sqo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1sqo
show sticks, 1sqo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
