load 02_5fe7.mol2
load 5fe7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fe7
show sticks, 5fe7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
