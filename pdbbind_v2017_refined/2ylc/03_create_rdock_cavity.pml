load 02_2ylc.mol2
load 2ylc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ylc
show sticks, 2ylc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
