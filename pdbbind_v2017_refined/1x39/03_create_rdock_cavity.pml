load 02_1x39.mol2
load 1x39_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1x39
show sticks, 1x39_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
