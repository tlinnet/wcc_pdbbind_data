load 02_3ebp.mol2
load 3ebp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ebp
show sticks, 3ebp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
