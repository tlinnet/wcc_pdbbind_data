load 02_4mss.mol2
load 4mss_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4mss
show sticks, 4mss_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
