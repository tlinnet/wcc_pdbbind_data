load 02_3p5l.mol2
load 3p5l_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3p5l
show sticks, 3p5l_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
