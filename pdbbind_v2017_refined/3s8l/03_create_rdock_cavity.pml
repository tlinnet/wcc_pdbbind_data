load 02_3s8l.mol2
load 3s8l_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3s8l
show sticks, 3s8l_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
