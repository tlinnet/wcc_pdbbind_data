load 02_5e28.mol2
load 5e28_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5e28
show sticks, 5e28_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
