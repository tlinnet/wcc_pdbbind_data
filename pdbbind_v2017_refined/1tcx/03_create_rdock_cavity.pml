load 02_1tcx.mol2
load 1tcx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1tcx
show sticks, 1tcx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
