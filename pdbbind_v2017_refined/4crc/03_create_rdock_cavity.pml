load 02_4crc.mol2
load 4crc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4crc
show sticks, 4crc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
