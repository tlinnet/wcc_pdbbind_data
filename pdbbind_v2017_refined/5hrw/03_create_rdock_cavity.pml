load 02_5hrw.mol2
load 5hrw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5hrw
show sticks, 5hrw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
