#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1dud_ligand.sdf 04_out_1dud.sd -o 04_out_1dud_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1dud_rmsd.sd | tee 05_get_rmsd.score
