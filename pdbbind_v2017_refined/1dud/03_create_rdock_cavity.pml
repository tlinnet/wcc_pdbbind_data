load 02_1dud.mol2
load 1dud_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1dud
show sticks, 1dud_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
