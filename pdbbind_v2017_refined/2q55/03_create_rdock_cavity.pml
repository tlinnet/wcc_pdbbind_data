load 02_2q55.mol2
load 2q55_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2q55
show sticks, 2q55_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
