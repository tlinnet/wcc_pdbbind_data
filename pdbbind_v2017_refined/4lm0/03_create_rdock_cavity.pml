load 02_4lm0.mol2
load 4lm0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4lm0
show sticks, 4lm0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
