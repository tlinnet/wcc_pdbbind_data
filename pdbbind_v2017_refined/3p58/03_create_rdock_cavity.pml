load 02_3p58.mol2
load 3p58_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3p58
show sticks, 3p58_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
