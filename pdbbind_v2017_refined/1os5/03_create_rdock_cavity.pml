load 02_1os5.mol2
load 1os5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1os5
show sticks, 1os5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
