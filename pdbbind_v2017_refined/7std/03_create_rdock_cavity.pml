load 02_7std.mol2
load 7std_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_7std
show sticks, 7std_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
