load 02_1c4u.mol2
load 1c4u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1c4u
show sticks, 1c4u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
