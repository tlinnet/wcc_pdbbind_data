load 02_5ikb.mol2
load 5ikb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ikb
show sticks, 5ikb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
