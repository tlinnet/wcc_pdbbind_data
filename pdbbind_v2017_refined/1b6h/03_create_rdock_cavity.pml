load 02_1b6h.mol2
load 1b6h_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1b6h
show sticks, 1b6h_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
