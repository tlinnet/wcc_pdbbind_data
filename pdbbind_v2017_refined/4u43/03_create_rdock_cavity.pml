load 02_4u43.mol2
load 4u43_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4u43
show sticks, 4u43_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
