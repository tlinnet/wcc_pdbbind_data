load 02_1u1b.mol2
load 1u1b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1u1b
show sticks, 1u1b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
