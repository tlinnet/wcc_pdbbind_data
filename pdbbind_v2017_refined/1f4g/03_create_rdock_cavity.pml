load 02_1f4g.mol2
load 1f4g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1f4g
show sticks, 1f4g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
