load 02_4b0b.mol2
load 4b0b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4b0b
show sticks, 4b0b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
