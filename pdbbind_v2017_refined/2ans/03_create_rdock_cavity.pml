load 02_2ans.mol2
load 2ans_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ans
show sticks, 2ans_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
