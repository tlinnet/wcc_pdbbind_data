load 02_4gfm.mol2
load 4gfm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4gfm
show sticks, 4gfm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
