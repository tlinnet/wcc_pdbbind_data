load 02_4x8v.mol2
load 4x8v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4x8v
show sticks, 4x8v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
