load 02_4g8v.mol2
load 4g8v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4g8v
show sticks, 4g8v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
