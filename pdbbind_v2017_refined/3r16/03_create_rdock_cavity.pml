load 02_3r16.mol2
load 3r16_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3r16
show sticks, 3r16_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
