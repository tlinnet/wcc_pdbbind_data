load 02_1rpf.mol2
load 1rpf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1rpf
show sticks, 1rpf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
