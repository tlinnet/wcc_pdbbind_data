load 02_1fch.mol2
load 1fch_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fch
show sticks, 1fch_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
