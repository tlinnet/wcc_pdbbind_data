load 02_1wvj.mol2
load 1wvj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1wvj
show sticks, 1wvj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
