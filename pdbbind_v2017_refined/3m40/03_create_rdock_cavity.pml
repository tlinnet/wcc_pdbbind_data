load 02_3m40.mol2
load 3m40_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3m40
show sticks, 3m40_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
