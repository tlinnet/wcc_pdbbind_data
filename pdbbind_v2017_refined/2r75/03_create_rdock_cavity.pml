load 02_2r75.mol2
load 2r75_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2r75
show sticks, 2r75_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
