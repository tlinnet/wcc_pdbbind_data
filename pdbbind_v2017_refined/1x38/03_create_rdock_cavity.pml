load 02_1x38.mol2
load 1x38_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1x38
show sticks, 1x38_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
