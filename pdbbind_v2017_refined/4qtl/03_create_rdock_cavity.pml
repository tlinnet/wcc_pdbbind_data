load 02_4qtl.mol2
load 4qtl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qtl
show sticks, 4qtl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
