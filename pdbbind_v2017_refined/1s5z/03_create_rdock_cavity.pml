load 02_1s5z.mol2
load 1s5z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1s5z
show sticks, 1s5z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
