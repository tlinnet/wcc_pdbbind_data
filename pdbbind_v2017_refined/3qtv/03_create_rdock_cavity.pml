load 02_3qtv.mol2
load 3qtv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qtv
show sticks, 3qtv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
