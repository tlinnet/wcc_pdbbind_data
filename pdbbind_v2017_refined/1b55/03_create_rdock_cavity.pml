load 02_1b55.mol2
load 1b55_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1b55
show sticks, 1b55_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
