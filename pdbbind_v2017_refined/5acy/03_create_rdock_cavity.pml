load 02_5acy.mol2
load 5acy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5acy
show sticks, 5acy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
