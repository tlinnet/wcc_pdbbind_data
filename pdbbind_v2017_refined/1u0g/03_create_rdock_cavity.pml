load 02_1u0g.mol2
load 1u0g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1u0g
show sticks, 1u0g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
