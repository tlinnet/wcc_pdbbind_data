load 02_2exm.mol2
load 2exm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2exm
show sticks, 2exm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
