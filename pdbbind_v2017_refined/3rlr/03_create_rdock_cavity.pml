load 02_3rlr.mol2
load 3rlr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3rlr
show sticks, 3rlr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
