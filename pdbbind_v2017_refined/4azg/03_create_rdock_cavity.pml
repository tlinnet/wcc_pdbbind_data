load 02_4azg.mol2
load 4azg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4azg
show sticks, 4azg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
