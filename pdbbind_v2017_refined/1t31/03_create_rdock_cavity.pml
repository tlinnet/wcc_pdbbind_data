load 02_1t31.mol2
load 1t31_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1t31
show sticks, 1t31_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
