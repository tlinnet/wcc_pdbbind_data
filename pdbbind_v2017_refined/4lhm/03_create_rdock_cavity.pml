load 02_4lhm.mol2
load 4lhm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4lhm
show sticks, 4lhm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
