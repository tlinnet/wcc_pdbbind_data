load 02_4h42.mol2
load 4h42_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4h42
show sticks, 4h42_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
