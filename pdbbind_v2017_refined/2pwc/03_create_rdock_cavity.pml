load 02_2pwc.mol2
load 2pwc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pwc
show sticks, 2pwc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
