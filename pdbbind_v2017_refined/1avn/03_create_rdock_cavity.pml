load 02_1avn.mol2
load 1avn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1avn
show sticks, 1avn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
