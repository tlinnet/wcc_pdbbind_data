#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4yk0_ligand.sdf 04_out_4yk0.sd -o 04_out_4yk0_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4yk0_rmsd.sd | tee 05_get_rmsd.score
