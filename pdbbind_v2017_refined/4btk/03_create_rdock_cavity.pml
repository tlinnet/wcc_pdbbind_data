load 02_4btk.mol2
load 4btk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4btk
show sticks, 4btk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
