load 02_1if7.mol2
load 1if7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1if7
show sticks, 1if7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
