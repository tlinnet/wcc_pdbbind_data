load 02_5d2r.mol2
load 5d2r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5d2r
show sticks, 5d2r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
