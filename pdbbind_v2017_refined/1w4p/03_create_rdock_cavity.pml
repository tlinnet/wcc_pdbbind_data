load 02_1w4p.mol2
load 1w4p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1w4p
show sticks, 1w4p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
