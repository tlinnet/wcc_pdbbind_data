load 02_2ole.mol2
load 2ole_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ole
show sticks, 2ole_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
