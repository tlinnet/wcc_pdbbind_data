load 02_5edd.mol2
load 5edd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5edd
show sticks, 5edd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
