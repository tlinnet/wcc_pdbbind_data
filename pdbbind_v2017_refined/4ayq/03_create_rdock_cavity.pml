load 02_4ayq.mol2
load 4ayq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ayq
show sticks, 4ayq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
