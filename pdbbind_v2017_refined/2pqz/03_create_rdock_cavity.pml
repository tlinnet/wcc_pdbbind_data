load 02_2pqz.mol2
load 2pqz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pqz
show sticks, 2pqz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
