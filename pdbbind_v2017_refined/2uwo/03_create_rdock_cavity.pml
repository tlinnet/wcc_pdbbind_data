load 02_2uwo.mol2
load 2uwo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2uwo
show sticks, 2uwo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
