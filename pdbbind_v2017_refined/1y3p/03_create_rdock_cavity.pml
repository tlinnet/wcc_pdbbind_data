load 02_1y3p.mol2
load 1y3p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1y3p
show sticks, 1y3p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
