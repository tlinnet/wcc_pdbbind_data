load 02_1qxk.mol2
load 1qxk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qxk
show sticks, 1qxk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
