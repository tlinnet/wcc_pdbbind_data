load 02_2pvl.mol2
load 2pvl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pvl
show sticks, 2pvl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
