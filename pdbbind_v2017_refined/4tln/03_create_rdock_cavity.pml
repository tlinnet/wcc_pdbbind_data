load 02_4tln.mol2
load 4tln_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4tln
show sticks, 4tln_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
