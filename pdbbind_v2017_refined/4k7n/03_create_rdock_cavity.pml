load 02_4k7n.mol2
load 4k7n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4k7n
show sticks, 4k7n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
