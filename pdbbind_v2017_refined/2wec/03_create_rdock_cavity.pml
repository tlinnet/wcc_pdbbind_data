load 02_2wec.mol2
load 2wec_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wec
show sticks, 2wec_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
