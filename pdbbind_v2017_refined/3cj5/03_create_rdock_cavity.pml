load 02_3cj5.mol2
load 3cj5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3cj5
show sticks, 3cj5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
