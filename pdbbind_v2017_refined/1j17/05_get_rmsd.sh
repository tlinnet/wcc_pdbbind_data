#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1j17_ligand.sdf 04_out_1j17.sd -o 04_out_1j17_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1j17_rmsd.sd | tee 05_get_rmsd.score
