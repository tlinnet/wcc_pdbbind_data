load 02_1f57.mol2
load 1f57_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1f57
show sticks, 1f57_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
