#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3veh_ligand.sdf 04_out_3veh.sd -o 04_out_3veh_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3veh_rmsd.sd | tee 05_get_rmsd.score
