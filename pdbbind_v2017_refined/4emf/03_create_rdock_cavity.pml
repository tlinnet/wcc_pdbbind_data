load 02_4emf.mol2
load 4emf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4emf
show sticks, 4emf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
