load 02_4ibe.mol2
load 4ibe_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ibe
show sticks, 4ibe_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
