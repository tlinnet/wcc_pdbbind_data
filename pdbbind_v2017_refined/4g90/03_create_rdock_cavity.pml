load 02_4g90.mol2
load 4g90_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4g90
show sticks, 4g90_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
