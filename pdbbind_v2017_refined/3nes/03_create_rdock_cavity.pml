load 02_3nes.mol2
load 3nes_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nes
show sticks, 3nes_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
