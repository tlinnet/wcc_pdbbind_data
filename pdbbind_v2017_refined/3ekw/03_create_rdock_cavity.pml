load 02_3ekw.mol2
load 3ekw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ekw
show sticks, 3ekw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
