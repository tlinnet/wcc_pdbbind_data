load 02_1mjj.mol2
load 1mjj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1mjj
show sticks, 1mjj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
