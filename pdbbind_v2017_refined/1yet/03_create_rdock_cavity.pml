load 02_1yet.mol2
load 1yet_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1yet
show sticks, 1yet_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
