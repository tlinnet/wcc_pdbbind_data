load 02_1wht.mol2
load 1wht_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1wht
show sticks, 1wht_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
