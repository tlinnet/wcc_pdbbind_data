load 02_3g0i.mol2
load 3g0i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3g0i
show sticks, 3g0i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
