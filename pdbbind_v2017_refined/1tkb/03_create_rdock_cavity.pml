load 02_1tkb.mol2
load 1tkb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1tkb
show sticks, 1tkb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
