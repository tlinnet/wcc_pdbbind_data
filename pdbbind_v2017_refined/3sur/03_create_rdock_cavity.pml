load 02_3sur.mol2
load 3sur_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3sur
show sticks, 3sur_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
