load 02_5cso.mol2
load 5cso_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5cso
show sticks, 5cso_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
