load 02_4ddk.mol2
load 4ddk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ddk
show sticks, 4ddk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
