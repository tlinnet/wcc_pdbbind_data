load 02_3wzn.mol2
load 3wzn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3wzn
show sticks, 3wzn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
