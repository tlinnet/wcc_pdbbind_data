load 02_4y2q.mol2
load 4y2q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4y2q
show sticks, 4y2q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
