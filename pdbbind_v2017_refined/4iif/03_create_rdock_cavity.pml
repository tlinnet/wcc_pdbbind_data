load 02_4iif.mol2
load 4iif_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4iif
show sticks, 4iif_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
