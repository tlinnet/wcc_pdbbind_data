load 02_1qan.mol2
load 1qan_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qan
show sticks, 1qan_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
