load 02_4z2b.mol2
load 4z2b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4z2b
show sticks, 4z2b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
