#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4z2b_ligand.sdf 04_out_4z2b.sd -o 04_out_4z2b_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4z2b_rmsd.sd | tee 05_get_rmsd.score
