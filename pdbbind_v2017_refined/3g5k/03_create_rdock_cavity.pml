load 02_3g5k.mol2
load 3g5k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3g5k
show sticks, 3g5k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
