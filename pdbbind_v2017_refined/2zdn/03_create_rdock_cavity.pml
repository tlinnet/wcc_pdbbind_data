load 02_2zdn.mol2
load 2zdn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zdn
show sticks, 2zdn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
