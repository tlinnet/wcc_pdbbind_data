#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4avi_ligand.sdf 04_out_4avi.sd -o 04_out_4avi_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4avi_rmsd.sd | tee 05_get_rmsd.score
