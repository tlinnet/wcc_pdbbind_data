load 02_5g4n.mol2
load 5g4n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5g4n
show sticks, 5g4n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
