load 02_4rhx.mol2
load 4rhx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rhx
show sticks, 4rhx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
