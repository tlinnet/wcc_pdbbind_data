load 02_3gvb.mol2
load 3gvb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gvb
show sticks, 3gvb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
