load 02_1njd.mol2
load 1njd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1njd
show sticks, 1njd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
