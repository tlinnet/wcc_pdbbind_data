load 02_1c84.mol2
load 1c84_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1c84
show sticks, 1c84_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
