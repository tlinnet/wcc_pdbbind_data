load 02_1mq5.mol2
load 1mq5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1mq5
show sticks, 1mq5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
