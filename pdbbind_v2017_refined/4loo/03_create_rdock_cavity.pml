load 02_4loo.mol2
load 4loo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4loo
show sticks, 4loo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
