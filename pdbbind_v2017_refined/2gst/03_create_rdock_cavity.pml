load 02_2gst.mol2
load 2gst_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2gst
show sticks, 2gst_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
