load 02_3b1m.mol2
load 3b1m_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3b1m
show sticks, 3b1m_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
