load 02_1jsv.mol2
load 1jsv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1jsv
show sticks, 1jsv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
