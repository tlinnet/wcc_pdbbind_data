load 02_1w3k.mol2
load 1w3k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1w3k
show sticks, 1w3k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
