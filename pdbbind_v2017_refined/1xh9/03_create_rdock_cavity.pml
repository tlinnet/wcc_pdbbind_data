load 02_1xh9.mol2
load 1xh9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1xh9
show sticks, 1xh9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
