load 02_1sqa.mol2
load 1sqa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1sqa
show sticks, 1sqa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
