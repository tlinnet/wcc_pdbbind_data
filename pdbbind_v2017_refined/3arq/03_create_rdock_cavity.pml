load 02_3arq.mol2
load 3arq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3arq
show sticks, 3arq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
