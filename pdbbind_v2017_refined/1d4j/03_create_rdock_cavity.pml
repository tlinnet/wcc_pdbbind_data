load 02_1d4j.mol2
load 1d4j_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1d4j
show sticks, 1d4j_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
