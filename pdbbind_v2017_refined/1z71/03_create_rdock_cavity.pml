load 02_1z71.mol2
load 1z71_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1z71
show sticks, 1z71_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
