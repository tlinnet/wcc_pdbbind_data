load 02_1rnm.mol2
load 1rnm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1rnm
show sticks, 1rnm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
