load 02_4cpz.mol2
load 4cpz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cpz
show sticks, 4cpz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
