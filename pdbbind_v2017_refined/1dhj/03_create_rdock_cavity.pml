load 02_1dhj.mol2
load 1dhj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1dhj
show sticks, 1dhj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
