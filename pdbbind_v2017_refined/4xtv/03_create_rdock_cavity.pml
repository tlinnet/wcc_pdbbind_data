load 02_4xtv.mol2
load 4xtv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4xtv
show sticks, 4xtv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
