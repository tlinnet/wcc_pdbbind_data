load 02_4bam.mol2
load 4bam_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4bam
show sticks, 4bam_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
