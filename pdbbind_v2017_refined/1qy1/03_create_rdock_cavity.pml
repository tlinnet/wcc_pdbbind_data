load 02_1qy1.mol2
load 1qy1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qy1
show sticks, 1qy1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
