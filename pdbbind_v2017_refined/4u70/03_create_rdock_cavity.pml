load 02_4u70.mol2
load 4u70_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4u70
show sticks, 4u70_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
