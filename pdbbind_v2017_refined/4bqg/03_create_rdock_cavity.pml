load 02_4bqg.mol2
load 4bqg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4bqg
show sticks, 4bqg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
