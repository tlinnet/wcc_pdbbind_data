load 02_4b3c.mol2
load 4b3c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4b3c
show sticks, 4b3c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
