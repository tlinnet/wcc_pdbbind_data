#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4l4v_ligand.sdf 04_out_4l4v.sd -o 04_out_4l4v_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4l4v_rmsd.sd | tee 05_get_rmsd.score
