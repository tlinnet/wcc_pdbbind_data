load 02_2pym.mol2
load 2pym_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pym
show sticks, 2pym_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
