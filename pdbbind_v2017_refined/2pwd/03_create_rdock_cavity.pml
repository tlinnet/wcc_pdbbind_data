load 02_2pwd.mol2
load 2pwd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pwd
show sticks, 2pwd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
