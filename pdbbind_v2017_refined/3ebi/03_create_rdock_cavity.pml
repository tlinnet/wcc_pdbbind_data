load 02_3ebi.mol2
load 3ebi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ebi
show sticks, 3ebi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
