load 02_2uxz.mol2
load 2uxz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2uxz
show sticks, 2uxz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
