#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4bt3_ligand.sdf 04_out_4bt3.sd -o 04_out_4bt3_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4bt3_rmsd.sd | tee 05_get_rmsd.score
