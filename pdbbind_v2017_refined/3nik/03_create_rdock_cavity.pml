load 02_3nik.mol2
load 3nik_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nik
show sticks, 3nik_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
