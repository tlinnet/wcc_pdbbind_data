load 02_2q64.mol2
load 2q64_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2q64
show sticks, 2q64_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
