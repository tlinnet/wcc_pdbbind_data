load 02_1qk3.mol2
load 1qk3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qk3
show sticks, 1qk3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
