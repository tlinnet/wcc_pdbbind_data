load 02_4nwc.mol2
load 4nwc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nwc
show sticks, 4nwc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
