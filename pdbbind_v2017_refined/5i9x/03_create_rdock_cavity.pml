load 02_5i9x.mol2
load 5i9x_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5i9x
show sticks, 5i9x_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
