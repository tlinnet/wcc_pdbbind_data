load 02_3tvc.mol2
load 3tvc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3tvc
show sticks, 3tvc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
