load 02_4o3f.mol2
load 4o3f_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4o3f
show sticks, 4o3f_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
