load 02_5d24.mol2
load 5d24_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5d24
show sticks, 5d24_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
