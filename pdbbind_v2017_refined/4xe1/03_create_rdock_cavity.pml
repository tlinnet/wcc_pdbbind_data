load 02_4xe1.mol2
load 4xe1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4xe1
show sticks, 4xe1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
