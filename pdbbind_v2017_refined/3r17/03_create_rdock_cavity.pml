load 02_3r17.mol2
load 3r17_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3r17
show sticks, 3r17_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
