load 02_2qi0.mol2
load 2qi0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qi0
show sticks, 2qi0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
