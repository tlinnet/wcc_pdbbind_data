load 02_3q1x.mol2
load 3q1x_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3q1x
show sticks, 3q1x_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
