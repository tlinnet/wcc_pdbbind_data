load 02_3i9g.mol2
load 3i9g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3i9g
show sticks, 3i9g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
