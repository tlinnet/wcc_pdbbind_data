load 02_4igt.mol2
load 4igt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4igt
show sticks, 4igt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
