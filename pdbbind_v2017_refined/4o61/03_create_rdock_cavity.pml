load 02_4o61.mol2
load 4o61_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4o61
show sticks, 4o61_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
