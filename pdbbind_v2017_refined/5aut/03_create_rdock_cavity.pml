load 02_5aut.mol2
load 5aut_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5aut
show sticks, 5aut_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
