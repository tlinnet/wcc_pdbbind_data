load 02_1ajp.mol2
load 1ajp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ajp
show sticks, 1ajp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
