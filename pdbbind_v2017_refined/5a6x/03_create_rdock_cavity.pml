load 02_5a6x.mol2
load 5a6x_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5a6x
show sticks, 5a6x_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
