load 02_4z0q.mol2
load 4z0q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4z0q
show sticks, 4z0q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
