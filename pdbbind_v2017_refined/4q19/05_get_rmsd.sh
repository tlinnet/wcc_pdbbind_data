#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4q19_ligand.sdf 04_out_4q19.sd -o 04_out_4q19_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4q19_rmsd.sd | tee 05_get_rmsd.score
