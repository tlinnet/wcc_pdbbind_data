load 02_4q19.mol2
load 4q19_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4q19
show sticks, 4q19_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
