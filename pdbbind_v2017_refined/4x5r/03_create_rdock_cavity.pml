load 02_4x5r.mol2
load 4x5r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4x5r
show sticks, 4x5r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
