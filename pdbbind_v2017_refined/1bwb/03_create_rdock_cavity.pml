load 02_1bwb.mol2
load 1bwb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bwb
show sticks, 1bwb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
