load 02_3i51.mol2
load 3i51_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3i51
show sticks, 3i51_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
