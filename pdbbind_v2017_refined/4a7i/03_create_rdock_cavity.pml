load 02_4a7i.mol2
load 4a7i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4a7i
show sticks, 4a7i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
