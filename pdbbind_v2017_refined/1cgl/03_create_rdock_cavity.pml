load 02_1cgl.mol2
load 1cgl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1cgl
show sticks, 1cgl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
