load 02_4rsk.mol2
load 4rsk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rsk
show sticks, 4rsk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
