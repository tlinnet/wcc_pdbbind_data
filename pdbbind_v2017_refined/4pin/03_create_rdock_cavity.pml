load 02_4pin.mol2
load 4pin_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pin
show sticks, 4pin_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
