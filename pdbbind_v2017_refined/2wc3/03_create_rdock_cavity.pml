load 02_2wc3.mol2
load 2wc3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wc3
show sticks, 2wc3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
