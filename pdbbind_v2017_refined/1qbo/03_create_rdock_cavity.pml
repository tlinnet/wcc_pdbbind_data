load 02_1qbo.mol2
load 1qbo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qbo
show sticks, 1qbo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
