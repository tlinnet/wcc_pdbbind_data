load 02_3tmk.mol2
load 3tmk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3tmk
show sticks, 3tmk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
