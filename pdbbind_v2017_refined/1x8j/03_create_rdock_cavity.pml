load 02_1x8j.mol2
load 1x8j_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1x8j
show sticks, 1x8j_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
