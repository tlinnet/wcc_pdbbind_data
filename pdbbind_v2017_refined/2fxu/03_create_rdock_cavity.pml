load 02_2fxu.mol2
load 2fxu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2fxu
show sticks, 2fxu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
