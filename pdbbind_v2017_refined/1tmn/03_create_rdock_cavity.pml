load 02_1tmn.mol2
load 1tmn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1tmn
show sticks, 1tmn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
