load 02_1t7j.mol2
load 1t7j_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1t7j
show sticks, 1t7j_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
