load 02_5a7b.mol2
load 5a7b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5a7b
show sticks, 5a7b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
