load 02_4eoh.mol2
load 4eoh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4eoh
show sticks, 4eoh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
