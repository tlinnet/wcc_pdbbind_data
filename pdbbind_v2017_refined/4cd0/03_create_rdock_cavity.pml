load 02_4cd0.mol2
load 4cd0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cd0
show sticks, 4cd0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
