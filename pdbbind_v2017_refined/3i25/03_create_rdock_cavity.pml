load 02_3i25.mol2
load 3i25_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3i25
show sticks, 3i25_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
