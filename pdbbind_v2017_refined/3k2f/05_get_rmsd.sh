#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3k2f_ligand.sdf 04_out_3k2f.sd -o 04_out_3k2f_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3k2f_rmsd.sd | tee 05_get_rmsd.score
