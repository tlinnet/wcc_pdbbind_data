load 02_3k2f.mol2
load 3k2f_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3k2f
show sticks, 3k2f_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
