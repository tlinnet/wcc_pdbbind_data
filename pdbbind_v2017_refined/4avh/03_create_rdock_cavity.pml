load 02_4avh.mol2
load 4avh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4avh
show sticks, 4avh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
