load 02_1hsl.mol2
load 1hsl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hsl
show sticks, 1hsl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
