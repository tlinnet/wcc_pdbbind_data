load 02_3bwj.mol2
load 3bwj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3bwj
show sticks, 3bwj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
