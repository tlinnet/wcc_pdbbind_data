load 02_5e13.mol2
load 5e13_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5e13
show sticks, 5e13_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
