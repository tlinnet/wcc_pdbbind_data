load 02_2i80.mol2
load 2i80_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i80
show sticks, 2i80_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
