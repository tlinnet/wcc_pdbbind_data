load 02_2psv.mol2
load 2psv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2psv
show sticks, 2psv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
