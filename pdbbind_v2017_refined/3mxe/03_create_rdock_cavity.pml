load 02_3mxe.mol2
load 3mxe_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3mxe
show sticks, 3mxe_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
