load 02_5b2d.mol2
load 5b2d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5b2d
show sticks, 5b2d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
