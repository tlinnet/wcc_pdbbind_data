load 02_4cgi.mol2
load 4cgi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cgi
show sticks, 4cgi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
