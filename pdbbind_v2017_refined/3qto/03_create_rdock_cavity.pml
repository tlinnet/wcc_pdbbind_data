load 02_3qto.mol2
load 3qto_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qto
show sticks, 3qto_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
