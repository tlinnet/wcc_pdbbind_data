load 02_2x96.mol2
load 2x96_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2x96
show sticks, 2x96_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
