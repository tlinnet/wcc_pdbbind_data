load 02_1li3.mol2
load 1li3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1li3
show sticks, 1li3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
