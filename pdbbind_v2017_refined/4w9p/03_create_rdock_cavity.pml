load 02_4w9p.mol2
load 4w9p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4w9p
show sticks, 4w9p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
