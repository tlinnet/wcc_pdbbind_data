load 02_3hkn.mol2
load 3hkn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hkn
show sticks, 3hkn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
