load 02_4r4o.mol2
load 4r4o_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4r4o
show sticks, 4r4o_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
