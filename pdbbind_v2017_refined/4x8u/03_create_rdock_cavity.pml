load 02_4x8u.mol2
load 4x8u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4x8u
show sticks, 4x8u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
