load 02_2gzl.mol2
load 2gzl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2gzl
show sticks, 2gzl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
