#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2r0z_ligand.sdf 04_out_2r0z.sd -o 04_out_2r0z_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2r0z_rmsd.sd | tee 05_get_rmsd.score
