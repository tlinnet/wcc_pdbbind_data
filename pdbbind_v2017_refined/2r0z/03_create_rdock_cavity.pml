load 02_2r0z.mol2
load 2r0z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2r0z
show sticks, 2r0z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
