load 02_3l4u.mol2
load 3l4u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3l4u
show sticks, 3l4u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
