load 02_4nbk.mol2
load 4nbk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nbk
show sticks, 4nbk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
