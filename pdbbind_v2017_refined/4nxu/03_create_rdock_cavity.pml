load 02_4nxu.mol2
load 4nxu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nxu
show sticks, 4nxu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
