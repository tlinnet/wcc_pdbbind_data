load 02_5e2k.mol2
load 5e2k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5e2k
show sticks, 5e2k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
