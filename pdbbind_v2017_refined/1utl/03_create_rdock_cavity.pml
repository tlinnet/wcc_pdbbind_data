load 02_1utl.mol2
load 1utl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1utl
show sticks, 1utl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
