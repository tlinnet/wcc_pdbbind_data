load 02_2idw.mol2
load 2idw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2idw
show sticks, 2idw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
