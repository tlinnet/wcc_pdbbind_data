load 02_1n1m.mol2
load 1n1m_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1n1m
show sticks, 1n1m_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
