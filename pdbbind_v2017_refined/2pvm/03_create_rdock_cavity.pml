load 02_2pvm.mol2
load 2pvm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pvm
show sticks, 2pvm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
