load 02_5sym.mol2
load 5sym_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5sym
show sticks, 5sym_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
