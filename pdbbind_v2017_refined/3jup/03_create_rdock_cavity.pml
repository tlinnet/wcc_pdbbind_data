load 02_3jup.mol2
load 3jup_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3jup
show sticks, 3jup_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
