load 02_1oss.mol2
load 1oss_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1oss
show sticks, 1oss_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
