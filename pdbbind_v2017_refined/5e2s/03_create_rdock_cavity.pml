load 02_5e2s.mol2
load 5e2s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5e2s
show sticks, 5e2s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
