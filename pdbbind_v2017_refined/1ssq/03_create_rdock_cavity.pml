load 02_1ssq.mol2
load 1ssq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ssq
show sticks, 1ssq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
