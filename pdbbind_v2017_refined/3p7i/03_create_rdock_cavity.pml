load 02_3p7i.mol2
load 3p7i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3p7i
show sticks, 3p7i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
