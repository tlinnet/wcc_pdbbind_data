load 02_5f2u.mol2
load 5f2u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5f2u
show sticks, 5f2u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
