load 02_3gi5.mol2
load 3gi5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gi5
show sticks, 3gi5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
