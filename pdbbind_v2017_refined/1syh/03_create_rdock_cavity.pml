load 02_1syh.mol2
load 1syh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1syh
show sticks, 1syh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
