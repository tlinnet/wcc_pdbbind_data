load 02_3n86.mol2
load 3n86_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3n86
show sticks, 3n86_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
