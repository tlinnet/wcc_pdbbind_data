load 02_3ujd.mol2
load 3ujd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ujd
show sticks, 3ujd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
