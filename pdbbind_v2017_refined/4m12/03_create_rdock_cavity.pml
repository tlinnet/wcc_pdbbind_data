load 02_4m12.mol2
load 4m12_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4m12
show sticks, 4m12_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
