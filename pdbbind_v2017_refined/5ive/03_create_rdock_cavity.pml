load 02_5ive.mol2
load 5ive_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ive
show sticks, 5ive_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
