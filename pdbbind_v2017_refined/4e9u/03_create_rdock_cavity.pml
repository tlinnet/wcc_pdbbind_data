load 02_4e9u.mol2
load 4e9u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4e9u
show sticks, 4e9u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
