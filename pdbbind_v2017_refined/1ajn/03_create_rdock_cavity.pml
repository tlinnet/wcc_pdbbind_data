load 02_1ajn.mol2
load 1ajn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ajn
show sticks, 1ajn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
