load 02_5sz0.mol2
load 5sz0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5sz0
show sticks, 5sz0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
