load 02_3bbb.mol2
load 3bbb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3bbb
show sticks, 3bbb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
