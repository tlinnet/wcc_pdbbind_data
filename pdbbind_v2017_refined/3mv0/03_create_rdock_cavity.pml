load 02_3mv0.mol2
load 3mv0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3mv0
show sticks, 3mv0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
