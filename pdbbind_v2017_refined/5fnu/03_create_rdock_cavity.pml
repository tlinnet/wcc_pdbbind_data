load 02_5fnu.mol2
load 5fnu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fnu
show sticks, 5fnu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
