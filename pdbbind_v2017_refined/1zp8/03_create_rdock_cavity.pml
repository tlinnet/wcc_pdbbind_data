load 02_1zp8.mol2
load 1zp8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1zp8
show sticks, 1zp8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
