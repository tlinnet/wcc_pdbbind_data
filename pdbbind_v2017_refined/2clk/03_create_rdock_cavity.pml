load 02_2clk.mol2
load 2clk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2clk
show sticks, 2clk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
