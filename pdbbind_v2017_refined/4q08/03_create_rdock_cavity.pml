load 02_4q08.mol2
load 4q08_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4q08
show sticks, 4q08_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
