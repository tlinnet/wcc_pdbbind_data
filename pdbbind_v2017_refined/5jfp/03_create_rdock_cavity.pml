load 02_5jfp.mol2
load 5jfp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5jfp
show sticks, 5jfp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
