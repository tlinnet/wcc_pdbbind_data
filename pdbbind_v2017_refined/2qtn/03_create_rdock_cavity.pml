load 02_2qtn.mol2
load 2qtn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qtn
show sticks, 2qtn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
