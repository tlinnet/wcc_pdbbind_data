load 02_4epy.mol2
load 4epy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4epy
show sticks, 4epy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
