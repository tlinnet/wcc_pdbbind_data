load 02_1ebz.mol2
load 1ebz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ebz
show sticks, 1ebz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
