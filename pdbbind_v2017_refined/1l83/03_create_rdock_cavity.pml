load 02_1l83.mol2
load 1l83_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1l83
show sticks, 1l83_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
