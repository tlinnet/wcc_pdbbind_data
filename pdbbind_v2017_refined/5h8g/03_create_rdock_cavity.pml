load 02_5h8g.mol2
load 5h8g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5h8g
show sticks, 5h8g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
