#!/usr/bin/env bash

rbdock -i 5ant_ligand.sdf -o 04_out_5ant -r 03_create_rdock_cavity.prm -p dock.prm -n 1 | tee 04_perform_rdock.log

echo $SECONDS > 04_perform_rdock.sec
times > 04_perform_rdock.time

