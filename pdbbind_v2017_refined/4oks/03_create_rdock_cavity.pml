load 02_4oks.mol2
load 4oks_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4oks
show sticks, 4oks_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
