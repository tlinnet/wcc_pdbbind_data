load 02_4mrg.mol2
load 4mrg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4mrg
show sticks, 4mrg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
