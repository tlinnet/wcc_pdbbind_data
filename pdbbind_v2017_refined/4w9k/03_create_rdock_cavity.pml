load 02_4w9k.mol2
load 4w9k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4w9k
show sticks, 4w9k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
