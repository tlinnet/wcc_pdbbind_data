load 02_1w4q.mol2
load 1w4q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1w4q
show sticks, 1w4q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
