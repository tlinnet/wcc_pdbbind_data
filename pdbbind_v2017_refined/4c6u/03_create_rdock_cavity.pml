load 02_4c6u.mol2
load 4c6u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4c6u
show sticks, 4c6u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
