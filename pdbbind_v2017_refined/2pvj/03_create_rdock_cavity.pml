load 02_2pvj.mol2
load 2pvj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pvj
show sticks, 2pvj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
