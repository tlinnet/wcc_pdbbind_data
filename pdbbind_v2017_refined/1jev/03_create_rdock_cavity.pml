load 02_1jev.mol2
load 1jev_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1jev
show sticks, 1jev_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
