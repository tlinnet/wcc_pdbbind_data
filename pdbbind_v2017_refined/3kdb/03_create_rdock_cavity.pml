load 02_3kdb.mol2
load 3kdb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kdb
show sticks, 3kdb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
