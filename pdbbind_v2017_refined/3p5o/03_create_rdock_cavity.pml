load 02_3p5o.mol2
load 3p5o_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3p5o
show sticks, 3p5o_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
