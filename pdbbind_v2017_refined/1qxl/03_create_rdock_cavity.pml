load 02_1qxl.mol2
load 1qxl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qxl
show sticks, 1qxl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
