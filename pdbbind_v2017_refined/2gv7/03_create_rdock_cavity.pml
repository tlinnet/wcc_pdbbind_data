load 02_2gv7.mol2
load 2gv7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2gv7
show sticks, 2gv7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
