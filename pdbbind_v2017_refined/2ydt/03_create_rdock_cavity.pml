load 02_2ydt.mol2
load 2ydt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ydt
show sticks, 2ydt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
