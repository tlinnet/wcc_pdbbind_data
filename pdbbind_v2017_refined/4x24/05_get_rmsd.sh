#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4x24_ligand.sdf 04_out_4x24.sd -o 04_out_4x24_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4x24_rmsd.sd | tee 05_get_rmsd.score
