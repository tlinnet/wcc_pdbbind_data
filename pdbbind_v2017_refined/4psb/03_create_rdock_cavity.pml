load 02_4psb.mol2
load 4psb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4psb
show sticks, 4psb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
