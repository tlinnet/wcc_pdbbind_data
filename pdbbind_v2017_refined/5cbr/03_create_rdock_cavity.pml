load 02_5cbr.mol2
load 5cbr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5cbr
show sticks, 5cbr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
