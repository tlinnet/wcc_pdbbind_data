load 02_4rrg.mol2
load 4rrg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rrg
show sticks, 4rrg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
