load 02_1atl.mol2
load 1atl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1atl
show sticks, 1atl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
