load 02_3f48.mol2
load 3f48_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3f48
show sticks, 3f48_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
