load 02_4asd.mol2
load 4asd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4asd
show sticks, 4asd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
