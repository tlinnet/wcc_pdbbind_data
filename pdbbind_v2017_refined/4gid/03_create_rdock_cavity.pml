load 02_4gid.mol2
load 4gid_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4gid
show sticks, 4gid_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
