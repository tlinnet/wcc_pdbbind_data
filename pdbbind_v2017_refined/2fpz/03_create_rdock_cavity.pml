load 02_2fpz.mol2
load 2fpz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2fpz
show sticks, 2fpz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
