load 02_4b9k.mol2
load 4b9k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4b9k
show sticks, 4b9k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
