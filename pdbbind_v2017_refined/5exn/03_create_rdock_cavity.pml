load 02_5exn.mol2
load 5exn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5exn
show sticks, 5exn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
