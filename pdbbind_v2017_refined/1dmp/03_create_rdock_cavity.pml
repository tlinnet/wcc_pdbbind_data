load 02_1dmp.mol2
load 1dmp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1dmp
show sticks, 1dmp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
