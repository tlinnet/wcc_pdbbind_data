load 02_4l6t.mol2
load 4l6t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4l6t
show sticks, 4l6t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
