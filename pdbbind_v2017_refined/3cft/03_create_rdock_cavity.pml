load 02_3cft.mol2
load 3cft_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3cft
show sticks, 3cft_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
