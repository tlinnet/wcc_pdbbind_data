load 02_2ot1.mol2
load 2ot1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ot1
show sticks, 2ot1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
