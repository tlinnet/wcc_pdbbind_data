load 02_3qqs.mol2
load 3qqs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qqs
show sticks, 3qqs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
