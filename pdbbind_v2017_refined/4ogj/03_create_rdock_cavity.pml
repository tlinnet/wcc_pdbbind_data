load 02_4ogj.mol2
load 4ogj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ogj
show sticks, 4ogj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
