load 02_1e6s.mol2
load 1e6s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1e6s
show sticks, 1e6s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
