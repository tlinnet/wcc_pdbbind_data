load 02_3rtf.mol2
load 3rtf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3rtf
show sticks, 3rtf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
