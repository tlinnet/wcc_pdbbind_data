load 02_3hl5.mol2
load 3hl5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hl5
show sticks, 3hl5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
