load 02_4yml.mol2
load 4yml_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4yml
show sticks, 4yml_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
