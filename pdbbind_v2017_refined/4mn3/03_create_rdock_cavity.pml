load 02_4mn3.mol2
load 4mn3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4mn3
show sticks, 4mn3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
