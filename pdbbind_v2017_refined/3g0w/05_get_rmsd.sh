#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3g0w_ligand.sdf 04_out_3g0w.sd -o 04_out_3g0w_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3g0w_rmsd.sd | tee 05_get_rmsd.score
