load 02_1k6t.mol2
load 1k6t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1k6t
show sticks, 1k6t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
