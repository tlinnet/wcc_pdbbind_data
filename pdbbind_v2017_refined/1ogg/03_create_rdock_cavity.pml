load 02_1ogg.mol2
load 1ogg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ogg
show sticks, 1ogg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
