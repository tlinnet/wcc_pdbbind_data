load 02_4elf.mol2
load 4elf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4elf
show sticks, 4elf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
