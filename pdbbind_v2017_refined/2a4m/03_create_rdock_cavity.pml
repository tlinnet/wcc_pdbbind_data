load 02_2a4m.mol2
load 2a4m_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2a4m
show sticks, 2a4m_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
