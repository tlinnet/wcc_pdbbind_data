load 02_3iub.mol2
load 3iub_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3iub
show sticks, 3iub_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
