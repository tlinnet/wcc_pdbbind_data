load 02_3g1v.mol2
load 3g1v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3g1v
show sticks, 3g1v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
