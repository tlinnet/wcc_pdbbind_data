load 02_4ysl.mol2
load 4ysl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ysl
show sticks, 4ysl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
