load 02_4yes.mol2
load 4yes_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4yes
show sticks, 4yes_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
