load 02_3p9l.mol2
load 3p9l_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3p9l
show sticks, 3p9l_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
