load 02_3lea.mol2
load 3lea_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3lea
show sticks, 3lea_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
