load 02_2wk6.mol2
load 2wk6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wk6
show sticks, 2wk6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
