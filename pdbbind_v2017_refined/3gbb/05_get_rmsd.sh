#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3gbb_ligand.sdf 04_out_3gbb.sd -o 04_out_3gbb_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3gbb_rmsd.sd | tee 05_get_rmsd.score
