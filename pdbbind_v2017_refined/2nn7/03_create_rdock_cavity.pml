load 02_2nn7.mol2
load 2nn7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2nn7
show sticks, 2nn7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
