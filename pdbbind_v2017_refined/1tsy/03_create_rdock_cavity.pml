load 02_1tsy.mol2
load 1tsy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1tsy
show sticks, 1tsy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
