load 02_4djp.mol2
load 4djp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4djp
show sticks, 4djp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
