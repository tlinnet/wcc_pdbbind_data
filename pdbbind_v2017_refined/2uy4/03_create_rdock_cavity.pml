load 02_2uy4.mol2
load 2uy4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2uy4
show sticks, 2uy4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
