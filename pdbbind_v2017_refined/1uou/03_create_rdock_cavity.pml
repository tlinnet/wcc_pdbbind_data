load 02_1uou.mol2
load 1uou_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1uou
show sticks, 1uou_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
