load 02_3o8p.mol2
load 3o8p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3o8p
show sticks, 3o8p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
