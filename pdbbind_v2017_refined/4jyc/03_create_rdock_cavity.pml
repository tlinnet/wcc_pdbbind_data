load 02_4jyc.mol2
load 4jyc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4jyc
show sticks, 4jyc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
