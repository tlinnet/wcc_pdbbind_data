load 02_3vvy.mol2
load 3vvy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3vvy
show sticks, 3vvy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
