load 02_5dfp.mol2
load 5dfp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5dfp
show sticks, 5dfp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
