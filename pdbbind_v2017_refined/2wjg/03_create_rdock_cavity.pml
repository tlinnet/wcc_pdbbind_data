load 02_2wjg.mol2
load 2wjg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wjg
show sticks, 2wjg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
