load 02_2o4s.mol2
load 2o4s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2o4s
show sticks, 2o4s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
