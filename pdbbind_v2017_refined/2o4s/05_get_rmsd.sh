#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2o4s_ligand.sdf 04_out_2o4s.sd -o 04_out_2o4s_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2o4s_rmsd.sd | tee 05_get_rmsd.score
