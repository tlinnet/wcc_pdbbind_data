load 02_4cst.mol2
load 4cst_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cst
show sticks, 4cst_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
