load 02_3isj.mol2
load 3isj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3isj
show sticks, 3isj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
