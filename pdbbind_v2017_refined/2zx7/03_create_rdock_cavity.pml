load 02_2zx7.mol2
load 2zx7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zx7
show sticks, 2zx7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
