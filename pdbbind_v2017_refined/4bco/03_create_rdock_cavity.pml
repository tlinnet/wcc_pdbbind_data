load 02_4bco.mol2
load 4bco_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4bco
show sticks, 4bco_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
