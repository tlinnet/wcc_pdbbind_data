load 02_2qrk.mol2
load 2qrk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qrk
show sticks, 2qrk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
