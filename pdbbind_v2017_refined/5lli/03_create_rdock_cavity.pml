load 02_5lli.mol2
load 5lli_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5lli
show sticks, 5lli_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
