load 02_2nta.mol2
load 2nta_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2nta
show sticks, 2nta_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
