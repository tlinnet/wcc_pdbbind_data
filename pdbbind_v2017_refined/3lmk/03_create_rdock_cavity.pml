load 02_3lmk.mol2
load 3lmk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3lmk
show sticks, 3lmk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
