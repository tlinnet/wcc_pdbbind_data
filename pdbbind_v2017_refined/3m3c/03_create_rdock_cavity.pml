load 02_3m3c.mol2
load 3m3c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3m3c
show sticks, 3m3c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
