#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2w8j_ligand.sdf 04_out_2w8j.sd -o 04_out_2w8j_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2w8j_rmsd.sd | tee 05_get_rmsd.score
