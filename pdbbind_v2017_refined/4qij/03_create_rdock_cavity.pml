load 02_4qij.mol2
load 4qij_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qij
show sticks, 4qij_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
