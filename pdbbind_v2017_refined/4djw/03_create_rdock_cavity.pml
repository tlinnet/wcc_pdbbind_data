load 02_4djw.mol2
load 4djw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4djw
show sticks, 4djw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
