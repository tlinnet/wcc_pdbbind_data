load 02_3ebo.mol2
load 3ebo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ebo
show sticks, 3ebo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
