load 02_4ish.mol2
load 4ish_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ish
show sticks, 4ish_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
