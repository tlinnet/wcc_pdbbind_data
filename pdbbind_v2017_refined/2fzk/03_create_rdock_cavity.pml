load 02_2fzk.mol2
load 2fzk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2fzk
show sticks, 2fzk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
