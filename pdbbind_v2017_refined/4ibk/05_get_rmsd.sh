#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4ibk_ligand.sdf 04_out_4ibk.sd -o 04_out_4ibk_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4ibk_rmsd.sd | tee 05_get_rmsd.score
