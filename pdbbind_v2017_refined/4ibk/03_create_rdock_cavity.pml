load 02_4ibk.mol2
load 4ibk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ibk
show sticks, 4ibk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
