load 02_1usk.mol2
load 1usk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1usk
show sticks, 1usk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
