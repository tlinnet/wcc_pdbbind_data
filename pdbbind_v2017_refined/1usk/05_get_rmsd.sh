#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1usk_ligand.sdf 04_out_1usk.sd -o 04_out_1usk_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1usk_rmsd.sd | tee 05_get_rmsd.score
