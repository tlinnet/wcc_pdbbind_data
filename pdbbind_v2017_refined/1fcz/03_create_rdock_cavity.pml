load 02_1fcz.mol2
load 1fcz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fcz
show sticks, 1fcz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
