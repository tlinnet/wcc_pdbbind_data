load 02_2a5s.mol2
load 2a5s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2a5s
show sticks, 2a5s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
