load 02_2j4i.mol2
load 2j4i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2j4i
show sticks, 2j4i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
