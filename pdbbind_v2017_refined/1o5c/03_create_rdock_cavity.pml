load 02_1o5c.mol2
load 1o5c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1o5c
show sticks, 1o5c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
