load 02_5sxm.mol2
load 5sxm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5sxm
show sticks, 5sxm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
