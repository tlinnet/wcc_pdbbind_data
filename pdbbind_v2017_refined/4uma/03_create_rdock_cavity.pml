load 02_4uma.mol2
load 4uma_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4uma
show sticks, 4uma_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
