load 02_4z1e.mol2
load 4z1e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4z1e
show sticks, 4z1e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
