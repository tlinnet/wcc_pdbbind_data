load 02_4ibf.mol2
load 4ibf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ibf
show sticks, 4ibf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
