load 02_2cej.mol2
load 2cej_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2cej
show sticks, 2cej_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
