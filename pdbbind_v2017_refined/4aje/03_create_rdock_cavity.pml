load 02_4aje.mol2
load 4aje_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4aje
show sticks, 4aje_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
