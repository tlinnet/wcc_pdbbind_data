#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3wtl_ligand.sdf 04_out_3wtl.sd -o 04_out_3wtl_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3wtl_rmsd.sd | tee 05_get_rmsd.score
