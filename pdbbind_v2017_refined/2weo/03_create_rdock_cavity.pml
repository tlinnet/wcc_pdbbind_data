load 02_2weo.mol2
load 2weo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2weo
show sticks, 2weo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
