load 02_4r3w.mol2
load 4r3w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4r3w
show sticks, 4r3w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
