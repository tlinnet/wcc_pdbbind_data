load 02_2yel.mol2
load 2yel_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2yel
show sticks, 2yel_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
