load 02_1hyo.mol2
load 1hyo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hyo
show sticks, 1hyo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
