load 02_5afv.mol2
load 5afv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5afv
show sticks, 5afv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
