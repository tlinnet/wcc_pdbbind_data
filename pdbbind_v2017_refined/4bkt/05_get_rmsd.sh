#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4bkt_ligand.sdf 04_out_4bkt.sd -o 04_out_4bkt_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4bkt_rmsd.sd | tee 05_get_rmsd.score
