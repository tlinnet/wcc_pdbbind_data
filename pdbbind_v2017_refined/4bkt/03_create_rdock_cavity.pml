load 02_4bkt.mol2
load 4bkt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4bkt
show sticks, 4bkt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
