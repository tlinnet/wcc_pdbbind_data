#!/usr/bin/env bash

# Get best rmsd
sdrmsd 5fto_ligand.sdf 04_out_5fto.sd -o 04_out_5fto_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_5fto_rmsd.sd | tee 05_get_rmsd.score
