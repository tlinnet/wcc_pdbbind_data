load 02_5fto.mol2
load 5fto_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fto
show sticks, 5fto_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
