load 02_4j44.mol2
load 4j44_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4j44
show sticks, 4j44_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
