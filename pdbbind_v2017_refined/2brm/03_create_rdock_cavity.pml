load 02_2brm.mol2
load 2brm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2brm
show sticks, 2brm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
