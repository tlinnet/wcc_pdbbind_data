load 02_5f1x.mol2
load 5f1x_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5f1x
show sticks, 5f1x_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
