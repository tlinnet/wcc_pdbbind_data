load 02_3dsz.mol2
load 3dsz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3dsz
show sticks, 3dsz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
