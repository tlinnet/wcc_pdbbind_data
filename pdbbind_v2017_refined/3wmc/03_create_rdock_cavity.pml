load 02_3wmc.mol2
load 3wmc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3wmc
show sticks, 3wmc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
