load 02_1trd.mol2
load 1trd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1trd
show sticks, 1trd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
