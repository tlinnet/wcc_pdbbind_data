load 02_2j27.mol2
load 2j27_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2j27
show sticks, 2j27_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
