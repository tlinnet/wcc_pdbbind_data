load 02_4bj8.mol2
load 4bj8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4bj8
show sticks, 4bj8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
