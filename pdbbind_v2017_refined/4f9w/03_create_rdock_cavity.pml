load 02_4f9w.mol2
load 4f9w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4f9w
show sticks, 4f9w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
