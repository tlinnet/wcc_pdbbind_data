load 02_3exh.mol2
load 3exh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3exh
show sticks, 3exh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
