load 02_2c92.mol2
load 2c92_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2c92
show sticks, 2c92_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
