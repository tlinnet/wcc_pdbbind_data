load 02_4ahu.mol2
load 4ahu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ahu
show sticks, 4ahu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
