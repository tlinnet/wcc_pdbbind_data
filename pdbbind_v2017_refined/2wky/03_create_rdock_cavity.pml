load 02_2wky.mol2
load 2wky_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wky
show sticks, 2wky_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
