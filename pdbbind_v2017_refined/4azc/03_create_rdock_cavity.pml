load 02_4azc.mol2
load 4azc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4azc
show sticks, 4azc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
