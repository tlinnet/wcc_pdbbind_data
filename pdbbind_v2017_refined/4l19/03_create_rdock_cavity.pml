load 02_4l19.mol2
load 4l19_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4l19
show sticks, 4l19_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
