load 02_3e5a.mol2
load 3e5a_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3e5a
show sticks, 3e5a_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
