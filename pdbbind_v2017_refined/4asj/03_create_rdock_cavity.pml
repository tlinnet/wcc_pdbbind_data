load 02_4asj.mol2
load 4asj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4asj
show sticks, 4asj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
