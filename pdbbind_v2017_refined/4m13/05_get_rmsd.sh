#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4m13_ligand.sdf 04_out_4m13.sd -o 04_out_4m13_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4m13_rmsd.sd | tee 05_get_rmsd.score
