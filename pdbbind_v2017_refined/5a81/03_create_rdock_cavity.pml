load 02_5a81.mol2
load 5a81_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5a81
show sticks, 5a81_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
