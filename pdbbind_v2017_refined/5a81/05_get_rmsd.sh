#!/usr/bin/env bash

# Get best rmsd
sdrmsd 5a81_ligand.sdf 04_out_5a81.sd -o 04_out_5a81_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_5a81_rmsd.sd | tee 05_get_rmsd.score
