load 02_5cqu.mol2
load 5cqu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5cqu
show sticks, 5cqu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
