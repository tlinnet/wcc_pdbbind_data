load 02_3hk1.mol2
load 3hk1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hk1
show sticks, 3hk1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
