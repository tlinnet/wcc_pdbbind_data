load 02_1b3f.mol2
load 1b3f_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1b3f
show sticks, 1b3f_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
