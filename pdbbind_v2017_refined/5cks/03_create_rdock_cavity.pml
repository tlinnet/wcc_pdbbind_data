load 02_5cks.mol2
load 5cks_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5cks
show sticks, 5cks_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
