load 02_1enu.mol2
load 1enu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1enu
show sticks, 1enu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
