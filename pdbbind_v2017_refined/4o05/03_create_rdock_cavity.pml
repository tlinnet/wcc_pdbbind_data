load 02_4o05.mol2
load 4o05_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4o05
show sticks, 4o05_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
