load 02_4c1u.mol2
load 4c1u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4c1u
show sticks, 4c1u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
