load 02_1hmr.mol2
load 1hmr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hmr
show sticks, 1hmr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
