load 02_1fzk.mol2
load 1fzk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fzk
show sticks, 1fzk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
