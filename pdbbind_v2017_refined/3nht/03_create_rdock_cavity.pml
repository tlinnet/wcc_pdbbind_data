load 02_3nht.mol2
load 3nht_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nht
show sticks, 3nht_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
