load 02_4azi.mol2
load 4azi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4azi
show sticks, 4azi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
