load 02_2weg.mol2
load 2weg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2weg
show sticks, 2weg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
