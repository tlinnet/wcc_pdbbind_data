#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4rpn_ligand.sdf 04_out_4rpn.sd -o 04_out_4rpn_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4rpn_rmsd.sd | tee 05_get_rmsd.score
