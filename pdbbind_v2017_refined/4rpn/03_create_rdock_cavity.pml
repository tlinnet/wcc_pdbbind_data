load 02_4rpn.mol2
load 4rpn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rpn
show sticks, 4rpn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
