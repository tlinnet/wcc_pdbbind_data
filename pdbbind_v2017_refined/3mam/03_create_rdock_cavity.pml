load 02_3mam.mol2
load 3mam_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3mam
show sticks, 3mam_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
