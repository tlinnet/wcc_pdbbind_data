load 02_3w07.mol2
load 3w07_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3w07
show sticks, 3w07_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
