load 02_4rak.mol2
load 4rak_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rak
show sticks, 4rak_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
