load 02_4o0b.mol2
load 4o0b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4o0b
show sticks, 4o0b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
