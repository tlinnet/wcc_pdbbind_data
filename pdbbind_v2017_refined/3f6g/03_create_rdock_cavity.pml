load 02_3f6g.mol2
load 3f6g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3f6g
show sticks, 3f6g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
