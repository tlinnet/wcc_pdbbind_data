load 02_1hpx.mol2
load 1hpx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hpx
show sticks, 1hpx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
