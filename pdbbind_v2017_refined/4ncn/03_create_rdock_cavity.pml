load 02_4ncn.mol2
load 4ncn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ncn
show sticks, 4ncn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
