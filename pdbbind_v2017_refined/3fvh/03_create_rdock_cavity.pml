load 02_3fvh.mol2
load 3fvh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3fvh
show sticks, 3fvh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
