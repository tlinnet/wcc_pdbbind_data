load 02_1jys.mol2
load 1jys_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1jys
show sticks, 1jys_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
