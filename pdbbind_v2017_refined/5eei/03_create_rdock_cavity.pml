load 02_5eei.mol2
load 5eei_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5eei
show sticks, 5eei_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
