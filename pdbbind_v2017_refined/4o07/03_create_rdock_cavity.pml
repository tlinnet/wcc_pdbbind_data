load 02_4o07.mol2
load 4o07_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4o07
show sticks, 4o07_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
