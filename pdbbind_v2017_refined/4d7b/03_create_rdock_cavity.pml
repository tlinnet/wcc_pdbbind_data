load 02_4d7b.mol2
load 4d7b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4d7b
show sticks, 4d7b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
