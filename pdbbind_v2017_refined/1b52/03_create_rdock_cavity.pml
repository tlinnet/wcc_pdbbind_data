load 02_1b52.mol2
load 1b52_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1b52
show sticks, 1b52_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
