load 02_1ppc.mol2
load 1ppc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ppc
show sticks, 1ppc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
