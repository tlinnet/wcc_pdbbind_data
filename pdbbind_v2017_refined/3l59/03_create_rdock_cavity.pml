load 02_3l59.mol2
load 3l59_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3l59
show sticks, 3l59_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
