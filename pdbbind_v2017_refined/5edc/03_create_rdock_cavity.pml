load 02_5edc.mol2
load 5edc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5edc
show sticks, 5edc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
