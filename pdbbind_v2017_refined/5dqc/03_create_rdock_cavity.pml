load 02_5dqc.mol2
load 5dqc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5dqc
show sticks, 5dqc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
