load 02_3t08.mol2
load 3t08_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3t08
show sticks, 3t08_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
