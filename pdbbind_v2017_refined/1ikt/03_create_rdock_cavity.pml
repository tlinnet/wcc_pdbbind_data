load 02_1ikt.mol2
load 1ikt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ikt
show sticks, 1ikt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
