load 02_2ra6.mol2
load 2ra6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ra6
show sticks, 2ra6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
