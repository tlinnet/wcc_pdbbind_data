load 02_3bug.mol2
load 3bug_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3bug
show sticks, 3bug_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
