load 02_5fnf.mol2
load 5fnf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fnf
show sticks, 5fnf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
