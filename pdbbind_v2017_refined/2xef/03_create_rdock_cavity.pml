load 02_2xef.mol2
load 2xef_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xef
show sticks, 2xef_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
