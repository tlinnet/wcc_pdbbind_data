load 02_2j7d.mol2
load 2j7d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2j7d
show sticks, 2j7d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
