load 02_4i7p.mol2
load 4i7p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4i7p
show sticks, 4i7p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
