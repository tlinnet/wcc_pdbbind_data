load 02_1m48.mol2
load 1m48_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1m48
show sticks, 1m48_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
