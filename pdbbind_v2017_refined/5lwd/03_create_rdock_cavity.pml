load 02_5lwd.mol2
load 5lwd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5lwd
show sticks, 5lwd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
