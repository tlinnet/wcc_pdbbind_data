load 02_2vvn.mol2
load 2vvn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vvn
show sticks, 2vvn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
