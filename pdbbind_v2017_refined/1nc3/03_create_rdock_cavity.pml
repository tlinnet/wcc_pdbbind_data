load 02_1nc3.mol2
load 1nc3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1nc3
show sticks, 1nc3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
