load 02_5fck.mol2
load 5fck_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fck
show sticks, 5fck_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
