load 02_2vh6.mol2
load 2vh6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vh6
show sticks, 2vh6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
