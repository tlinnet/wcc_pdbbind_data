load 02_4hge.mol2
load 4hge_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4hge
show sticks, 4hge_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
