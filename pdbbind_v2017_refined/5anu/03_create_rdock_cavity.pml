load 02_5anu.mol2
load 5anu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5anu
show sticks, 5anu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
