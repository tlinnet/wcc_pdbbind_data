load 02_3ocp.mol2
load 3ocp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ocp
show sticks, 3ocp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
