load 02_4jwk.mol2
load 4jwk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4jwk
show sticks, 4jwk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
