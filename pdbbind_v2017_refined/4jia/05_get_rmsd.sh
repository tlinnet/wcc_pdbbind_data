#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4jia_ligand.sdf 04_out_4jia.sd -o 04_out_4jia_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4jia_rmsd.sd | tee 05_get_rmsd.score
