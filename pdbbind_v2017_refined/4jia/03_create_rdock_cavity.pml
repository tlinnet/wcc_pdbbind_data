load 02_4jia.mol2
load 4jia_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4jia
show sticks, 4jia_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
