load 02_1txr.mol2
load 1txr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1txr
show sticks, 1txr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
