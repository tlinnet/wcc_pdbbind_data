load 02_2ceq.mol2
load 2ceq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ceq
show sticks, 2ceq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
