load 02_4ujb.mol2
load 4ujb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ujb
show sticks, 4ujb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
