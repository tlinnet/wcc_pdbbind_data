load 02_2nsl.mol2
load 2nsl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2nsl
show sticks, 2nsl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
