load 02_3v3q.mol2
load 3v3q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3v3q
show sticks, 3v3q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
