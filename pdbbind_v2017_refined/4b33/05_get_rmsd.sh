#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4b33_ligand.sdf 04_out_4b33.sd -o 04_out_4b33_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4b33_rmsd.sd | tee 05_get_rmsd.score
