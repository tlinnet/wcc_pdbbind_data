load 02_3nq3.mol2
load 3nq3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nq3
show sticks, 3nq3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
