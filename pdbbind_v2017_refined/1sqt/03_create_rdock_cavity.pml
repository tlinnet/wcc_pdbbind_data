load 02_1sqt.mol2
load 1sqt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1sqt
show sticks, 1sqt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
