load 02_3q6z.mol2
load 3q6z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3q6z
show sticks, 3q6z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
