load 02_4dkp.mol2
load 4dkp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4dkp
show sticks, 4dkp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
