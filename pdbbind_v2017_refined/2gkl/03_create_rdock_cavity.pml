load 02_2gkl.mol2
load 2gkl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2gkl
show sticks, 2gkl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
