load 02_2zft.mol2
load 2zft_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zft
show sticks, 2zft_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
