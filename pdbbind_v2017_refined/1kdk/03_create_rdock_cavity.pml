load 02_1kdk.mol2
load 1kdk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1kdk
show sticks, 1kdk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
