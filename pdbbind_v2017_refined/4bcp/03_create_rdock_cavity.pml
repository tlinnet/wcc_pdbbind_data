load 02_4bcp.mol2
load 4bcp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4bcp
show sticks, 4bcp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
