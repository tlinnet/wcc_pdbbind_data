load 02_5lso.mol2
load 5lso_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5lso
show sticks, 5lso_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
