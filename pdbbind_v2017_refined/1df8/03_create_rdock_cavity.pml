load 02_1df8.mol2
load 1df8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1df8
show sticks, 1df8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
