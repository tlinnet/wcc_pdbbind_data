#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1swg_ligand.sdf 04_out_1swg.sd -o 04_out_1swg_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1swg_rmsd.sd | tee 05_get_rmsd.score
