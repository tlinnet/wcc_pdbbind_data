load 02_1vso.mol2
load 1vso_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1vso
show sticks, 1vso_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
