load 02_4nxv.mol2
load 4nxv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nxv
show sticks, 4nxv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
