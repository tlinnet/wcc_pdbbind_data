load 02_4cwp.mol2
load 4cwp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cwp
show sticks, 4cwp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
