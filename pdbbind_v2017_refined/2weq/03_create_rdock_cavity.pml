load 02_2weq.mol2
load 2weq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2weq
show sticks, 2weq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
