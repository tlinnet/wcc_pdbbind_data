load 02_4cg8.mol2
load 4cg8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cg8
show sticks, 4cg8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
