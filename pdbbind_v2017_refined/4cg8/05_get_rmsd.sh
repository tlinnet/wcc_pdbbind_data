#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4cg8_ligand.sdf 04_out_4cg8.sd -o 04_out_4cg8_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4cg8_rmsd.sd | tee 05_get_rmsd.score
