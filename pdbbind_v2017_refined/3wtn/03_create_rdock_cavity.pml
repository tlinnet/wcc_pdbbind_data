load 02_3wtn.mol2
load 3wtn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3wtn
show sticks, 3wtn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
