load 02_3pck.mol2
load 3pck_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3pck
show sticks, 3pck_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
