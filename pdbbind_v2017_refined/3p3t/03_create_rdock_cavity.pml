load 02_3p3t.mol2
load 3p3t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3p3t
show sticks, 3p3t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
