load 02_2cc7.mol2
load 2cc7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2cc7
show sticks, 2cc7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
