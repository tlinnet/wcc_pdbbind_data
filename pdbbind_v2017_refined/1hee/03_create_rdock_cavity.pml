load 02_1hee.mol2
load 1hee_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hee
show sticks, 1hee_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
