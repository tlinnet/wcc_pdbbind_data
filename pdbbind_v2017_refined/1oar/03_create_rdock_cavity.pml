load 02_1oar.mol2
load 1oar_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1oar
show sticks, 1oar_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
