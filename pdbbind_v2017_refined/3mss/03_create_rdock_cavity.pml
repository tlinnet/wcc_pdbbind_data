load 02_3mss.mol2
load 3mss_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3mss
show sticks, 3mss_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
