load 02_4qfo.mol2
load 4qfo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qfo
show sticks, 4qfo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
