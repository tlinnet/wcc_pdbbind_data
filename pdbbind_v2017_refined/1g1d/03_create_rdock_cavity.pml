load 02_1g1d.mol2
load 1g1d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1g1d
show sticks, 1g1d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
