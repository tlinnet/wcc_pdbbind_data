load 02_3aqt.mol2
load 3aqt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3aqt
show sticks, 3aqt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
