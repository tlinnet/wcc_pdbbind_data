load 02_4r74.mol2
load 4r74_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4r74
show sticks, 4r74_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
