#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4m6u_ligand.sdf 04_out_4m6u.sd -o 04_out_4m6u_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4m6u_rmsd.sd | tee 05_get_rmsd.score
