load 02_2ha2.mol2
load 2ha2_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ha2
show sticks, 2ha2_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
