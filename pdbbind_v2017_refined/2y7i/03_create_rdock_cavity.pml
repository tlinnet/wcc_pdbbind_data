load 02_2y7i.mol2
load 2y7i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2y7i
show sticks, 2y7i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
