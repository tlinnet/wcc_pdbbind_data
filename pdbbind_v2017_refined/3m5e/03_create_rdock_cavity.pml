load 02_3m5e.mol2
load 3m5e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3m5e
show sticks, 3m5e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
