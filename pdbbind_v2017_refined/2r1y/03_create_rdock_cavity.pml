load 02_2r1y.mol2
load 2r1y_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2r1y
show sticks, 2r1y_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
