load 02_4dkr.mol2
load 4dkr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4dkr
show sticks, 4dkr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
