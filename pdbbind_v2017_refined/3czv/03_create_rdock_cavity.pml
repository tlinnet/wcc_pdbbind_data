load 02_3czv.mol2
load 3czv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3czv
show sticks, 3czv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
