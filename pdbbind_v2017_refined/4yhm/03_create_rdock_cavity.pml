load 02_4yhm.mol2
load 4yhm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4yhm
show sticks, 4yhm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
