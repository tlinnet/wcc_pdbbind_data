load 02_4qfn.mol2
load 4qfn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qfn
show sticks, 4qfn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
