load 02_1ogx.mol2
load 1ogx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ogx
show sticks, 1ogx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
