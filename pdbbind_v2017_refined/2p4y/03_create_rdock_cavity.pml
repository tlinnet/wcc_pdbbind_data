load 02_2p4y.mol2
load 2p4y_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2p4y
show sticks, 2p4y_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
