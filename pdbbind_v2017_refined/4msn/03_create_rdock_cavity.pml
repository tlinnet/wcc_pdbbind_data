load 02_4msn.mol2
load 4msn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4msn
show sticks, 4msn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
