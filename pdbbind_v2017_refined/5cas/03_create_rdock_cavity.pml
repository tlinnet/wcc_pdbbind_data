load 02_5cas.mol2
load 5cas_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5cas
show sticks, 5cas_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
