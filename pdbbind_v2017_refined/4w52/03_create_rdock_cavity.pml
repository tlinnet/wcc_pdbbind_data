load 02_4w52.mol2
load 4w52_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4w52
show sticks, 4w52_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
