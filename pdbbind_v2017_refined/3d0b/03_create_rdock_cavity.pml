load 02_3d0b.mol2
load 3d0b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3d0b
show sticks, 3d0b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
