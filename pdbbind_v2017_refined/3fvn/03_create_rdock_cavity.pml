load 02_3fvn.mol2
load 3fvn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3fvn
show sticks, 3fvn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
