load 02_3mzc.mol2
load 3mzc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3mzc
show sticks, 3mzc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
