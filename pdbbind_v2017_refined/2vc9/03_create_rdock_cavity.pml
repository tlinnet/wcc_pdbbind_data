load 02_2vc9.mol2
load 2vc9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vc9
show sticks, 2vc9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
