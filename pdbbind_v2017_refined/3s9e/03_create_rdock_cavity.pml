load 02_3s9e.mol2
load 3s9e_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3s9e
show sticks, 3s9e_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
