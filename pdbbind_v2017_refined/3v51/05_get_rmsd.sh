#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3v51_ligand.sdf 04_out_3v51.sd -o 04_out_3v51_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3v51_rmsd.sd | tee 05_get_rmsd.score
