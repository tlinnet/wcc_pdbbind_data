load 02_3v51.mol2
load 3v51_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3v51
show sticks, 3v51_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
