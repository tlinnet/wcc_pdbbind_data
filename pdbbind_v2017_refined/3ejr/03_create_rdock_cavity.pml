load 02_3ejr.mol2
load 3ejr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ejr
show sticks, 3ejr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
