load 02_3k5v.mol2
load 3k5v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3k5v
show sticks, 3k5v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
