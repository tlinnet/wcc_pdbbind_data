load 02_2vba.mol2
load 2vba_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vba
show sticks, 2vba_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
