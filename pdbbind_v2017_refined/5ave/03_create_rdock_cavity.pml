load 02_5ave.mol2
load 5ave_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ave
show sticks, 5ave_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
