load 02_3lir.mol2
load 3lir_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3lir
show sticks, 3lir_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
