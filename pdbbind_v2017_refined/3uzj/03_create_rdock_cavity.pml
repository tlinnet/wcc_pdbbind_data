load 02_3uzj.mol2
load 3uzj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3uzj
show sticks, 3uzj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
