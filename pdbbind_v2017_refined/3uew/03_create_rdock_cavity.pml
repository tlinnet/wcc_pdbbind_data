load 02_3uew.mol2
load 3uew_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3uew
show sticks, 3uew_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
