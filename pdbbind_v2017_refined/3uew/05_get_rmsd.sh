#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3uew_ligand.sdf 04_out_3uew.sd -o 04_out_3uew_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3uew_rmsd.sd | tee 05_get_rmsd.score
