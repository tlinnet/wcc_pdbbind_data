load 02_1egh.mol2
load 1egh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1egh
show sticks, 1egh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
