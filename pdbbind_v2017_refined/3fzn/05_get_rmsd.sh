#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3fzn_ligand.sdf 04_out_3fzn.sd -o 04_out_3fzn_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3fzn_rmsd.sd | tee 05_get_rmsd.score
