load 02_3fzn.mol2
load 3fzn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3fzn
show sticks, 3fzn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
