load 02_4q4p.mol2
load 4q4p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4q4p
show sticks, 4q4p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
