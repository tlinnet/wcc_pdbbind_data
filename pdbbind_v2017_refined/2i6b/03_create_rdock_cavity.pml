load 02_2i6b.mol2
load 2i6b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i6b
show sticks, 2i6b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
