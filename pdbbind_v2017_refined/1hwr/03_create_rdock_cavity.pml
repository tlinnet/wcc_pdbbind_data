load 02_1hwr.mol2
load 1hwr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hwr
show sticks, 1hwr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
