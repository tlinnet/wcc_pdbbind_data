load 02_2q1q.mol2
load 2q1q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2q1q
show sticks, 2q1q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
