load 02_3nyd.mol2
load 3nyd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nyd
show sticks, 3nyd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
