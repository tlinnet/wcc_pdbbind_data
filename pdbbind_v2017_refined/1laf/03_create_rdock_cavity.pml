load 02_1laf.mol2
load 1laf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1laf
show sticks, 1laf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
