load 02_1bai.mol2
load 1bai_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bai
show sticks, 1bai_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
