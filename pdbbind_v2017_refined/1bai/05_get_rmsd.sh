#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1bai_ligand.sdf 04_out_1bai.sd -o 04_out_1bai_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1bai_rmsd.sd | tee 05_get_rmsd.score
