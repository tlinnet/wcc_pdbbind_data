load 02_4gkm.mol2
load 4gkm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4gkm
show sticks, 4gkm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
