load 02_3suu.mol2
load 3suu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3suu
show sticks, 3suu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
