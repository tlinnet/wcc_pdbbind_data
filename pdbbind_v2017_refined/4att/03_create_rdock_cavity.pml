load 02_4att.mol2
load 4att_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4att
show sticks, 4att_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
