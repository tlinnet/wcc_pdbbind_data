load 02_1qyg.mol2
load 1qyg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1qyg
show sticks, 1qyg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
