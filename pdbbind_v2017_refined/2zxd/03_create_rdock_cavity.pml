load 02_2zxd.mol2
load 2zxd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zxd
show sticks, 2zxd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
