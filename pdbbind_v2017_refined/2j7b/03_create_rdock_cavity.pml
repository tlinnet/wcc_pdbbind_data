load 02_2j7b.mol2
load 2j7b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2j7b
show sticks, 2j7b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
