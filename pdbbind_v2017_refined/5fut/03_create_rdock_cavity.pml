load 02_5fut.mol2
load 5fut_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fut
show sticks, 5fut_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
