load 02_4yho.mol2
load 4yho_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4yho
show sticks, 4yho_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
