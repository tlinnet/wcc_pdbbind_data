load 02_3tzm.mol2
load 3tzm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3tzm
show sticks, 3tzm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
