load 02_1fkg.mol2
load 1fkg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1fkg
show sticks, 1fkg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
