load 02_3ejp.mol2
load 3ejp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ejp
show sticks, 3ejp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
