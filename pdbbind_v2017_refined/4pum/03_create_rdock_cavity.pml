load 02_4pum.mol2
load 4pum_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pum
show sticks, 4pum_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
