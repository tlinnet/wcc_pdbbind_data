load 02_4g5f.mol2
load 4g5f_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4g5f
show sticks, 4g5f_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
