load 02_4qlk.mol2
load 4qlk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qlk
show sticks, 4qlk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
