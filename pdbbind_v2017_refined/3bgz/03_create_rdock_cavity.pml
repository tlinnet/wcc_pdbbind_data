load 02_3bgz.mol2
load 3bgz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3bgz
show sticks, 3bgz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
