load 02_1j36.mol2
load 1j36_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1j36
show sticks, 1j36_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
