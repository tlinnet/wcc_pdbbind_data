load 02_2gj5.mol2
load 2gj5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2gj5
show sticks, 2gj5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
