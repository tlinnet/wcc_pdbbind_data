#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2j34_ligand.sdf 04_out_2j34.sd -o 04_out_2j34_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2j34_rmsd.sd | tee 05_get_rmsd.score
