load 02_4ryd.mol2
load 4ryd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ryd
show sticks, 4ryd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
