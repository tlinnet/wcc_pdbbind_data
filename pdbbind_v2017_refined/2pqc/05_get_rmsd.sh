#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2pqc_ligand.sdf 04_out_2pqc.sd -o 04_out_2pqc_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2pqc_rmsd.sd | tee 05_get_rmsd.score
