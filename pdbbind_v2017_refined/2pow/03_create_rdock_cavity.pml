load 02_2pow.mol2
load 2pow_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pow
show sticks, 2pow_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
