load 02_5l4j.mol2
load 5l4j_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5l4j
show sticks, 5l4j_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
