load 02_2vnt.mol2
load 2vnt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vnt
show sticks, 2vnt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
