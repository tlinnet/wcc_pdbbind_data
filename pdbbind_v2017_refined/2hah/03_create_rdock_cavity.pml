load 02_2hah.mol2
load 2hah_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2hah
show sticks, 2hah_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
