load 02_2zda.mol2
load 2zda_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zda
show sticks, 2zda_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
