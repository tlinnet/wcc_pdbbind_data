load 02_5lif.mol2
load 5lif_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5lif
show sticks, 5lif_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
