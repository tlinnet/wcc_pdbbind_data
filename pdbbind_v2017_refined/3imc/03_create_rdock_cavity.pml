load 02_3imc.mol2
load 3imc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3imc
show sticks, 3imc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
