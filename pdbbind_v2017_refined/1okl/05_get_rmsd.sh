#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1okl_ligand.sdf 04_out_1okl.sd -o 04_out_1okl_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1okl_rmsd.sd | tee 05_get_rmsd.score
