load 02_3ui7.mol2
load 3ui7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ui7
show sticks, 3ui7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
