load 02_4zeb.mol2
load 4zeb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zeb
show sticks, 4zeb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
