load 02_3gqz.mol2
load 3gqz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gqz
show sticks, 3gqz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
