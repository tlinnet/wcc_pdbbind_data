load 02_2pyy.mol2
load 2pyy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2pyy
show sticks, 2pyy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
