load 02_5anv.mol2
load 5anv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5anv
show sticks, 5anv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
