load 02_2xht.mol2
load 2xht_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xht
show sticks, 2xht_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
