load 02_2d1n.mol2
load 2d1n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2d1n
show sticks, 2d1n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
