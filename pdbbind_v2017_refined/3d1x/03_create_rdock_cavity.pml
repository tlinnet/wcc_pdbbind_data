load 02_3d1x.mol2
load 3d1x_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3d1x
show sticks, 3d1x_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
