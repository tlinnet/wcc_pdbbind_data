load 02_2xda.mol2
load 2xda_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xda
show sticks, 2xda_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
