load 02_1tpw.mol2
load 1tpw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1tpw
show sticks, 1tpw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
