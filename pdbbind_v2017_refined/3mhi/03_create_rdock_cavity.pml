load 02_3mhi.mol2
load 3mhi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3mhi
show sticks, 3mhi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
