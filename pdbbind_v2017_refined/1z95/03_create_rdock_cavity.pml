load 02_1z95.mol2
load 1z95_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1z95
show sticks, 1z95_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
