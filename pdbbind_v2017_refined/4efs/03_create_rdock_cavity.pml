load 02_4efs.mol2
load 4efs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4efs
show sticks, 4efs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
