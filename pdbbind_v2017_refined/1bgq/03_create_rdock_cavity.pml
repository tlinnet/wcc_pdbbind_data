load 02_1bgq.mol2
load 1bgq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bgq
show sticks, 1bgq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
