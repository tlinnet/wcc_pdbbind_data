load 02_3ip5.mol2
load 3ip5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ip5
show sticks, 3ip5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
