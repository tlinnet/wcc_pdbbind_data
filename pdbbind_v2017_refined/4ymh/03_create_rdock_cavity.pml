load 02_4ymh.mol2
load 4ymh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ymh
show sticks, 4ymh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
