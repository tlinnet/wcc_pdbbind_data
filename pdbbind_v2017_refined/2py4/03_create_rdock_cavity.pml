load 02_2py4.mol2
load 2py4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2py4
show sticks, 2py4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
