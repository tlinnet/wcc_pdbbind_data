load 02_2vfk.mol2
load 2vfk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vfk
show sticks, 2vfk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
