load 02_1eld.mol2
load 1eld_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1eld
show sticks, 1eld_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
