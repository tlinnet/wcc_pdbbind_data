load 02_4fxq.mol2
load 4fxq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4fxq
show sticks, 4fxq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
