load 02_3ibi.mol2
load 3ibi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ibi
show sticks, 3ibi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
