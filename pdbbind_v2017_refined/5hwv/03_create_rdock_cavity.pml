load 02_5hwv.mol2
load 5hwv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5hwv
show sticks, 5hwv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
