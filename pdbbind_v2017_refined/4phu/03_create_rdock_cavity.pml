load 02_4phu.mol2
load 4phu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4phu
show sticks, 4phu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
