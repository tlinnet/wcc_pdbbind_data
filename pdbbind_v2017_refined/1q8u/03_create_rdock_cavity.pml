load 02_1q8u.mol2
load 1q8u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1q8u
show sticks, 1q8u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
