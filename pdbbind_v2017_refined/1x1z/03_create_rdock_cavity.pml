load 02_1x1z.mol2
load 1x1z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1x1z
show sticks, 1x1z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
