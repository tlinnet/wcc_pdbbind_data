load 02_4i9h.mol2
load 4i9h_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4i9h
show sticks, 4i9h_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
