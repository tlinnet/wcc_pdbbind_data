load 02_3t8v.mol2
load 3t8v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3t8v
show sticks, 3t8v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
