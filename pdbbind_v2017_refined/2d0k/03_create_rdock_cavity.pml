load 02_2d0k.mol2
load 2d0k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2d0k
show sticks, 2d0k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
