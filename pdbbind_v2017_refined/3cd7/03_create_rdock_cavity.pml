load 02_3cd7.mol2
load 3cd7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3cd7
show sticks, 3cd7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
