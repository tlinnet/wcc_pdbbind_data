load 02_1rpj.mol2
load 1rpj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1rpj
show sticks, 1rpj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
