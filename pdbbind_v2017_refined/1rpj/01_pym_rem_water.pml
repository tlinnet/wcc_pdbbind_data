load 1rpj_protein.pdb

#Remove alternates
cmd.remove("not alt +A")
cmd.valence("guess", selection1='1rpj_protein')

#Prepare molecules with polar hydrogens:
cmd.h_add("elem O or elem N or elem S")

#Delete Waters and Heteroatoms
cmd.remove("solvent")
cmd.remove("resn HOH")
cmd.remove("hetatm")

save 01_1rpj.pdb
