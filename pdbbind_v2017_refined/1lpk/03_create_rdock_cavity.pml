load 02_1lpk.mol2
load 1lpk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lpk
show sticks, 1lpk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
