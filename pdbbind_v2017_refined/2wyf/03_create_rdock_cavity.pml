load 02_2wyf.mol2
load 2wyf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wyf
show sticks, 2wyf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
