load 02_1bxo.mol2
load 1bxo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bxo
show sticks, 1bxo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
