load 02_4zek.mol2
load 4zek_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zek
show sticks, 4zek_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
