load 02_4ban.mol2
load 4ban_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ban
show sticks, 4ban_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
