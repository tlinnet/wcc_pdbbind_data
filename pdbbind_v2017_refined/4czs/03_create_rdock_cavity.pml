load 02_4czs.mol2
load 4czs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4czs
show sticks, 4czs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
