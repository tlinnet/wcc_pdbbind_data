load 02_2vpo.mol2
load 2vpo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vpo
show sticks, 2vpo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
