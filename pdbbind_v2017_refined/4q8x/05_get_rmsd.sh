#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4q8x_ligand.sdf 04_out_4q8x.sd -o 04_out_4q8x_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4q8x_rmsd.sd | tee 05_get_rmsd.score
