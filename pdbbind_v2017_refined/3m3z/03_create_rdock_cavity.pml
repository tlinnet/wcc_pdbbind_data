load 02_3m3z.mol2
load 3m3z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3m3z
show sticks, 3m3z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
