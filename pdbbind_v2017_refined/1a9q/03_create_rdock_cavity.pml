load 02_1a9q.mol2
load 1a9q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1a9q
show sticks, 1a9q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
