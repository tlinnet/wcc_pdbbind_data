load 02_3uev.mol2
load 3uev_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3uev
show sticks, 3uev_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
