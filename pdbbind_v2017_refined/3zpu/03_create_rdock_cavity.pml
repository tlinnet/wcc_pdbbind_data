load 02_3zpu.mol2
load 3zpu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3zpu
show sticks, 3zpu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
