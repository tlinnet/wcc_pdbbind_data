load 02_4dmw.mol2
load 4dmw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4dmw
show sticks, 4dmw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
