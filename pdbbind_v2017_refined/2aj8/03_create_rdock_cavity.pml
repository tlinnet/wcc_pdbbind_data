load 02_2aj8.mol2
load 2aj8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2aj8
show sticks, 2aj8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
