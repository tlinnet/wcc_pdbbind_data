load 02_2y5f.mol2
load 2y5f_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2y5f
show sticks, 2y5f_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
