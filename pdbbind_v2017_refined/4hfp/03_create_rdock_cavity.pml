load 02_4hfp.mol2
load 4hfp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4hfp
show sticks, 4hfp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
