load 02_4b9z.mol2
load 4b9z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4b9z
show sticks, 4b9z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
