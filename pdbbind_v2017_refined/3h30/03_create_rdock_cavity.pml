load 02_3h30.mol2
load 3h30_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3h30
show sticks, 3h30_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
