load 02_1mue.mol2
load 1mue_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1mue
show sticks, 1mue_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
