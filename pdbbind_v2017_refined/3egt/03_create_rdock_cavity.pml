load 02_3egt.mol2
load 3egt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3egt
show sticks, 3egt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
