load 02_1o1s.mol2
load 1o1s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1o1s
show sticks, 1o1s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
