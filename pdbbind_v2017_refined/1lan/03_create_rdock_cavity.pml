load 02_1lan.mol2
load 1lan_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lan
show sticks, 1lan_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
