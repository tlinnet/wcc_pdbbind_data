load 02_4lxd.mol2
load 4lxd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4lxd
show sticks, 4lxd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
