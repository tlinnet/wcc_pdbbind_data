load 02_2uwd.mol2
load 2uwd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2uwd
show sticks, 2uwd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
