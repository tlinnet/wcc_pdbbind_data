load 02_1p1q.mol2
load 1p1q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1p1q
show sticks, 1p1q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
