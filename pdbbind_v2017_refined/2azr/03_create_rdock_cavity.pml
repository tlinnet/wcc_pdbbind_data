load 02_2azr.mol2
load 2azr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2azr
show sticks, 2azr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
