load 02_3pju.mol2
load 3pju_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3pju
show sticks, 3pju_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
