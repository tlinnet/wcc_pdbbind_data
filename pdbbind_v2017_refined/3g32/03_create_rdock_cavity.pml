load 02_3g32.mol2
load 3g32_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3g32
show sticks, 3g32_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
