load 02_2f8g.mol2
load 2f8g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2f8g
show sticks, 2f8g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
