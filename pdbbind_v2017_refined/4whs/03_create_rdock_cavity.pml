load 02_4whs.mol2
load 4whs_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4whs
show sticks, 4whs_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
