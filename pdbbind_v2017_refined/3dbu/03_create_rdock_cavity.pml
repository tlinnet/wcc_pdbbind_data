load 02_3dbu.mol2
load 3dbu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3dbu
show sticks, 3dbu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
