load 02_4r5t.mol2
load 4r5t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4r5t
show sticks, 4r5t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
