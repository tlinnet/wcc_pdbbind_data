load 02_4aba.mol2
load 4aba_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4aba
show sticks, 4aba_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
