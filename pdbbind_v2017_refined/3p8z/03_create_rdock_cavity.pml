load 02_3p8z.mol2
load 3p8z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3p8z
show sticks, 3p8z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
