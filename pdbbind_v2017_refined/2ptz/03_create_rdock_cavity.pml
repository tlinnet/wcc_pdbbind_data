load 02_2ptz.mol2
load 2ptz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ptz
show sticks, 2ptz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
