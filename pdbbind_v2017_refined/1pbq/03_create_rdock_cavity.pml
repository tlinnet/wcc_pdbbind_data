load 02_1pbq.mol2
load 1pbq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pbq
show sticks, 1pbq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
