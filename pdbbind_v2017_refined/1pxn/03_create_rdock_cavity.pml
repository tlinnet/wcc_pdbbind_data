load 02_1pxn.mol2
load 1pxn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pxn
show sticks, 1pxn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
