load 02_4ufk.mol2
load 4ufk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ufk
show sticks, 4ufk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
