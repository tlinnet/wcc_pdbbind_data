load 02_4k77.mol2
load 4k77_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4k77
show sticks, 4k77_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
