load 02_3nee.mol2
load 3nee_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nee
show sticks, 3nee_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
