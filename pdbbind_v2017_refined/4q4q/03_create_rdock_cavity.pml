load 02_4q4q.mol2
load 4q4q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4q4q
show sticks, 4q4q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
