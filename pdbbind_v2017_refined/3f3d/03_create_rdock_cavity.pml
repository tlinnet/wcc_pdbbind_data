load 02_3f3d.mol2
load 3f3d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3f3d
show sticks, 3f3d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
