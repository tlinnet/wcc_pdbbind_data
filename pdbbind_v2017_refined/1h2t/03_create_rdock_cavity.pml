load 02_1h2t.mol2
load 1h2t_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1h2t
show sticks, 1h2t_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
