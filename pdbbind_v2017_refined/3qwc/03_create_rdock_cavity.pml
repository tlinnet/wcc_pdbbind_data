load 02_3qwc.mol2
load 3qwc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qwc
show sticks, 3qwc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
