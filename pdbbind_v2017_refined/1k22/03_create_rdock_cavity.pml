load 02_1k22.mol2
load 1k22_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1k22
show sticks, 1k22_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
