load 02_2zcq.mol2
load 2zcq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zcq
show sticks, 2zcq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
