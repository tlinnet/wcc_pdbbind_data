load 02_5cep.mol2
load 5cep_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5cep
show sticks, 5cep_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
