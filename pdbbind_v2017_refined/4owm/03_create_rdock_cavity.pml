load 02_4owm.mol2
load 4owm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4owm
show sticks, 4owm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
