load 02_3ckz.mol2
load 3ckz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ckz
show sticks, 3ckz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
