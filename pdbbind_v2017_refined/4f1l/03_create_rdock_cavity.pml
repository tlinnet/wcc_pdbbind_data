load 02_4f1l.mol2
load 4f1l_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4f1l
show sticks, 4f1l_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
