load 02_3d8w.mol2
load 3d8w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3d8w
show sticks, 3d8w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
