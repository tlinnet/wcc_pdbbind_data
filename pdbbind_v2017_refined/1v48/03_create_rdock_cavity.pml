load 02_1v48.mol2
load 1v48_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1v48
show sticks, 1v48_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
