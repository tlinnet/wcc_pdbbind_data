load 02_5ia2.mol2
load 5ia2_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ia2
show sticks, 5ia2_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
