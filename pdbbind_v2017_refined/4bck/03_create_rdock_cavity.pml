load 02_4bck.mol2
load 4bck_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4bck
show sticks, 4bck_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
