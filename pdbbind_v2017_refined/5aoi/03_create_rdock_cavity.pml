load 02_5aoi.mol2
load 5aoi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5aoi
show sticks, 5aoi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
