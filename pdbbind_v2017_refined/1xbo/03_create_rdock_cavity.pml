load 02_1xbo.mol2
load 1xbo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1xbo
show sticks, 1xbo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
