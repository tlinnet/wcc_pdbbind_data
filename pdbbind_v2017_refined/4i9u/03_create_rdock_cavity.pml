load 02_4i9u.mol2
load 4i9u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4i9u
show sticks, 4i9u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
