load 02_2zxg.mol2
load 2zxg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zxg
show sticks, 2zxg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
