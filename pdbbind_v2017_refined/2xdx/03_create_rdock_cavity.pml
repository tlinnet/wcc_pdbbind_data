load 02_2xdx.mol2
load 2xdx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xdx
show sticks, 2xdx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
