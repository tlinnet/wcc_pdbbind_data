load 02_5itp.mol2
load 5itp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5itp
show sticks, 5itp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
