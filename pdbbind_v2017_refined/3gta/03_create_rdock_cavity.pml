load 02_3gta.mol2
load 3gta_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gta
show sticks, 3gta_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
