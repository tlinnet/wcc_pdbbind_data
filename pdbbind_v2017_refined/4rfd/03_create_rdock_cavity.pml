load 02_4rfd.mol2
load 4rfd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4rfd
show sticks, 4rfd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
