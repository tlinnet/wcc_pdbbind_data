load 02_3zyf.mol2
load 3zyf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3zyf
show sticks, 3zyf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
