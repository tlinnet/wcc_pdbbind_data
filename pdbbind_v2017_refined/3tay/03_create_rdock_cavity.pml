load 02_3tay.mol2
load 3tay_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3tay
show sticks, 3tay_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
