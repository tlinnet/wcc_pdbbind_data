load 02_6upj.mol2
load 6upj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_6upj
show sticks, 6upj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
