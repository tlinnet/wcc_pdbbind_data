load 02_4cjp.mol2
load 4cjp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cjp
show sticks, 4cjp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
