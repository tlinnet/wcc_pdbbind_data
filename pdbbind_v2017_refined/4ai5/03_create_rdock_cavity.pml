load 02_4ai5.mol2
load 4ai5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ai5
show sticks, 4ai5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
