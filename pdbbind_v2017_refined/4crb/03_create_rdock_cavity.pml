load 02_4crb.mol2
load 4crb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4crb
show sticks, 4crb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
