load 02_1olx.mol2
load 1olx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1olx
show sticks, 1olx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
