load 02_1ony.mol2
load 1ony_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ony
show sticks, 1ony_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
