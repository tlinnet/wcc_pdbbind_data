load 02_4m2u.mol2
load 4m2u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4m2u
show sticks, 4m2u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
