load 02_4afg.mol2
load 4afg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4afg
show sticks, 4afg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
