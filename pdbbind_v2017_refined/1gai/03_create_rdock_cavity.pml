load 02_1gai.mol2
load 1gai_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1gai
show sticks, 1gai_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
