load 02_4jfm.mol2
load 4jfm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4jfm
show sticks, 4jfm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
