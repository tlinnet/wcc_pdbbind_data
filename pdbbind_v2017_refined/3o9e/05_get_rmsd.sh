#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3o9e_ligand.sdf 04_out_3o9e.sd -o 04_out_3o9e_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3o9e_rmsd.sd | tee 05_get_rmsd.score
