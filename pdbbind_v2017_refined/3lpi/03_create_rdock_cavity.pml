load 02_3lpi.mol2
load 3lpi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3lpi
show sticks, 3lpi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
