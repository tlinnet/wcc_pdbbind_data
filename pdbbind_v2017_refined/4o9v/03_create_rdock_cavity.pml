load 02_4o9v.mol2
load 4o9v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4o9v
show sticks, 4o9v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
