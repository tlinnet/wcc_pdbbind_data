#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4r4t_ligand.sdf 04_out_4r4t.sd -o 04_out_4r4t_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4r4t_rmsd.sd | tee 05_get_rmsd.score
