load 02_3sjf.mol2
load 3sjf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3sjf
show sticks, 3sjf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
