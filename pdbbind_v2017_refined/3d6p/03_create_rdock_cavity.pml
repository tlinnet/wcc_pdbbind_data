load 02_3d6p.mol2
load 3d6p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3d6p
show sticks, 3d6p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
