load 02_4qpl.mol2
load 4qpl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4qpl
show sticks, 4qpl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
