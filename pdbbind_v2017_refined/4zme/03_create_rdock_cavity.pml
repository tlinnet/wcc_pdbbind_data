load 02_4zme.mol2
load 4zme_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4zme
show sticks, 4zme_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
