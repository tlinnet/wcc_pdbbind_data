load 02_4pow.mol2
load 4pow_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pow
show sticks, 4pow_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
