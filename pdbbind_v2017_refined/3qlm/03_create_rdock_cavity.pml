load 02_3qlm.mol2
load 3qlm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qlm
show sticks, 3qlm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
