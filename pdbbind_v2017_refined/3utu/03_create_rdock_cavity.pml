load 02_3utu.mol2
load 3utu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3utu
show sticks, 3utu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
