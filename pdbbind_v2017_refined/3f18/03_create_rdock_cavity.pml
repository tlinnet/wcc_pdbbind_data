load 02_3f18.mol2
load 3f18_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3f18
show sticks, 3f18_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
