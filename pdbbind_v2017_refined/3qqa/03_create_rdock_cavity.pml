load 02_3qqa.mol2
load 3qqa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qqa
show sticks, 3qqa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
