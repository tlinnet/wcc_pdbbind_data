load 02_2std.mol2
load 2std_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2std
show sticks, 2std_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
