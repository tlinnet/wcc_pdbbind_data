load 02_5bwc.mol2
load 5bwc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5bwc
show sticks, 5bwc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
