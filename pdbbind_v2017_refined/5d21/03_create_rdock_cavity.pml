load 02_5d21.mol2
load 5d21_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5d21
show sticks, 5d21_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
