load 02_1dar.mol2
load 1dar_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1dar
show sticks, 1dar_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
