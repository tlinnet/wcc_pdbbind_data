load 02_4wkp.mol2
load 4wkp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4wkp
show sticks, 4wkp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
