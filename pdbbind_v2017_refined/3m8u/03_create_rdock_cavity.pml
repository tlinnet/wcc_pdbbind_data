load 02_3m8u.mol2
load 3m8u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3m8u
show sticks, 3m8u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
