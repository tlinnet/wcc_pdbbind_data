load 02_3gst.mol2
load 3gst_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gst
show sticks, 3gst_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
