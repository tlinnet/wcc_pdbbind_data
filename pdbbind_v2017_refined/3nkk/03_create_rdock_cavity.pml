load 02_3nkk.mol2
load 3nkk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3nkk
show sticks, 3nkk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
