load 02_3e3c.mol2
load 3e3c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3e3c
show sticks, 3e3c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
