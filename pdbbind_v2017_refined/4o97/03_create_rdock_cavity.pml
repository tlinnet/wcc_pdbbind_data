load 02_4o97.mol2
load 4o97_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4o97
show sticks, 4o97_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
