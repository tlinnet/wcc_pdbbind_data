load 02_3ao5.mol2
load 3ao5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ao5
show sticks, 3ao5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
