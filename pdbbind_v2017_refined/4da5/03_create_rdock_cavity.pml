load 02_4da5.mol2
load 4da5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4da5
show sticks, 4da5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
