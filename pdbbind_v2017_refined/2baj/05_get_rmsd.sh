#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2baj_ligand.sdf 04_out_2baj.sd -o 04_out_2baj_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2baj_rmsd.sd | tee 05_get_rmsd.score
