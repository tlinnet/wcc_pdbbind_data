load 02_4nkt.mol2
load 4nkt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nkt
show sticks, 4nkt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
