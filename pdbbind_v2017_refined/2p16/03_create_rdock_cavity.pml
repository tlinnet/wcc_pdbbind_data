load 02_2p16.mol2
load 2p16_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2p16
show sticks, 2p16_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
