#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2p16_ligand.sdf 04_out_2p16.sd -o 04_out_2p16_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2p16_rmsd.sd | tee 05_get_rmsd.score
