load 02_5egu.mol2
load 5egu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5egu
show sticks, 5egu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
