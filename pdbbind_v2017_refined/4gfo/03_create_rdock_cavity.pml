load 02_4gfo.mol2
load 4gfo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4gfo
show sticks, 4gfo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
