load 02_1msn.mol2
load 1msn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1msn
show sticks, 1msn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
