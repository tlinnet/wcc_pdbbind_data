load 02_2uyq.mol2
load 2uyq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2uyq
show sticks, 2uyq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
