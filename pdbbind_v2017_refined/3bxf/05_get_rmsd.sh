#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3bxf_ligand.sdf 04_out_3bxf.sd -o 04_out_3bxf_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3bxf_rmsd.sd | tee 05_get_rmsd.score
