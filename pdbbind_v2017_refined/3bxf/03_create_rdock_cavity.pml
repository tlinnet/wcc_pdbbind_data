load 02_3bxf.mol2
load 3bxf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3bxf
show sticks, 3bxf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
