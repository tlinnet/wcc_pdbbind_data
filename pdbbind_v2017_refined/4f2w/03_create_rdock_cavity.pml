load 02_4f2w.mol2
load 4f2w_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4f2w
show sticks, 4f2w_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
