load 02_2xbv.mol2
load 2xbv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xbv
show sticks, 2xbv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
