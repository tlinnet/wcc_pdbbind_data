load 02_4wko.mol2
load 4wko_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4wko
show sticks, 4wko_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
