#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4a6c_ligand.sdf 04_out_4a6c.sd -o 04_out_4a6c_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4a6c_rmsd.sd | tee 05_get_rmsd.score
