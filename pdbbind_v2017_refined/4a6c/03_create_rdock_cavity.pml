load 02_4a6c.mol2
load 4a6c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4a6c
show sticks, 4a6c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
