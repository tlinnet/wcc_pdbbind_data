load 02_5d3x.mol2
load 5d3x_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5d3x
show sticks, 5d3x_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
