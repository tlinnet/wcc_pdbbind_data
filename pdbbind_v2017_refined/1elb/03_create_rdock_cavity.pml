load 02_1elb.mol2
load 1elb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1elb
show sticks, 1elb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
