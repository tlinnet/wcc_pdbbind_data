load 02_5jvi.mol2
load 5jvi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5jvi
show sticks, 5jvi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
