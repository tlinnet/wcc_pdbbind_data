load 02_3qgy.mol2
load 3qgy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qgy
show sticks, 3qgy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
