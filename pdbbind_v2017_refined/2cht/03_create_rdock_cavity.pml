load 02_2cht.mol2
load 2cht_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2cht
show sticks, 2cht_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
