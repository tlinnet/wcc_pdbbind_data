load 02_4dsy.mol2
load 4dsy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4dsy
show sticks, 4dsy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
