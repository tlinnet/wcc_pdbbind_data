load 02_1efy.mol2
load 1efy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1efy
show sticks, 1efy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
