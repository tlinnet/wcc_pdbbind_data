load 02_2f35.mol2
load 2f35_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2f35
show sticks, 2f35_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
