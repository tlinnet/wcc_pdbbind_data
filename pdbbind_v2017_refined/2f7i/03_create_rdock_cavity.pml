load 02_2f7i.mol2
load 2f7i_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2f7i
show sticks, 2f7i_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
