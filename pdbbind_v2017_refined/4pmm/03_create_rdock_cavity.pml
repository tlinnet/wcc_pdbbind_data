load 02_4pmm.mol2
load 4pmm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4pmm
show sticks, 4pmm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
