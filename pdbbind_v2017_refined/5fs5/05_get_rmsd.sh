#!/usr/bin/env bash

# Get best rmsd
sdrmsd 5fs5_ligand.sdf 04_out_5fs5.sd -o 04_out_5fs5_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_5fs5_rmsd.sd | tee 05_get_rmsd.score
