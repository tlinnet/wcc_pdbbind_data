load 02_5fs5.mol2
load 5fs5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fs5
show sticks, 5fs5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
