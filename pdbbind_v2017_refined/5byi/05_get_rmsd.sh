#!/usr/bin/env bash

# Get best rmsd
sdrmsd 5byi_ligand.sdf 04_out_5byi.sd -o 04_out_5byi_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_5byi_rmsd.sd | tee 05_get_rmsd.score
