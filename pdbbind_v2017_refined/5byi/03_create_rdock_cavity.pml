load 02_5byi.mol2
load 5byi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5byi
show sticks, 5byi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
