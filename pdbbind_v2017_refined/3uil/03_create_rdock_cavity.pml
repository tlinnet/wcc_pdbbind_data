load 02_3uil.mol2
load 3uil_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3uil
show sticks, 3uil_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
