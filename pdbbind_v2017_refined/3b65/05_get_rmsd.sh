#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3b65_ligand.sdf 04_out_3b65.sd -o 04_out_3b65_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3b65_rmsd.sd | tee 05_get_rmsd.score
