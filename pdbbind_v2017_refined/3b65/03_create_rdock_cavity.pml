load 02_3b65.mol2
load 3b65_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3b65
show sticks, 3b65_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
