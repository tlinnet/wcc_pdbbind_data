load 02_1g54.mol2
load 1g54_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1g54
show sticks, 1g54_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
