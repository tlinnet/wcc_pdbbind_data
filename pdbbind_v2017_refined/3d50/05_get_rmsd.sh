#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3d50_ligand.sdf 04_out_3d50.sd -o 04_out_3d50_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3d50_rmsd.sd | tee 05_get_rmsd.score
