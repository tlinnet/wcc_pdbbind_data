load 02_2avm.mol2
load 2avm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2avm
show sticks, 2avm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
