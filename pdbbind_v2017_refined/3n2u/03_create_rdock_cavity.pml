load 02_3n2u.mol2
load 3n2u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3n2u
show sticks, 3n2u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
