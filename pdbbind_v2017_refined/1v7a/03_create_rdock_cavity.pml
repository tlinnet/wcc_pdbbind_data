load 02_1v7a.mol2
load 1v7a_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1v7a
show sticks, 1v7a_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
