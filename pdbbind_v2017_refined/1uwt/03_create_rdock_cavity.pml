load 02_1uwt.mol2
load 1uwt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1uwt
show sticks, 1uwt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
