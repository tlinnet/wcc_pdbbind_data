load 02_3kgu.mol2
load 3kgu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kgu
show sticks, 3kgu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
