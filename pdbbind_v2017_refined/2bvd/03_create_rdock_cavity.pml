load 02_2bvd.mol2
load 2bvd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2bvd
show sticks, 2bvd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
