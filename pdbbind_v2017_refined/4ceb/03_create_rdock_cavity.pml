load 02_4ceb.mol2
load 4ceb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ceb
show sticks, 4ceb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
