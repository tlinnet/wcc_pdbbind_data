load 02_2cf8.mol2
load 2cf8_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2cf8
show sticks, 2cf8_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
