load 02_1hmt.mol2
load 1hmt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hmt
show sticks, 1hmt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
