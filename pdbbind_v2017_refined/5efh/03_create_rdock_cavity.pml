load 02_5efh.mol2
load 5efh_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5efh
show sticks, 5efh_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
