load 02_2rka.mol2
load 2rka_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2rka
show sticks, 2rka_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
