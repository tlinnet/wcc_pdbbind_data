load 02_3lzu.mol2
load 3lzu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3lzu
show sticks, 3lzu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
