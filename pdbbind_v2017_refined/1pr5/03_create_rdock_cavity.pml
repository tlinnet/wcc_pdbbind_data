load 02_1pr5.mol2
load 1pr5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pr5
show sticks, 1pr5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
