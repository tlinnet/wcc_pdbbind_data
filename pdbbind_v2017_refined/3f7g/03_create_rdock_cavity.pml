load 02_3f7g.mol2
load 3f7g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3f7g
show sticks, 3f7g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
