load 02_4x50.mol2
load 4x50_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4x50
show sticks, 4x50_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
