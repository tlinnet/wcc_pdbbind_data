load 02_4myd.mol2
load 4myd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4myd
show sticks, 4myd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
