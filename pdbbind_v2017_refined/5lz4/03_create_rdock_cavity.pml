load 02_5lz4.mol2
load 5lz4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5lz4
show sticks, 5lz4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
