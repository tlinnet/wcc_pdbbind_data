load 02_2csn.mol2
load 2csn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2csn
show sticks, 2csn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
