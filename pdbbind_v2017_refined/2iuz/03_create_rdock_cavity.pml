load 02_2iuz.mol2
load 2iuz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2iuz
show sticks, 2iuz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
