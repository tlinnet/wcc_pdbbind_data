#!/usr/bin/env bash

# Get best rmsd
sdrmsd 3q6w_ligand.sdf 04_out_3q6w.sd -o 04_out_3q6w_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_3q6w_rmsd.sd | tee 05_get_rmsd.score
