load 02_3ov1.mol2
load 3ov1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ov1
show sticks, 3ov1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
