load 02_4u5n.mol2
load 4u5n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4u5n
show sticks, 4u5n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
