load 02_2bok.mol2
load 2bok_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2bok
show sticks, 2bok_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
