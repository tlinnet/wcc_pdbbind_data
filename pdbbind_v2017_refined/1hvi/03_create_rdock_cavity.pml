load 02_1hvi.mol2
load 1hvi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hvi
show sticks, 1hvi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
