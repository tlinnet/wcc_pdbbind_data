load 02_4l2l.mol2
load 4l2l_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4l2l
show sticks, 4l2l_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
