load 02_5flo.mol2
load 5flo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5flo
show sticks, 5flo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
