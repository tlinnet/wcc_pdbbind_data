load 02_2w66.mol2
load 2w66_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2w66
show sticks, 2w66_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
