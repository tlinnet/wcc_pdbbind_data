load 02_5a5q.mol2
load 5a5q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5a5q
show sticks, 5a5q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
