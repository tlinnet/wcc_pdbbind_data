load 02_1p19.mol2
load 1p19_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1p19
show sticks, 1p19_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
