load 02_1g45.mol2
load 1g45_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1g45
show sticks, 1g45_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
