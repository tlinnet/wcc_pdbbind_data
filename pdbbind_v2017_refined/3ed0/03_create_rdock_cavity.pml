load 02_3ed0.mol2
load 3ed0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ed0
show sticks, 3ed0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
