load 02_4abb.mol2
load 4abb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4abb
show sticks, 4abb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
