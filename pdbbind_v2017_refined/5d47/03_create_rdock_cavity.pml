load 02_5d47.mol2
load 5d47_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5d47
show sticks, 5d47_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
