load 02_2h4k.mol2
load 2h4k_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2h4k
show sticks, 2h4k_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
