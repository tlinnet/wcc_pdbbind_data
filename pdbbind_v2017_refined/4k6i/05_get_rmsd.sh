#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4k6i_ligand.sdf 04_out_4k6i.sd -o 04_out_4k6i_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4k6i_rmsd.sd | tee 05_get_rmsd.score
