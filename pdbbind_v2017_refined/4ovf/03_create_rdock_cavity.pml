load 02_4ovf.mol2
load 4ovf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ovf
show sticks, 4ovf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
