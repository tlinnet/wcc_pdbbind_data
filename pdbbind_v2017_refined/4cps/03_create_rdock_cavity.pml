load 02_4cps.mol2
load 4cps_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cps
show sticks, 4cps_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
