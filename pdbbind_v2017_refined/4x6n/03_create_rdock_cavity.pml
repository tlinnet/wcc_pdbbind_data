load 02_4x6n.mol2
load 4x6n_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4x6n
show sticks, 4x6n_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
