load 02_5std.mol2
load 5std_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5std
show sticks, 5std_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
