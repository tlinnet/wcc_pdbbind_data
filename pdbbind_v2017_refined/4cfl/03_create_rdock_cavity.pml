load 02_4cfl.mol2
load 4cfl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cfl
show sticks, 4cfl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
