load 02_2hnc.mol2
load 2hnc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2hnc
show sticks, 2hnc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
