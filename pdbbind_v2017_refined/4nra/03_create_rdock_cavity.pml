load 02_4nra.mol2
load 4nra_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nra
show sticks, 4nra_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
