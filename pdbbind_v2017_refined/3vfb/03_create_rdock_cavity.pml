load 02_3vfb.mol2
load 3vfb_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3vfb
show sticks, 3vfb_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
