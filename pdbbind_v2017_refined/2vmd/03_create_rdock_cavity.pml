load 02_2vmd.mol2
load 2vmd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vmd
show sticks, 2vmd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
