load 02_2r43.mol2
load 2r43_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2r43
show sticks, 2r43_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
