load 02_1j14.mol2
load 1j14_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1j14
show sticks, 1j14_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
