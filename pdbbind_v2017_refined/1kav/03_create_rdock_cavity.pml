load 02_1kav.mol2
load 1kav_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1kav
show sticks, 1kav_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
