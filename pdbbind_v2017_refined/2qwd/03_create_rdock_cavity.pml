load 02_2qwd.mol2
load 2qwd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qwd
show sticks, 2qwd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
