load 02_1amk.mol2
load 1amk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1amk
show sticks, 1amk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
