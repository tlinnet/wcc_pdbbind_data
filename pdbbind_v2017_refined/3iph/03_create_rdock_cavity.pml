load 02_3iph.mol2
load 3iph_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3iph
show sticks, 3iph_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
