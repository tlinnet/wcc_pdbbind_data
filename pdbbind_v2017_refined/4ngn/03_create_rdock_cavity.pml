load 02_4ngn.mol2
load 4ngn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ngn
show sticks, 4ngn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
