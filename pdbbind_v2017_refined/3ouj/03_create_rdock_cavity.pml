load 02_3ouj.mol2
load 3ouj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ouj
show sticks, 3ouj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
