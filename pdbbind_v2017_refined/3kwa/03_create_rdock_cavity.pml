load 02_3kwa.mol2
load 3kwa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kwa
show sticks, 3kwa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
