load 02_2qu6.mol2
load 2qu6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2qu6
show sticks, 2qu6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
