load 02_4x5y.mol2
load 4x5y_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4x5y
show sticks, 4x5y_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
