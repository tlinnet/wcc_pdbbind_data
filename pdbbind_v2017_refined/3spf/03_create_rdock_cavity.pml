load 02_3spf.mol2
load 3spf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3spf
show sticks, 3spf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
