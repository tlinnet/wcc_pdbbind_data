load 02_1dzk.mol2
load 1dzk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1dzk
show sticks, 1dzk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
