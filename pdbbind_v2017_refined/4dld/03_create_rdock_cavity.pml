load 02_4dld.mol2
load 4dld_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4dld
show sticks, 4dld_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
