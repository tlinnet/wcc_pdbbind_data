load 02_1w11.mol2
load 1w11_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1w11
show sticks, 1w11_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
