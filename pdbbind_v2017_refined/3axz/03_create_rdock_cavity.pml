load 02_3axz.mol2
load 3axz_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3axz
show sticks, 3axz_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
