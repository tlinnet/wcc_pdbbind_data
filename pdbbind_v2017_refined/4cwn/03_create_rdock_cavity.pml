load 02_4cwn.mol2
load 4cwn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cwn
show sticks, 4cwn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
