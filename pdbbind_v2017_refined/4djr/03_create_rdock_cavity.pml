load 02_4djr.mol2
load 4djr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4djr
show sticks, 4djr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
