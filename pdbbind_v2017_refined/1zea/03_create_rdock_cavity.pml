load 02_1zea.mol2
load 1zea_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1zea
show sticks, 1zea_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
