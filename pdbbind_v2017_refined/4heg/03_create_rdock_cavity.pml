load 02_4heg.mol2
load 4heg_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4heg
show sticks, 4heg_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
