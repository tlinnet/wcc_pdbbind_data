load 02_3evd.mol2
load 3evd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3evd
show sticks, 3evd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
