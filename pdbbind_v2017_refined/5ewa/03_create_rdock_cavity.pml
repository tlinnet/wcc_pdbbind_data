load 02_5ewa.mol2
load 5ewa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ewa
show sticks, 5ewa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
