load 02_1ppk.mol2
load 1ppk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ppk
show sticks, 1ppk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
