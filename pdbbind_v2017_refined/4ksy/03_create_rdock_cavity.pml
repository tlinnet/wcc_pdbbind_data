load 02_4ksy.mol2
load 4ksy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ksy
show sticks, 4ksy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
