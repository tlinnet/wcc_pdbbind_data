load 02_3ikd.mol2
load 3ikd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ikd
show sticks, 3ikd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
