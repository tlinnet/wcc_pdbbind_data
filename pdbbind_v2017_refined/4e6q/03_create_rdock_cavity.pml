load 02_4e6q.mol2
load 4e6q_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4e6q
show sticks, 4e6q_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
