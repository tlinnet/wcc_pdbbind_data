load 02_2xxr.mol2
load 2xxr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xxr
show sticks, 2xxr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
