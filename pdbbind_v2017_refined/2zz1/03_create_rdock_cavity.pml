load 02_2zz1.mol2
load 2zz1_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2zz1
show sticks, 2zz1_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
