load 02_3ttp.mol2
load 3ttp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ttp
show sticks, 3ttp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
