load 02_5epl.mol2
load 5epl_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5epl
show sticks, 5epl_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
