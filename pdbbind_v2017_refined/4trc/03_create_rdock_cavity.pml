load 02_4trc.mol2
load 4trc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4trc
show sticks, 4trc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
