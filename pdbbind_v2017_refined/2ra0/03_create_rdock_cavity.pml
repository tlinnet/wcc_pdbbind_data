load 02_2ra0.mol2
load 2ra0_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ra0
show sticks, 2ra0_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
