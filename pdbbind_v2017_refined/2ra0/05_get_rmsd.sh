#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2ra0_ligand.sdf 04_out_2ra0.sd -o 04_out_2ra0_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2ra0_rmsd.sd | tee 05_get_rmsd.score
