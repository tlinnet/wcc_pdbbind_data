load 02_1vfn.mol2
load 1vfn_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1vfn
show sticks, 1vfn_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
