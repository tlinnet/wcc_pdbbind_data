load 02_5ipj.mol2
load 5ipj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5ipj
show sticks, 5ipj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
