load 02_5drr.mol2
load 5drr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5drr
show sticks, 5drr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
