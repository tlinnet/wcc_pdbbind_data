load 02_3cd5.mol2
load 3cd5_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3cd5
show sticks, 3cd5_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
