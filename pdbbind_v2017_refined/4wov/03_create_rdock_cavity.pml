load 02_4wov.mol2
load 4wov_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4wov
show sticks, 4wov_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
