load 02_1hii.mol2
load 1hii_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1hii
show sticks, 1hii_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
