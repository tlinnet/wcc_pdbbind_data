load 02_3hmo.mol2
load 3hmo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3hmo
show sticks, 3hmo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
