load 02_3kdc.mol2
load 3kdc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3kdc
show sticks, 3kdc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
