load 02_3gi6.mol2
load 3gi6_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gi6
show sticks, 3gi6_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
