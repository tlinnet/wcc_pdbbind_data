load 02_1bnv.mol2
load 1bnv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bnv
show sticks, 1bnv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
