load 02_4u0f.mol2
load 4u0f_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4u0f
show sticks, 4u0f_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
