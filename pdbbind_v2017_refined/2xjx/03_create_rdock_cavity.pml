load 02_2xjx.mol2
load 2xjx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xjx
show sticks, 2xjx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
