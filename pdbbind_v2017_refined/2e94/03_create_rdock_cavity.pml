load 02_2e94.mol2
load 2e94_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2e94
show sticks, 2e94_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
