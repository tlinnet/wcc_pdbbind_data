load 02_1lnm.mol2
load 1lnm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1lnm
show sticks, 1lnm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
