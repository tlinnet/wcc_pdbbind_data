load 02_4ngm.mol2
load 4ngm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ngm
show sticks, 4ngm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
