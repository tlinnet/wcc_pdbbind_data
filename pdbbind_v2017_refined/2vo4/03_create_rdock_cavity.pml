load 02_2vo4.mol2
load 2vo4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2vo4
show sticks, 2vo4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
