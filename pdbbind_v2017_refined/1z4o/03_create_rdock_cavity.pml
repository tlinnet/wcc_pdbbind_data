load 02_1z4o.mol2
load 1z4o_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1z4o
show sticks, 1z4o_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
