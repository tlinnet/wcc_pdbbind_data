load 02_3ggu.mol2
load 3ggu_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ggu
show sticks, 3ggu_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
