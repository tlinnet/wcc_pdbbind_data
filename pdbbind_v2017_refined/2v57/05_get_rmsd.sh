#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2v57_ligand.sdf 04_out_2v57.sd -o 04_out_2v57_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2v57_rmsd.sd | tee 05_get_rmsd.score
