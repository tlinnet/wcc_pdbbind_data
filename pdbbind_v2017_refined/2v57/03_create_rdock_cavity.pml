load 02_2v57.mol2
load 2v57_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2v57
show sticks, 2v57_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
