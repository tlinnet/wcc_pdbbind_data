load 02_4c2v.mol2
load 4c2v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4c2v
show sticks, 4c2v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
