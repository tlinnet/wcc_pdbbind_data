load 02_1kjr.mol2
load 1kjr_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1kjr
show sticks, 1kjr_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
