load 02_5d6j.mol2
load 5d6j_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5d6j
show sticks, 5d6j_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
