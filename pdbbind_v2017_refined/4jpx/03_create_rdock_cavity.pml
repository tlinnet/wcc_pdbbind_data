load 02_4jpx.mol2
load 4jpx_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4jpx
show sticks, 4jpx_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
