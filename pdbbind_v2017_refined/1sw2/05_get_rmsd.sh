#!/usr/bin/env bash

# Get best rmsd
sdrmsd 1sw2_ligand.sdf 04_out_1sw2.sd -o 04_out_1sw2_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_1sw2_rmsd.sd | tee 05_get_rmsd.score
