load 02_3ozj.mol2
load 3ozj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ozj
show sticks, 3ozj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
