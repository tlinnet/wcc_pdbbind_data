load 02_5d3c.mol2
load 5d3c_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5d3c
show sticks, 5d3c_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
