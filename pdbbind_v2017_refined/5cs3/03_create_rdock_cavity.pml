load 02_5cs3.mol2
load 5cs3_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5cs3
show sticks, 5cs3_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
