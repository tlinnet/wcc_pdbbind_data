#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2q54_ligand.sdf 04_out_2q54.sd -o 04_out_2q54_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2q54_rmsd.sd | tee 05_get_rmsd.score
