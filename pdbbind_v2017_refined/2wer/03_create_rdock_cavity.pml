load 02_2wer.mol2
load 2wer_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2wer
show sticks, 2wer_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
