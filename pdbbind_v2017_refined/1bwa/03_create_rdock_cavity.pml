load 02_1bwa.mol2
load 1bwa_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1bwa
show sticks, 1bwa_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
