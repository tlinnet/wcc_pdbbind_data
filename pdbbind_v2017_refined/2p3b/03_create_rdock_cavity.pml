load 02_2p3b.mol2
load 2p3b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2p3b
show sticks, 2p3b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
