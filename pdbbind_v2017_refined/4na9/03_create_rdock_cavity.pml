load 02_4na9.mol2
load 4na9_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4na9
show sticks, 4na9_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
