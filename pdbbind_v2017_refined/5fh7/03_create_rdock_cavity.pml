load 02_5fh7.mol2
load 5fh7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5fh7
show sticks, 5fh7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
