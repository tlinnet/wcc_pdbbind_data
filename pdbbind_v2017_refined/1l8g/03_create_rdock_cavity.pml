load 02_1l8g.mol2
load 1l8g_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1l8g
show sticks, 1l8g_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
