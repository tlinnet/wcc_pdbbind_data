load 02_2bpv.mol2
load 2bpv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2bpv
show sticks, 2bpv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
