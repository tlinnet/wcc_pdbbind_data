load 02_2cet.mol2
load 2cet_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2cet
show sticks, 2cet_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
