load 02_3re4.mol2
load 3re4_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3re4
show sticks, 3re4_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
