load 02_1g4o.mol2
load 1g4o_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1g4o
show sticks, 1g4o_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
