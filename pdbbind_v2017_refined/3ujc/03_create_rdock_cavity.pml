load 02_3ujc.mol2
load 3ujc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3ujc
show sticks, 3ujc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
