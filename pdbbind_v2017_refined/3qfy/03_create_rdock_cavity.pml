load 02_3qfy.mol2
load 3qfy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3qfy
show sticks, 3qfy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
