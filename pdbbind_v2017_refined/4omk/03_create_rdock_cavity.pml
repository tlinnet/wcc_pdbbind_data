load 02_4omk.mol2
load 4omk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4omk
show sticks, 4omk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
