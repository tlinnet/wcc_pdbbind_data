load 02_1xhy.mol2
load 1xhy_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1xhy
show sticks, 1xhy_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
