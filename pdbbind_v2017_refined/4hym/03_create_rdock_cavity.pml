load 02_4hym.mol2
load 4hym_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4hym
show sticks, 4hym_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
