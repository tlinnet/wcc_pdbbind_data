load 02_3c2r.mol2
load 3c2r_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3c2r
show sticks, 3c2r_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
