load 02_1drj.mol2
load 1drj_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1drj
show sticks, 1drj_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
