load 02_4ghi.mol2
load 4ghi_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4ghi
show sticks, 4ghi_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
