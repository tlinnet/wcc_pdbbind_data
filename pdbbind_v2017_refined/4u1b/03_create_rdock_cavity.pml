load 02_4u1b.mol2
load 4u1b_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4u1b
show sticks, 4u1b_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
