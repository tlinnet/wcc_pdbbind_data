load 02_4q4s.mol2
load 4q4s_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4q4s
show sticks, 4q4s_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
