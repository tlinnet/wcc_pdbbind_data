load 02_2i4d.mol2
load 2i4d_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i4d
show sticks, 2i4d_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
