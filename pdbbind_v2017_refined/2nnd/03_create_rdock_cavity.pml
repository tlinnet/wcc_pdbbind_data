load 02_2nnd.mol2
load 2nnd_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2nnd
show sticks, 2nnd_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
