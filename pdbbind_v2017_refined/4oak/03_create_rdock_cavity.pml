load 02_4oak.mol2
load 4oak_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4oak
show sticks, 4oak_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
