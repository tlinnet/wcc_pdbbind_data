load 02_2ews.mol2
load 2ews_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ews
show sticks, 2ews_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
