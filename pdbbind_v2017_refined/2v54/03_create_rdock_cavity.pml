load 02_2v54.mol2
load 2v54_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2v54
show sticks, 2v54_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
