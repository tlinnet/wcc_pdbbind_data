load 02_2fgv.mol2
load 2fgv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2fgv
show sticks, 2fgv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
