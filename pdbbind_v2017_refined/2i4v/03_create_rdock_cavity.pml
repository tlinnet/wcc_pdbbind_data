load 02_2i4v.mol2
load 2i4v_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2i4v
show sticks, 2i4v_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
