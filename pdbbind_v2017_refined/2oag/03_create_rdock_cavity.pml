load 02_2oag.mol2
load 2oag_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2oag
show sticks, 2oag_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
