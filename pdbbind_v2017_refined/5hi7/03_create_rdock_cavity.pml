load 02_5hi7.mol2
load 5hi7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5hi7
show sticks, 5hi7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
