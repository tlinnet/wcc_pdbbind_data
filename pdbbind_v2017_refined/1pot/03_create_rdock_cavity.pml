load 02_1pot.mol2
load 1pot_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1pot
show sticks, 1pot_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
