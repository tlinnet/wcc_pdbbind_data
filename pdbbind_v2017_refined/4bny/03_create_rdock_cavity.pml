load 02_4bny.mol2
load 4bny_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4bny
show sticks, 4bny_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
