load 02_3gtc.mol2
load 3gtc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3gtc
show sticks, 3gtc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
