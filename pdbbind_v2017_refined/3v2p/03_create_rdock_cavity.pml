load 02_3v2p.mol2
load 3v2p_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3v2p
show sticks, 3v2p_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
