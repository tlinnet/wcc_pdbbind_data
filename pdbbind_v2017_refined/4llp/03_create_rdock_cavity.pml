load 02_4llp.mol2
load 4llp_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4llp
show sticks, 4llp_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
