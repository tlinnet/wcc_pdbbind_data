#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2z4o_ligand.sdf 04_out_2z4o.sd -o 04_out_2z4o_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2z4o_rmsd.sd | tee 05_get_rmsd.score
