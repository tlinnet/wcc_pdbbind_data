load 02_1ebw.mol2
load 1ebw_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1ebw
show sticks, 1ebw_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
