load 02_1kpm.mol2
load 1kpm_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1kpm
show sticks, 1kpm_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
