load 02_1h23.mol2
load 1h23_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1h23
show sticks, 1h23_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
