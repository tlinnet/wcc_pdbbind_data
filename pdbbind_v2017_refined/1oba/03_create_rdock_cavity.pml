load 02_1oba.mol2
load 1oba_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1oba
show sticks, 1oba_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
