load 02_2xxt.mol2
load 2xxt_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2xxt
show sticks, 2xxt_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
