#!/usr/bin/env bash

# Get best rmsd
sdrmsd 2f6t_ligand.sdf 04_out_2f6t.sd -o 04_out_2f6t_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_2f6t_rmsd.sd | tee 05_get_rmsd.score
