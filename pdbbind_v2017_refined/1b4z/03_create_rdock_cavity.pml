load 02_1b4z.mol2
load 1b4z_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1b4z
show sticks, 1b4z_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
