load 02_3own.mol2
load 3own_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_3own
show sticks, 3own_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
