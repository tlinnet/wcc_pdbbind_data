load 02_2buv.mol2
load 2buv_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2buv
show sticks, 2buv_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
