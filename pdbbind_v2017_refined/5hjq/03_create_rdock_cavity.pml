load 02_5hjq.mol2
load 5hjq_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_5hjq
show sticks, 5hjq_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
