load 02_2gss.mol2
load 2gss_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2gss
show sticks, 2gss_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
