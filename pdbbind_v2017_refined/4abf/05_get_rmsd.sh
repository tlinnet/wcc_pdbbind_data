#!/usr/bin/env bash

# Get best rmsd
sdrmsd 4abf_ligand.sdf 04_out_4abf.sd -o 04_out_4abf_rmsd.sd | tee 05_get_rmsd.log

# Create the SCORE and RMSD in a file
sdreport -cSCORE,RMSD 04_out_4abf_rmsd.sd | tee 05_get_rmsd.score
