load 02_4abf.mol2
load 4abf_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4abf
show sticks, 4abf_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
