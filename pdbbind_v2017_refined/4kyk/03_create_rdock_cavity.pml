load 02_4kyk.mol2
load 4kyk_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4kyk
show sticks, 4kyk_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
