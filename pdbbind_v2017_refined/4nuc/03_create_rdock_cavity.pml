load 02_4nuc.mol2
load 4nuc_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4nuc
show sticks, 4nuc_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
