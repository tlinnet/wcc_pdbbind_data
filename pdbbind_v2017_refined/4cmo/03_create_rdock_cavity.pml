load 02_4cmo.mol2
load 4cmo_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_4cmo
show sticks, 4cmo_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
