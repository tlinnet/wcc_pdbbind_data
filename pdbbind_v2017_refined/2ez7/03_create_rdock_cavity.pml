load 02_2ez7.mol2
load 2ez7_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_2ez7
show sticks, 2ez7_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
