load 02_1a30.mol2
load 1a30_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1a30
show sticks, 1a30_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
