load 02_1w9u.mol2
load 1w9u_ligand.sdf
load 03_create_rdock_cavity_cav1.grd

hide everything
show cartoon, 02_1w9u
show sticks, 1w9u_ligand

isomesh cavity, 03_create_rdock_cavity_cav1, 0.99
