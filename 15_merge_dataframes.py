import pandas as pd
import numpy as np
import os.path
import json

df1 = pd.read_pickle("12_info_dataframe.pkl")
df2 = pd.read_pickle("13_ligand_dataframe.pkl")

#print(df1.info())
#print(df2.info())
#s="sanitizable"; print("\n"+s)
#print(df2[s].value_counts())

# Merge them
df_m = pd.merge(df1, df2, how='left', on=["pdb_ids"])
#print(df_m.info())

# Load dictionary with uniprot 
json_dic = '14_uniprot_json_dict.json'
if os.path.exists(json_dic):
    with open(json_dic, 'r') as fp:
        uniprot_dic = json.load(fp)


# Now loop over 
d_list = []
for pdb_id, Uniprot, Uniprot_pdb_ids in zip(df_m['pdb_ids'], df_m['Uniprot'], df_m['Uniprot_pdb_ids_s']):
    #print(pdb_id, Uniprot, Uniprot_pdb_ids)
    d = {}
    d['pdb_ids'] = pdb_id
    # Get the list of proteins 
    if Uniprot == "------":
        lp = []
        #continue
    else:
        lp = uniprot_dic[Uniprot]
    # Make them small 
    lps = [x.lower() for x in lp]
    # Remove current pdb
    if pdb_id in lps:
        lps.remove(pdb_id)
    # Read list from string with ,
    ulps = Uniprot_pdb_ids.split(",")
    # Make Complement of Set "ulps"
    comp_ulps = list(set(lps)-set(ulps))

    #print(len(lps), len(ulps), len(comp_ulps))

    # Make ready for store
    Uniprot_N_all = len(lps)
    Uniprot_pdb_ids_s_all  = ",".join(str(x) for x in lps)
    Uniprot_N_miss = len(comp_ulps)
    Uniprot_pdb_ids_s_miss  = ",".join(str(x) for x in comp_ulps)

    d['Uniprot_N_all'] = Uniprot_N_all
    d['Uniprot_pdb_ids_s_all'] = Uniprot_pdb_ids_s_all
    d['Uniprot_N_miss'] = Uniprot_N_miss
    d['Uniprot_pdb_ids_s_miss'] = Uniprot_pdb_ids_s_miss

    d_list.append(d)

# Merge from dic
df = pd.DataFrame.from_dict(d_list)

# Merge them
df_m = pd.merge(df_m, df, how='left', on=["pdb_ids"])
df_m.to_pickle("15_dataframes_merged.pkl")